<div class="Gallery">
	@foreach ($projects as $post)
		<div class="Gallery__item">
			<a href="{{ $context->getPath($post) }}" class="Gallery__image">
				{!! $post->preview->getHtml(['alt' => $post->title]) !!}
			</a>
			<div class="Gallery__info">
				<h3 class="Gallery__title">
					<a href="{{ $context->getPath($post) }}">{{ $post->title }}</a>
				</h3>
				<em class="Gallery__date">{{ $post->getDate()->format('F Y') }}</em>
				<p class="Gallery__excerpt">{{ $post->excerpt }}</p>
			</div>
		</div>
	@endforeach
</div>