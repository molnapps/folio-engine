<ul class="Menu Menu--{{ $context->getContainerModifier() }}">
	@foreach ($pages as $post)
		<li class="Menu__item Menu__item--{{ $post->slug }}">
			<a 
				href="{{ $context->getPath($post) }}" 
				class="Menu__link Menu__link--{{ $post->slug }}"
			>{{ $post->title }}</a>
		</li>
	@endforeach
</ul>