<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<title>{{ $settings->title }}</title>
	<meta name="description" content="{{ $settings->description }}" />
	<meta name="keywords" content="{{ $settings->keywords }}" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="{{ theme()->assets('template.css') }}"/>
	<script type="text/javascript" src="{{ theme()->assets('template.js') }}"></script>
</head>
<body>
<div id="container" class="Container Container--{{ $context->getContainerModifier() }}">
	<div class="Container__contents">
		<a href="/" id="logo" class="Header__logo">
			@if ($settings->logo)
				<img 
					src="{{ $settings->logo->getUrl() }}" 
					width="72" 
					height="72" 
					type="image/svg+xml" 
					codebase="http://www.adobe.com/svg/viewer/install" 
				/> 
			@endif
		</a>
		<h1 id="title" class="Header__title">
			<a href="/">{{ $settings->title }}</a>
		</h1>
		@include('menu')
		@yield('content')
		<div class="Footer">
			<p class="Footer__headline">{{ $settings->footer }}</p>
			@include('links')
			<h4 class="Footer__copyright">Copyright &copy; 2007 &mdash; {{ $utilities->year }}</h4>
		</div>
	</div>
</div>
</body>
</html>