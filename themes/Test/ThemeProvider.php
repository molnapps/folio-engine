<?php

namespace Folio\Themes\Test;

use App\Themes\ThemeProvider as ThemeProviderInterface;
use org\bovigo\vfs\vfsStream;

class ThemeProvider implements ThemeProviderInterface
{
	public function getBasePath()
	{
		return vfsStream::url('folio/theme');
	}
}