<?php

namespace Tests;

use App\App;
use App\Portfolio;
use Illuminate\Testing\TestResponse;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use org\bovigo\vfs\vfsStream;

abstract class TestCase extends PHPUnitTestCase
{
	/** @before */
	public function setUpApp()
	{
		App::bootstrapThemes([
			'folio' => \Folio\Themes\Test\ThemeProvider::class,
			'test' => \Folio\Themes\Test\ThemeProvider::class
		]);
	}
	
	/** @after */
	public function tearDownApp()
	{
		App::destroy();
	}

	protected function visit($path)
	{
		$request = new Request(
			$get = [], 
			$post = [], 
			$attr = [], 
			$cookies = [], 
			$files = [], 
			$server = ['REQUEST_URI' => $path]
		);

		return $this->getResponse($request);
	}

	protected function getResponse(Request $request = null)
	{
		ob_start();
	    $controller = app(Portfolio::class)->run($request);
	    ob_get_clean();

	    return $this->getTestResponse($controller);
	}

	protected function getTestResponse($controller)
	{
		return new TestResponse(
	    	$controller->getResponse()
	    );
	}

	protected function getPublicJsonFiles()
	{
		return $this->getPublicHtmlFiles('folio/public/json');
	}

	protected function getPublicHtmlFiles($path = 'folio/public')
	{
		return array_values(
			array_filter(
				scandir(vfsStream::url($path)), 
				function ($filename) {
					return $filename != '.' && $filename != '..';
				}
			)
		);
	}

	protected function overrideEnvironmentVariable($key, $value)
	{
		$_SERVER[$key] = $value;
	}
}