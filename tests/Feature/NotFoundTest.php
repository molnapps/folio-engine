<?php

namespace Tests\Feature;

use App\App;
use App\Portfolio;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\FeatureTestCase;
use org\bovigo\vfs\vfsStream;

class NotFoundTest extends FeatureTestCase
{
	/** @test */
	public function it_returns_successful_response()
	{
	    $response = $this->visit('/page/foobarbaz');

	    $response->assertOk();
	}

	/** @test */
	public function it_displays_not_found()
	{
	    $response = $this->visit('/page/foobarbaz');

	    $response->assertSee('Not found');
	    $response->assertSee('The page you were looking for could not be found');
	}

	/** @test */
	public function it_displays_links()
	{
	    $response = $this->visit('/page/foobarbaz');

	    $this->assertSeeLinks($response);
	}

	/** @test */
	public function it_displays_settings()
	{
	    $response = $this->visit('/page/foobarbaz');

	    $this->assertSeeSettings($response);
	}

	/** @test */
	public function it_displays_assets()
	{
	    $response = $this->visit('/page/foobarbaz');

	    $this->assertSeeAssets($response);
	}
}