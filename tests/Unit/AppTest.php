<?php

namespace Tests\Unit;

use App\App;
use App\Portfolio;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ClientBuilder;
use App\Testing\ProjectBuilder;
use App\Views\OpenGraph\OpenGraph;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;

class AppTest extends TestCase
{
	use InteractsWithFileSystem;
	
	/** @test */
	public function it_can_be_instantiated_through_static_method()
	{
	    $this->assertNotNull(App::instance());
	}

	/** @test */
	public function it_returns_singleton()
	{
		$app = App::instance();
	    $this->assertSame($app, App::instance());
	}

	/** @test */
	public function it_destroys_app()
	{
		$app = App::instance();
	    $this->assertSame($app, App::instance());
	    App::destroy();
	    $this->assertNotSame($app, App::instance());
	}

	/** @test */
	public function it_destroys_themes()
	{
		$themeProvider = App::instance()->getThemeProvider('test');
	    
	    App::destroy();
	    
	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage(
	    	'Please provide a theme provider through ::bootstrapThemes() method'
	    );

	    App::instance()->getThemeProvider('test');
	}

	/** @test */
	public function it_destroys_repository()
	{
		$fooRegistry = new FooRegistry;

		App::bootstrapRegistry($fooRegistry);

		$this->assertSame(
	    	$fooRegistry, 
	    	App::instance()->get(\App\Repositories\Registry::class)
	    );
	    
	    App::destroy();
	    
	    $this->assertNotSame(
	    	$fooRegistry, 
	    	App::instance()->get(\App\Repositories\Registry::class)
	    );
	}

	/** @test */
	public function it_returns_environment()
	{
	    $this->assertTrue(App::instance()->env('testing'));
	    $this->assertFalse(App::instance()->env('production'));
	    $this->assertFalse(App::instance()->env('local'));
	}

	/** @test */
	public function it_allows_to_bootstrap_themes()
	{
	    App::bootstrapThemes([
	    	'foobar' => \Folio\Themes\Test\ThemeProvider::class
	    ]);

	    $this->assertInstanceOf(
	    	\Folio\Themes\Test\ThemeProvider::class, 
	    	App::instance()->getThemeProvider('foobar')
	    );
	}

	public function it_overrides_themes()
	{
		$this->assertInstanceOf(
	    	\Folio\Themes\Test\ThemeProvider::class, 
	    	App::instance()->getThemeProvider('test')
	    );

	    App::bootstrapThemes([
	    	'foobar' => \Folio\Themes\Test\ThemeProvider::class
	    ]);

	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage(
	    	'Please provide a theme provider through ::bootstrapThemes() method'
	    );

	    App::instance()->getThemeProvider('test');
	}

	/** @test */
	public function it_throws_if_themes_were_not_bootstrapped()
	{
	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage(
	    	'Please provide a theme provider through ::bootstrapThemes() method'
	    );

	    App::instance()->getThemeProvider('foobar');
	}

	/** @test */
	public function it_returns_instance_of_portfolio()
	{
	    $this->assertInstanceOf(
	    	\App\Portfolio::class, 
	    	App::instance()->get(\App\Portfolio::class)
	    );
	}

	/** @test */
	public function it_returns_instance_of_repository()
	{
	    $this->assertInstanceOf(
	    	\App\Repositories\Registry::class, 
	    	App::instance()->get(\App\Repositories\Registry::class)
	    );
	}

	/** @test */
	public function it_returns_instance_of_blade()
	{
	    $this->assertInstanceOf(
	    	\Jenssegers\Blade\Blade::class, 
	    	App::instance()->get(\Jenssegers\Blade\Blade::class)
	    );
	}

	/** @test */
	public function it_provides_qualified_error_if_theme_is_not_defined_in_settings()
	{
		$this->eraseSettings();

	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage(
	    	'Please provide a theme in your settings.json file'
	    );

	    App::instance()->get(\Jenssegers\Blade\Blade::class);
	}

	/** @test */
	public function it_allows_to_provide_custom_repository()
	{
		$fooRegistry = new FooRegistry;

	    App::bootstrapRegistry($fooRegistry);

	    $this->assertInstanceOf(
	    	FooRegistry::class,
	    	App::instance()->get(\App\Repositories\Registry::class)
	    );
	    $this->assertSame(
	    	$fooRegistry, 
	    	App::instance()->get(\App\Repositories\Registry::class)
	    );
	}

	/** @test */
	public function it_allows_to_specify_a_whitelist_for_image_publishers()
	{
        App::bootstrapImagesWhitelist(['_icons']);
        
        $instance = app(\App\Themes\Publishers\Images\Used\UsedImagesFinder::class);

        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/_icons/my-icon.jpg')));
        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/foo.jpg')));
	}

	/** @test */
	public function it_allows_to_specify_config_for_image_crops()
	{
		$croppedImagesMap = app(CroppedImagesMap::class);

		$sourceImagePath = vfsStream::url('folio/contents/images/_icons/my-icon.jpg');

		$this->assertEquals(
			['square', 'portrait'], 
			$croppedImagesMap->getFormats($sourceImagePath)
		);

        App::bootstrapImagesFormats([
			'_icons' => ['skip' => true]
		]);
        
        $this->assertEquals(
			[], 
			$croppedImagesMap->getFormats($sourceImagePath)
		);
	}

	/** @test */
	public function it_allows_to_specify_config_for_open_graph()
	{
		registry()->settings()->merge([
			'company' => 'Foo Company',
			'fullName' => 'John Doe',
			'jobTitle' => 'Head of Crayons',
		]);

		App::bootstrapOpenGraph(
			function ($settings) {
				return [
					'title' => $settings->company,
					'description' => $settings->fullName . ' / ' . $settings->jobTitle,
					'image' => '_icons/website-feature.png'
				];
			}
		);

		$openGraph = app(OpenGraph::class);

		$this->assertEquals('Foo Company', $openGraph->getTitle());

		$this->assertEquals('John Doe / Head of Crayons', $openGraph->getDescription());

		$this->assertEquals(
			'https://folio-engine.test/images/_icons/website-feature.png', 
			$openGraph->getImageUrl()
		);
	}

	/** @test */
	public function it_allows_to_specify_config_for_open_graph_post()
	{
		ClientBuilder::fromSlug('foo-client')->create();

		ProjectBuilder::fromSlug('foo')
			->withWebsiteFeatureImage('bar')
			->withMeta([
				'client' => 'foo-client', 
				'title' => 'My project', 
				'excerpt' => 'My excerpt'
			])
			->create()
			->publishImages();

		$post = app(Portfolio::class)->getProject('foo');
		$postOpenGraph = $post->getOpenGraph();

		$expected = [
			'title' => 'My project',
			'description' => 'My excerpt',
			'websiteFeature' => 'https://folio-engine.test/images/foo/bar-website-feature-compressed.jpg',
		];

		$this->assertEquals($expected['title'], $postOpenGraph->getTitle());
		$this->assertEquals($expected['description'], $postOpenGraph->getDescription());
		$this->assertEquals($expected['websiteFeature'], $postOpenGraph->getImageUrl());
		
		App::bootstrapOpenGraph(
			$baseCallback = null,
			function ($post) {
				return [
					'title' => $post->client->title . ': ' . $post->title
				];
			}
		);

		$postOpenGraph = $post->getOpenGraph();

		$this->assertEquals('My client: My project', $postOpenGraph->getTitle());
		$this->assertEquals($expected['description'], $postOpenGraph->getDescription());
		$this->assertEquals($expected['websiteFeature'], $postOpenGraph->getImageUrl());
		
	}

	private function eraseSettings()
	{
		return file_put_contents(vfsStream::url('folio/contents/settings.json'), json_encode([]));
	}
}

class FooRegistry extends \App\Repositories\Registry
{
	
}