<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\GenerateNuxtPortfolio;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithMarkdownFiles;
use App\Generators\FolioManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Console\Testing\TestOutput;
use App\Testing\InspectsBuildUrls;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ArrayOutput;
use Symfony\Component\Console\Output\NullOutput;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class GenerateNuxtPortfolioTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;
	use InspectsBuildUrls;

	private $console;
	
	/** @before */
	public function setUpConsole()
	{
		$this->console = new \App\Console\Application;
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new GenerateNuxtPortfolio);
	}

	/** @test */
	public function it_will_publish_json_files()
	{
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/json/projects.json'));

		$this->console
	    	->find('portfolio:generate-nuxt')
	    	->run(
	    		new ArrayInput(['--destination' => vfsStream::url('folio/public')]),
	    		$output = new TestOutput
	    	);

	    $this->assertFileExists(vfsStream::url('folio/public/json/projects.json'));
	}

	/** @test */
	public function it_will_publish_theme_assets()
	{
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/_assets/test'));

		$this->console
	    	->find('portfolio:generate-nuxt')
	    	->run(
	    		new ArrayInput(['--destination' => vfsStream::url('folio/public')]),
	    		$output = new TestOutput
	    	);

	    $this->assertFileExists(vfsStream::url('folio/public/_assets/test'));
	}

	/** @test */
	public function it_will_publish_theme_assets_in_a_different_folder()
	{
		mkdir(vfsStream::url('folio/public/api'));
		mkdir(vfsStream::url('folio/public/themes'));

		$this->assertFileDoesNotExist(vfsStream::url('folio/public/api/json/projects.json'));
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/themes/test'));

		$this->console
	    	->find('portfolio:generate-nuxt')
	    	->run(
	    		new ArrayInput([
	    			'--destination' => vfsStream::url('folio/public/api'),
	    			'--theme-destination' => vfsStream::url('folio/public/themes')
	    		]),
	    		$output = new TestOutput
	    	);

	    $this->assertFileExists(vfsStream::url('folio/public/themes/test'));
		$this->assertFileExists(vfsStream::url('folio/public/api/json/projects.json'));
	}

	/** @test */
	public function it_will_publish_manifest()
	{
		$this->createImageFolderForSlug('my-project');
        $this->createDummyImage(vfsStream::url('folio/contents/images/my-project/my-image.jpg'));
        $this->createPublishedProjectFile(['slug' => 'my-project', 'markdown' => '![my-image]']);
        $this->createPublishedPageFile(['slug' => 'my-page']);

		$manifest = FolioManifest::fromJson();
        $this->assertCount(0, $manifest);
		
		$this->console
	    	->find('portfolio:generate-nuxt')
	    	->run(
	    		new ArrayInput([]),
	    		$output = new TestOutput
	    	);

		$manifest = FolioManifest::fromJson();
		$this->assertCount(13, $manifest);
		
		// Assets
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/_assets')));
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/_assets/test')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/_assets/test/app.css')));
		// Images
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/images')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/foo.jpg')));
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/images/my-project')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-image.jpg')));
		// Json
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/json')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/json/clients.json')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/json/links.json')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/json/pages.json')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/json/projects.json')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/json/settings.json')));
	}

	/** @test */
	public function it_will_publish_json_files_with_data()
	{
		$imageRelativePath = $this->createProjectWithImage('foo');
		$imageRelativePath = $this->createPageWithImage('bar');
		$this->defineLogoImageInSettings($imageRelativePath);
        
		$this->assertNotNull($baseUrlBuild = env('BASE_URL_BUILD'));
        $this->assertNotNull($baseUrl = env('BASE_URL'));
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/json/projects.json'));

		$this->console
	    	->find('portfolio:generate-nuxt')
	    	->run(new ArrayInput([]), $output = new TestOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/json/projects.json'));

		$jsonFiles = [
			['path' => 'public/json/pages.json'],
			['path' => 'public/json/projects.json'],
			['path' => 'public/json/settings.json'],
		];

		foreach ($jsonFiles as $jsonFile) {
			$json = file_get_contents(
				vfsStream::url('folio/' . $jsonFile['path'])
			);
	
			$this->assertStringContainsString(
				$this->getJsonEncodedUrl($baseUrlBuild), 
				$json
			);
			$this->assertStringNotContainsString(
				$this->getJsonEncodedUrl($baseUrl), 
				$json
			);
			$this->assertStringNotContainsString(
				$baseUrl, 
				$json
			);
		}
	}
}