<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use Symfony\Component\Console\Application;
use App\Testing\InteractsWithMarkdownFiles;
use App\Console\Commands\ProcessImageAssets;
use App\Console\Commands\PublishImageAssets;
use App\Themes\Publishers\Manifests\Manifest;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;

class ProcessImageAssetsTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ProcessImageAssets);
	}

	/** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new ProcessImageAssets);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('images:process', ProcessImageAssets::getDefaultName());
	}

	/** @test */
	public function it_can_be_executed()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImages = [
            vfsStream::url('folio/storage/images/foo-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo-compressed.webp'),
            vfsStream::url('folio/storage/images/foo-portrait-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo-portrait-compressed.webp'),
        ];

        app(UsedImagesFinder::class)
            ->whitelist()
            ->set(['foo.jpg']);

		$instance = new ProcessImageAssets;

        foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileDoesNotExist($destinationImagePath);
        }
	    
	    $instance->run(new ArrayInput([]), new NullOutput);

	    foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileExists($destinationImagePath);
        }
	}

	/** @test */
	public function it_can_be_executed_for_all_projects()
	{
		$this->createProjectWithImage('foo');
		$this->createProjectWithoutImage('bar');
		$this->createProjectWithImage('baz');

		$fooDestinationImages = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo/foo-compressed.webp'),
            vfsStream::url('folio/storage/images/foo/foo-portrait-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo/foo-portrait-compressed.webp'),
			vfsStream::url('folio/storage/images/foo/foo-portrait.jpg'),
		];

		$barDestinationImages = [
			vfsStream::url('folio/storage/images/baz/baz-compressed.jpg'),
            vfsStream::url('folio/storage/images/baz/baz-compressed.webp'),
            vfsStream::url('folio/storage/images/baz/baz-portrait-compressed.jpg'),
            vfsStream::url('folio/storage/images/baz/baz-portrait-compressed.webp'),
			vfsStream::url('folio/storage/images/baz/baz-portrait.jpg'),
		];
        
        $destinationImages = [
			...$fooDestinationImages,
			...$barDestinationImages
        ];

        $instance = new ProcessImageAssets;

        foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileDoesNotExist($destinationImagePath);
        }
	    
	    $instance->run(new ArrayInput([]), new NullOutput);

	    foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileExists($destinationImagePath);
        }

		$this->assertCount(2, $this->getAllFiles(vfsStream::url('folio/storage/images')));
		$this->assertCount(
			count($fooDestinationImages), 
			$this->getAllFiles(vfsStream::url('folio/storage/images/foo'))
		);
		$this->assertCount(
			count($barDestinationImages), 
			$this->getAllFiles(vfsStream::url('folio/storage/images/baz'))
		);
	}

	/** @test */
	public function it_can_be_executed_for_a_single_project()
	{
		$this->createProjectWithImage('foo');
		$this->createProjectWithoutImage('bar');
		$this->createProjectWithImage('baz');
        
        $destinationImages = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo/foo-compressed.webp'),
            vfsStream::url('folio/storage/images/foo/foo-portrait-compressed.jpg'),
            vfsStream::url('folio/storage/images/foo/foo-portrait-compressed.webp'),
			vfsStream::url('folio/storage/images/foo/foo-portrait.jpg'),
        ];

        $instance = new ProcessImageAssets;

        foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileDoesNotExist($destinationImagePath);
        }
	    
	    $instance->run(new ArrayInput(['--path' => 'foo']), new NullOutput);

	    foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileExists($destinationImagePath);
        }

		$this->assertCount(1, $this->getAllFiles(vfsStream::url('folio/storage/images')));
		$this->assertCount(
			count($destinationImages), 
			$this->getAllFiles(vfsStream::url('folio/storage/images/foo'))
		);
	}

	private function createProjectWithImage($slug)
	{
		return $this->createProject($slug, $markdown = "![{$slug}]");
	}

	private function createProjectWithoutImage($slug)
	{
		return $this->createProject($slug, $markdown = '');
	}

	private function createProject($slug, $markdown = '')
	{
		$this->createImageFolderForSlug($slug);
		
		$this->createDummyImage(
			$this->getSourceImage($slug)
		);
		
		$this->createPublishedProjectFile([
			'slug' => $slug, 
			'markdown' => $markdown
		]);
	}

	private function getSourceImage($slug)
	{
		return vfsStream::url("folio/contents/images/{$slug}/{$slug}.jpg");
	}

	private function getAllFiles($path)
	{
		return array_diff(
			scandir($path),
			['.', '..']
		);
	}
}