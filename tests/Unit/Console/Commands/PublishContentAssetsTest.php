<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\PublishContentAssets;
use App\Generators\FolioManifest;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PublishContentAssetsTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithMarkdownFiles;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PublishContentAssets);
	}

	/** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new PublishContentAssets);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('publish:content', PublishContentAssets::getDefaultName());
	}

	/** @test */
	public function it_generates_html()
	{
		$this->createPublishedProjectFile(['slug' => 'foo']);
		$this->createPublishedPageFile(['slug' => 'about']);

		$instance = new PublishContentAssets;

		$this->assertFileDoesNotExist(vfsStream::url('folio/public/project'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/page'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/index.html'));
		
	    $instance->run(new ArrayInput(['--format' => 'html']), new NullOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/project'));
	    $this->assertFileExists(vfsStream::url('folio/public/project/foo.html'));
	    $this->assertFileExists(vfsStream::url('folio/public/page'));
	    $this->assertFileExists(vfsStream::url('folio/public/page/about.html'));
	    $this->assertFileExists(vfsStream::url('folio/public/index.html'));
	}

	/** @test */
	public function it_generates_html_with_different_destination_folder()
	{
		mkdir(vfsStream::url('folio/public/html'));
		$this->createPublishedProjectFile(['slug' => 'foo']);
		$this->createPublishedPageFile(['slug' => 'about']);

		$instance = new PublishContentAssets;

		$this->assertFileDoesNotExist(vfsStream::url('folio/public/html/project'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/html/page'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/html/index.html'));

	    $instance->run(new ArrayInput([
	    	'--format' => 'html',
	    	'--destination' => vfsStream::url('folio/public/html')
	    ]), new NullOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/html/project'));
	    $this->assertFileExists(vfsStream::url('folio/public/html/project/foo.html'));
	    $this->assertFileExists(vfsStream::url('folio/public/html/page'));
	    $this->assertFileExists(vfsStream::url('folio/public/html/page/about.html'));
	    $this->assertFileExists(vfsStream::url('folio/public/html/index.html'));
	}

	/** @test */
	public function it_generates_json()
	{
		$this->createPublishedProjectFile(['slug' => 'foo']);
		$this->createPublishedPageFile(['slug' => 'about']);

		$instance = new PublishContentAssets;

		$this->assertFileDoesNotExist(vfsStream::url('folio/public/json/projects.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/json/pages.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/json/settings.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/json/links.json'));

	    $instance->run(new ArrayInput(['--format' => 'json']), new NullOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/json/projects.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/json/pages.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/json/settings.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/json/links.json'));
	}

	/** @test */
	public function it_generates_json_with_different_destination_folder()
	{
		mkdir(vfsStream::url('folio/public/api'));

		$instance = new PublishContentAssets;

		$this->assertFileDoesNotExist(vfsStream::url('folio/public/api/json/projects.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/api/json/pages.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/api/json/settings.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/api/json/links.json'));

	    $instance->run(new ArrayInput([
	    	'--format' => 'json',
	    	'--destination' => vfsStream::url('folio/public/api')
	    ]), new NullOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/api/json/projects.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/api/json/pages.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/api/json/settings.json'));
	    $this->assertFileExists(vfsStream::url('folio/public/api/json/links.json'));
	}
}