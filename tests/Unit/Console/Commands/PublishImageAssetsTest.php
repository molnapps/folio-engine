<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use App\Console\Commands\PublishImageAssets;
use App\Themes\Publishers\Manifests\Manifest;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class PublishImageAssetsTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PublishImageAssets);
	}

	/** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new PublishImageAssets);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('publish:images', PublishImageAssets::getDefaultName());
	}

	/** @test */
	public function it_can_be_executed()
	{
		$instance = new PublishImageAssets;

	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/images/foo.jpg'));
	    
	    $instance->run(
			new ArrayInput(['--publisher' => 'copy']), 
			new NullOutput
		);

	    $this->assertFileExists(vfsStream::url('folio/public/images/foo.jpg'));
	}

	/** @test */
	public function it_can_be_executed_with_optimized_publisher_all()
	{
		$instance = new PublishImageAssets;

		$expectedFiles = [
			vfsStream::url('folio/public/images/foo-compressed.jpg'),
			vfsStream::url('folio/public/images/foo-compressed.webp'),
		];

		foreach ($expectedFiles as $expectedFile) {
			$this->assertFileDoesNotExist($expectedFile);
		}
	    
	    $instance->run(
			$this->optimized(['--only-used' => false]), 
			new NullOutput
		);

	    foreach ($expectedFiles as $expectedFile) {
			$this->assertFileExists($expectedFile);
		}
	}

	/** @test */
	public function it_publishes_optimized_files_again_if_manually_deleted()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo/foo.jpg');
		$processedImage = vfsStream::url('folio/storage/images/foo/foo-compressed.jpg');
		$publicImage = vfsStream::url('folio/public/images/foo/foo-compressed.jpg');

		ProjectBuilder::fromSlug('foo')
			->withUsedImages('foo')
			->create();
		
		$instance = new PublishImageAssets;
		
		$this->assertFileDoesNotExist($processedImage);
		$this->assertFileDoesNotExist($publicImage);

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$this->assertFileExists($processedImage);
		$this->assertFileExists($publicImage);

		unlink($processedImage);
		$this->assertFileDoesNotExist($processedImage);

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$this->assertFileExists($processedImage);
		$this->assertFileExists($publicImage);
	}

	/** @test */
	public function it_can_be_executed_with_optimized_publisher_only_used_images()
	{
		$instance = new PublishImageAssets;

		$helper = ProjectBuilder::fromSlug('foo')
			->withUsedImages('bar')
			->withNotUsedImages('foo')
			->create();

		$helper->assertBefore($this);

		$instance->run(
			$this->optimized(['--only-used' => true]), 
			new NullOutput
		);

		$helper->assertAfter($this);

		$this->assertEquals(['foo'], $this->getAllPublicFiles());
		$this->assertCount(4, $this->getAllPublicFiles('foo'));
	}

	/** @test */
	public function it_publishes_website_feature_from_hero_only_used()
	{
		$instance = new PublishImageAssets;

		ProjectBuilder::fromSlug('foo')
			->withUsedImages('hero')
			->create();

		$manifest = $this->getManifestInstance();
		$storageFiles = [
			vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg'),
			vfsStream::url('folio/storage/images/foo/hero-website-feature-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/hero-website-feature-compressed.webp'),
		];
		$publicFiles = [
			vfsStream::url('folio/public/images/foo/hero-website-feature-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/hero-website-feature-compressed.webp'),
		];
		
		foreach ($storageFiles as $storageFile) {
			$this->assertFileDoesNotExist($storageFile);
		}

		foreach ($publicFiles as $publicFile) {
			$this->assertFileDoesNotExist($publicFile);
		}

		$this->assertManifestHasNot($storageFiles[0], $manifest);

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		foreach ($storageFiles as $storageFile) {
			$this->assertFileExists($storageFile);
		}
		
		foreach ($publicFiles as $publicFile) {
			$this->assertFileExists($publicFile);
		}

		$this->assertManifestHas($storageFiles[0], $manifest->refresh());

		$this->assertEquals(['foo'], $this->getAllPublicFiles());
		$this->assertCount(6, $this->getAllPublicFiles('foo'));
	}

	/** @test */
	public function it_publishes_website_feature_from_hero_specified_path()
	{
		$instance = new PublishImageAssets;

		ProjectBuilder::fromSlug('foo')
			->withUsedImages('hero')
			->create();

		ProjectBuilder::fromSlug('bar')
			->withUsedImages('hero')
			->create();

		$manifest = $this->getManifestInstance();
		$storageFiles = [
			vfsStream::url('folio/storage/images/bar/hero-website-feature.jpg'),
			vfsStream::url('folio/storage/images/bar/hero-website-feature-compressed.jpg'),
			vfsStream::url('folio/storage/images/bar/hero-website-feature-compressed.webp')
		];
		$publicFiles = [
			vfsStream::url('folio/public/images/bar/hero-website-feature-compressed.jpg'),
			vfsStream::url('folio/public/images/bar/hero-website-feature-compressed.webp'),
		];

		foreach ($publicFiles as $publicFile) {
			$this->assertFileDoesNotExist($publicFile);
		}
		$this->assertManifestHasNot($storageFiles[0], $manifest);

		$instance->run(
			$this->optimized(['--path' => 'bar']), 
			new NullOutput
		);

		foreach ($publicFiles as $publicFile) {
			$this->assertFileExists($publicFile);
		}
		$this->assertManifestHas($storageFiles[0], $manifest->refresh());

		$this->assertEquals(['bar'], $this->getAllPublicFiles());
		$this->assertCount(6, $this->getAllPublicFiles('bar'));
	}

	/** @test */
	public function it_can_be_executed_with_optimized_publisher_only_used_formats()
	{
		$helper = ProjectBuilder::fromSlug('foo')
			->withLandscapeUsedImage('foo')
			->create();
		
		$helper->assertBefore($this);

		$instance = new PublishImageAssets;
	    $instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$helper->assertAfter($this);
	}

	/** @test */
	public function it_allows_to_specify_a_relative_path_and_updates_manifest()
	{
		$this->unlinkDefaultImage();

		$fooHelper = ProjectBuilder::fromSlug('foo')
			->withUsedImages('foo')
			->create();

		$barHelper = ProjectBuilder::fromSlug('bar')
			->withUsedImages('bar')
			->create();
		
		$sourceImages = [
			'foo' => $fooHelper->getSourceImagePath('foo'),
			'foo2' => $fooHelper->getSourceImagePath('foo2'),
			'bar' => $barHelper->getSourceImagePath('bar'),
		];

		$instance = new PublishImageAssets;

		$masterManifest = vfsStream::url('folio/manifests/manifest.img.json');
		
	    $this->assertFileDoesNotExist($masterManifest);
	    
	    $instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$this->assertFileExists($masterManifest);

		$manifest = new Manifest($masterManifest);
	    $manifest->loadIfNotLoaded();

		$this->assertManifestHas($sourceImages['foo'], $manifest);
		$this->assertManifestHasNot($sourceImages['foo2'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);
		
		$fooHelper->replaceUsedImage('foo', 'foo2');
		
		$this->tearDownApp();
		$this->setUpApp();

		$instance->run(
			$this->optimized(['--path' => 'foo']), 
			new NullOutput
		);

		$this->assertFileExists($masterManifest);
		
	    $manifest->refresh();

		$this->assertManifestHasNot($sourceImages['foo'], $manifest);
		$this->assertManifestHas($sourceImages['foo2'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);
	}

	/** @test */
	public function it_allows_to_specify_a_relative_path_and_updates_folder_files()
	{
		$this->unlinkDefaultImage();

		$fooHelper = ProjectBuilder::fromSlug('foo')
			->withUsedImages('foo')
			->create();
		
		ProjectBuilder::fromSlug('bar')
			->withUsedImages('bar')
			->create();

		$instance = new PublishImageAssets;

		$expectedFiles = [
			// Folders
			vfsStream::url('folio/public/images/foo'),
			vfsStream::url('folio/public/images/bar'),
			// Images
			vfsStream::url('folio/public/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo-compressed.webp'),
			vfsStream::url('folio/public/images/bar/bar-compressed.jpg'),
			vfsStream::url('folio/public/images/bar/bar-compressed.webp'),
			// Formats
			vfsStream::url('folio/public/images/foo/foo-portrait-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo-portrait-compressed.webp'),
			vfsStream::url('folio/public/images/bar/bar-portrait-compressed.jpg'),
			vfsStream::url('folio/public/images/bar/bar-portrait-compressed.webp'),
		];

		foreach ($expectedFiles as $expectedFile) {
			$this->assertFileDoesNotExist($expectedFile);
		}

	    $instance->run(
			$this->optimized(), 
			new NullOutput
		);

		foreach ($expectedFiles as $expectedFile) {
			$this->assertFileExists($expectedFile);
		}

		$fooHelper->replaceUsedImage('foo', 'foo2');
		
		$this->tearDownApp();
		$this->setUpApp();

		$instance->run($this->optimized(['--path' => 'foo']), new NullOutput);

		$notExpectedFiles = [
			vfsStream::url('folio/public/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo-compressed.webp'),
			vfsStream::url('folio/public/images/foo/foo-portrait-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo-portrait-compressed.webp'),
		];

		$expectedFiles = [
			// Folders
			vfsStream::url('folio/public/images/foo'),
			vfsStream::url('folio/public/images/bar'),
			// Images
			vfsStream::url('folio/public/images/foo/foo2-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo2-compressed.webp'),
			vfsStream::url('folio/public/images/bar/bar-compressed.jpg'),
			vfsStream::url('folio/public/images/bar/bar-compressed.webp'),
			// Formats
			vfsStream::url('folio/public/images/foo/foo2-portrait-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo2-portrait-compressed.webp'),
			vfsStream::url('folio/public/images/bar/bar-portrait-compressed.jpg'),
			vfsStream::url('folio/public/images/bar/bar-portrait-compressed.webp'),
		];

		foreach ($notExpectedFiles as $notExpectedFile) {
			$this->assertFileDoesNotExist($notExpectedFile);
		}

		foreach ($expectedFiles as $expectedFile) {
			$this->assertFileExists($expectedFile);
		}
	}

	/** @test */
	public function it_erases_backup_copy()
	{
		$instance = new PublishImageAssets;

		$imagesFolder = vfsStream::url('folio/public/images');
		$imagesBackupFolder = vfsStream::url('folio/public/images_backup');

		$this->assertFileDoesNotExist($imagesFolder);
		$this->assertFileDoesNotExist($imagesBackupFolder);

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$this->assertFileExists($imagesFolder);
		$this->assertFileDoesNotExist($imagesBackupFolder);
		
		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$this->assertFileExists($imagesFolder);
		$this->assertFileDoesNotExist($imagesBackupFolder);
	}

	/** @test */
	public function it_updates_manifest_when_deleting_files()
	{
		$this->unlinkDefaultImage();

		$fooHelper = ProjectBuilder::fromSlug('foo')->withUsedImages('foo')->create();
		$barHelper = ProjectBuilder::fromSlug('bar')->withUsedImages('bar')->create();

		$sourceImages = [
			'foo' => $fooHelper->getSourceImagePath('foo'),
			'bar' => $barHelper->getSourceImagePath('bar'),
		];

		$instance = new PublishImageAssets;

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$manifest = $this->getManifestInstance();

		$this->assertManifestHas($sourceImages['foo'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);

		$fooHelper->deleteSourceImage('foo');
		
	    $instance->run(
			$this->optimized(), 
			new NullOutput
		);

	    $manifest->refresh();

		$this->assertManifestHasNot($sourceImages['foo'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);
	}

	/** @test */
	public function it_updates_manifest_when_deleting_folders()
	{
		$this->unlinkDefaultImage();

		$fooHelper = ProjectBuilder::fromSlug('foo')
			->withUsedImages('foo')
			->create();
		
		$barHelper = ProjectBuilder::fromSlug('bar')
			->withUsedImages('bar')
			->create();

		$sourceImages = [
			'foo' => $fooHelper->getSourceImagePath('foo'),
			'bar' => $barHelper->getSourceImagePath('bar'),
		];

		$instance = new PublishImageAssets;

		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		$manifest = $this->getManifestInstance();

		$this->assertManifestHas($sourceImages['foo'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);

	    $fooHelper
			->deleteSourceImage('foo')
			->deleteSourceFolder();
	    
	    $instance->run(
			$this->optimized(), 
			new NullOutput
		);

	    $manifest->refresh();

		$this->assertManifestHasNot($sourceImages['foo'], $manifest);
		$this->assertManifestHas($sourceImages['bar'], $manifest);
	}

	/** @test */
	public function it_deletes_all_images()
	{
		$sourceImages = [
			vfsStream::url('folio/contents/images/foo/foo.jpg'),
			vfsStream::url('folio/contents/images/foo/bar.svg'),
		];

		$publicImages = [
			vfsStream::url('folio/public/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/public/images/foo/foo-compressed.webp'),
			vfsStream::url('folio/public/images/foo/bar.svg')
		];

		$imagesBackupFolder = vfsStream::url('folio/public/images_backup');

		$this->unlinkDefaultImage();

		$this->createImageFolderForSlug('foo');

		foreach ($sourceImages as $sourceImage) {
			$this->createDummyImage($sourceImage);
		}

		$this->createPublishedProjectFile([
			'slug' => 'foo',
			'markdown' => '![foo] ![bar]'
		]);

		$instance = new PublishImageAssets;
		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		foreach ($publicImages as $publicImage) {
			$this->assertFileExists($publicImage);
		}

		$instance->run(
			$this->symlink(), 
			new NullOutput
		);

		foreach ($publicImages as $publicImage) {
			$this->assertFileDoesNotExist($publicImage);
		}

		$this->assertFileDoesNotExist( $imagesBackupFolder );
	}

	/** @test */
	public function it_does_not_override_cropped_image_with_its_compressed_version()
	{
		// Given that I have a source picture
		ProjectBuilder::fromSlug('foo')->withUsedImages('foo')->create();
		
		// And I generate crops and compressed version
		$instance = new PublishImageAssets;
		$instance->run(
			$this->optimized(), 
			new NullOutput
		);

		// Then the compressed version should not override the original cropped version
		$expected = [
			"foo-compressed.jpg",
			"foo-compressed.webp",
			"foo-portrait-compressed.jpg",
			"foo-portrait-compressed.webp",
			"foo-portrait.jpg",
		];
		
		$this->assertEquals($expected, $this->getAllStorageFiles('foo'));
	}

	private function getManifestInstance() : Manifest
	{
		$manifest = new Manifest(
			vfsStream::url('folio/manifests/manifest.img.json')
		);
	    
	    return $manifest->loadIfNotLoaded();
	}

	private function assertManifestHas($path, $manifest)
	{
		$this->assertTrue(
			$manifest->has($path),
			"Manifest does not have {$path}"
		);
	}

	private function assertManifestHasNot($path, $manifest)
	{
		$this->assertFalse(
			$manifest->has($path),
			"Manifest has {$path}"
		);
	}
	
	private function optimized(array $override = [])
	{
		return new ArrayInput(
			array_merge(
				[
					'--publisher' => 'optimized', 
					'--only-used' => true
				], 
				$override
			)
		);
	}

	private function symlink(array $override = [])
	{
		return new ArrayInput(
			array_merge(
				['--publisher' => 'symlink'], 
				$override
			)
		);
	}

	private function getAllStorageFiles($relativePath = null)
	{
		$basePath = vfsStream::url('folio/storage/images');

		$path = $relativePath 
			? $basePath . '/' . $relativePath
			: $basePath;

		return array_values(
			array_diff(
				scandir($path),
				['.', '..']
			)
		);
	}

	private function getAllPublicFiles($relativePath = null)
	{
		$basePath = vfsStream::url('folio/public/images');

		$path = $relativePath 
			? $basePath . '/' . $relativePath
			: $basePath;

		return array_values(
			array_diff(
				scandir($path),
				['.', '..']
			)
		);
	}
}