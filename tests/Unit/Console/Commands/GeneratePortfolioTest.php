<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\GeneratePortfolio;
use App\Testing\InteractsWithFileSystem;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class GeneratePortfolioTest extends TestCase
{
	use InteractsWithFileSystem;

	private $commands = [];
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new GeneratePortfolio);
	}

	/** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new GeneratePortfolio);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('portfolio:generate', GeneratePortfolio::getDefaultName());
	}

	/** @test */
	public function it_can_be_executed()
	{
		$instance = new GeneratePortfolio;

		$this->createConsoleApplication($instance);

		$this->assertEquals([], $this->commands);

	    $instance->run(new ArrayInput([]), new NullOutput);

	    $this->assertEquals([
	    	'publish:theme',
	    	'publish:images', 
	    	'publish:content'
	    ], $this->commands);
	}

	/** @test */
	public function it_can_be_executed_without_images()
	{
		$instance = new GeneratePortfolio;

		$this->createConsoleApplication($instance);

		$this->assertEquals([], $this->commands);

	    $instance->run(new ArrayInput([
	    	'--no-images' => true
	    ]), new NullOutput);

	    $this->assertEquals([
	    	'publish:theme', 
	    	'publish:content'
	    ], $this->commands);
	}

	/** @test */
	public function it_uses_default_base_url_by_default()
	{
		$instance = new GeneratePortfolio;

		$this->createConsoleApplication($instance);

		$this->assertEquals('https://folio-engine.test', $_SERVER['BASE_URL']);

	    $instance->run(new ArrayInput([]), new NullOutput);

	    $this->assertEquals('https://folio-engine.test', $_SERVER['BASE_URL']);
	}

	private function createConsoleApplication(GeneratePortfolio $instance)
	{
		$application = new Application;

		$application->add( $this->createCommandStub('publish:content') );
		$application->add( $this->createCommandStub('publish:theme') );
		$application->add( $this->createCommandStub('publish:images') );

		$instance->setApplication($application);

		return $application;
	}

	private function createCommandStub($name)
	{
		return new FooCommand($name, function ($name) {
			$this->commands[] = $name;
		});
	}
}

class FooCommand extends Command
{
	private $name;
	private $callback;

    public function __construct($name, $callback)
    {
    	parent::__construct();
    	$this->name = $name;
    	$this->callback = $callback;
    }

    protected function configure()
	{
		$this
			->setDescription('Test command.')
			->setHelp('This command will be used only by tests');

		$this->addOption(
			'format',
			$shortcut = null,
			InputOption::VALUE_REQUIRED,
			'Specify format',
			'html'
		);

		$this->addOption(
			'publisher',
			$shortcut = null,
			InputOption::VALUE_REQUIRED,
			'Publication of assets will use copy instead of symlinks',
			'symlink'
		);

		$this->addOption(
			'only-used',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Publish only used images',
			false
		);
	}

    public function getName()
    {
    	return $this->name;
    }

    protected function execute(
    	\Symfony\Component\Console\Input\InputInterface $input, 
    	\Symfony\Component\Console\Output\OutputInterface $output
    ) {
    	($this->callback)($this->name);
        return Command::SUCCESS;
    }
}