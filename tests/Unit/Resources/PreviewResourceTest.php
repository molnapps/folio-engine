<?php

namespace Tests\Unit\Resources;

use App\Resources\PreviewResource;
use App\Domain\Preview;
use App\Testing\PreviewTest;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PreviewResourceTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PreviewResource(new Preview));
	}

    /** @test */
	public function it_returns_image()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));

        $instance = new PreviewResource(
            new Preview(
                PreviewTest::previewImage('foo/foo.jpg')
            )
        );

        $result = $instance->toArray();

	    $this->assertIsArray($result);
        
        $this->assertArrayHasKey('type', $result);
        $this->assertEquals('image', $result['type']);

        $this->assertArrayHasKey('media', $result);
        $this->assertIsArray($result['media']);
        $this->assertEquals(['preview'], $result['media']);

        $this->assertArrayHasKey('resource', $result);
        $this->assertIsArray($result['resource']);
        $this->assertEquals([
            'src' => $this->baseUrlImages . '/foo/foo.jpg',
            'width' => 50,
            'height' => 50
        ], $result['resource']);
	}

    /** @test */
	public function it_returns_video()
	{
        $instance = new PreviewResource(
            new Preview(
                PreviewTest::vimeoVideo($expectedUrl = 'https://www.vimeo.com/12345')
            )
        );

        $result = $instance->toArray();

	    $this->assertIsArray($result);
        
        $this->assertArrayHasKey('type', $result);
        $this->assertEquals('video', $result['type']);

        $this->assertArrayHasKey('media', $result);
        $this->assertIsArray($result['media']);
        $this->assertEquals(['preview'], $result['media']);

        $this->assertArrayHasKey('resource', $result);
        $this->assertIsArray($result['resource']);
        $this->assertEquals([
            'src' => $expectedUrl,
            'width' => 0,
            'height' => 0
        ], $result['resource']);
	}
}