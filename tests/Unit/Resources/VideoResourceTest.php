<?php

namespace Tests\Unit\Resources;

use App\Resources\VideoResource;
use App\Domain\Video;
use App\Testing\VideoTest;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class VideoResourceTest extends TestCase
{
    use InteractsWithFileSystem;
    
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new VideoResource(new Video));
	}

    /** @test */
	public function it_returns_video()
	{
        $instance = new VideoResource(
            new Video(
                VideoTest::vimeoVideo($expectedId = '54321')
            )
        );

        $result = $instance->toArray();

	    $this->assertIsArray($result);

        $this->assertArrayHasKey('provider', $result);
        $this->assertEquals('vimeo', $result['provider']);

        $this->assertArrayHasKey('id', $result);
        $this->assertEquals($expectedId, $result['id']);

        $this->assertArrayHasKey('slug', $result);
        $this->assertEquals('foo-video', $result['slug']);

        $this->assertArrayHasKey('size', $result);
        $this->assertIsArray($result['size']);
        $this->assertEquals([
            'width' => 1920, 
            'height' => 1080
        ], $result['size']);
	}
}