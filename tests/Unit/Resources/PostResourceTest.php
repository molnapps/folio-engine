<?php

namespace Tests\Unit\Resources;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Post;
use App\Testing\PreviewTest;
use App\Resources\PostResource;
use Tests\TestCase;

class PostResourceTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull($this->getInstance());
	}

	/** @test */
	public function it_implements_arrayable_interface()
	{
	    $this->assertInstanceOf(
	    	\Illuminate\Contracts\Support\Arrayable::class, 
	    	$this->getInstance()
	    );
	}

	/** @test */
	public function it_can_be_rendered_to_array()
	{
	    $this->assertIsArray($this->getInstance()->toArray());
	}

	/** @test */
	public function it_returns_type()
	{
		$result = $this->getInstance(['type' => 'foo'])->toArray();

		$this->assertArrayHasKey('type', $result);
	    $this->assertEquals('foo', $result['type']);
	}

	/** @test */
	public function it_returns_slug()
	{
		$result = $this->getInstance(['slug' => 'my-slug'])->toArray();

		$this->assertArrayHasKey('slug', $result);
	    $this->assertEquals('my-slug', $result['slug']);
	}

	/** @test */
	public function it_returns_path()
	{
		$result = $this->getInstance(['path' => '/foo/bar'])->toArray();

		$this->assertArrayHasKey('path', $result);
	    $this->assertEquals('/foo/bar', $result['path']);
	}

	/** @test */
	public function it_returns_title()
	{
		$result = $this->getInstance(['title' => 'My title'])->toArray();

		$this->assertArrayHasKey('title', $result);
	    $this->assertEquals('My title', $result['title']);
	}

	/** @test */
	public function it_returns_client()
	{
		$result = $this->getInstance(['client' => 'my-client'])->toArray();

		$this->assertArrayHasKey('client', $result);
	    $this->assertIsArray($result['client']);
		$this->assertArrayHasKey('slug', $result['client']);
		$this->assertEquals('my-client', $result['client']['slug']);
		$this->assertArrayHasKey('title', $result['client']);
		$this->assertEquals('My-client', $result['client']['title']);
		$this->assertArrayHasKey('logo', $result['client']);
		$this->assertNull($result['client']['logo']);
	}

	/** @test */
	public function it_returns_excerpt()
	{
		$result = $this->getInstance(['excerpt' => 'My excerpt'])->toArray();

		$this->assertArrayHasKey('excerpt', $result);
	    $this->assertEquals('My excerpt', $result['excerpt']);
	}

	/** @test */
	public function it_returns_lead()
	{
		$result = $this->getInstance([
			'lead_separator' => '<!--more-->', 
			'content' => 'My lead<!--more-->My body'
		])->toArray();

		$this->assertArrayHasKey('lead', $result);
	    $this->assertEquals('<p>My lead</p>', $result['lead']);
	}

	/** @test */
	public function it_returns_body()
	{
		$result = $this->getInstance([
			'lead_separator' => '<!--more-->', 
			'content' => 'My lead<!--more-->My body'
		])->toArray();

		$this->assertArrayHasKey('body', $result);
	    $this->assertEquals('<p>My body</p>', $result['body']);
	}

	/** @test */
	public function it_returns_credits()
	{
		$result = $this->getInstance([
			'credits' => 'My credits'
		])->toArray();

		$this->assertArrayHasKey('credits', $result);
	    $this->assertEquals('<p>My credits</p>', $result['credits']);
	}

	/** @test */
	public function it_returns_preview()
	{
		$result = $this->getInstance([
			'previews' => [
				PreviewTest::previewImage($this->getImageFileName())
			]
		])->toArray();

		$this->assertArrayHasKey('preview', $result);
	    $this->assertEquals([
	    	'src' => $this->baseUrlImages . '/foo.jpg', 
	    	'width' => '120', 
	    	'height' => '20'
	   	], $result['preview']);
	}

	/** @test */
	public function it_returns_hero()
	{
		$result = $this->getInstance([
			'previews' => [
				PreviewTest::heroImage($this->getImageFileName())
			]
		])->toArray();

		$this->assertArrayHasKey('hero', $result);
	    $this->assertEquals([
	    	'src' => $this->baseUrlImages . '/foo.jpg', 
	    	'width' => '120', 
	    	'height' => '20'
	   	], $result['hero']);
	}

	/** @test */
	public function it_returns_published_at()
	{
		$result = $this->getInstance([
			'published_at' => '15 february 2020'
		])->toArray();

		$this->assertArrayHasKey('published_at', $result);
	    $this->assertEquals('2020-02-15 00:00:00', $result['published_at']);
	}

	/** @test */
	public function it_returns_keys_with_empty_post()
	{
		$result = $this->getInstance()->toArray();

		$attr = [
			'type', 'path', 'slug', 'title', 'client',
			'excerpt', 'lead', 'body', 'credits', 
			'preview', 'hero', 'published_at'
		];

		foreach ($attr as $key) {
			$this->assertArrayHasKey($key, $result);
	    	$this->assertNull($result[$key]);
		}
	}

	private function getInstance(array $attr = []) 
	{
		return new PostResource(new Post($attr));
	}
}