<?php

namespace Tests\Unit\Resources;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Client;
use App\Resources\ClientResource;
use Tests\TestCase;

class ClientResourceTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull($this->getInstance());
	}

	/** @test */
	public function it_implements_arrayable_interface()
	{
	    $this->assertInstanceOf(
	    	\Illuminate\Contracts\Support\Arrayable::class, 
	    	$this->getInstance()
	    );
	}

	/** @test */
	public function it_can_be_rendered_to_array()
	{
	    $this->assertIsArray($this->getInstance()->toArray());
	}

	/** @test */
	public function it_returns_slug()
	{
		$result = $this->getInstance(['slug' => 'my-slug'])->toArray();

		$this->assertArrayHasKey('slug', $result);
	    $this->assertEquals('my-slug', $result['slug']);
	}

	/** @test */
	public function it_returns_title()
	{
		$result = $this->getInstance(['title' => 'My title'])->toArray();

		$this->assertArrayHasKey('title', $result);
	    $this->assertEquals('My title', $result['title']);
	}

	/** @test */
	public function it_returns_logo()
	{
		$result = $this->getInstance([
			'logo' => $this->getImageFileName()
		])->toArray();

		$this->assertArrayHasKey('logo', $result);
	    $this->assertEquals([
	    	'src' => $this->baseUrlImages . '/foo.jpg', 
	    	'width' => '120', 
	    	'height' => '20'
	   	], $result['logo']);
	}

	/** @test */
	public function it_returns_keys_with_empty_post()
	{
		$result = $this->getInstance()->toArray();

        $attr = [
			'slug', 'title', 'logo'
		];

		foreach ($attr as $key) {
			$this->assertArrayHasKey($key, $result);
	    	$this->assertNull($result[$key]);
		}
	}

	private function getInstance(array $attr = []) 
	{
		return new ClientResource(new Client($attr));
	}
}