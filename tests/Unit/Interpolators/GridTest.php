<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Interpolator;
use App\Interpolators\Grid;
use Tests\TestCase;

class GridTest extends TestCase
{
    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Grid);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Grid);
	}

    /** @test */
	public function it_replaces_slideshow_tokens()
	{
        $grid = new Grid;

	    $content = $grid->text('foo
	    	@grid
	    	bar
	    	@endgrid
	    	baz');

	    $this->assertStringNotContainsString('@grid', $content);
	    $this->assertStringNotContainsString('@endgrid', $content);

	    $this->assertStringContainsString('<div class="Post-grid Post-grid--cols-3">', $content);
	    $this->assertStringContainsString('</div>', $content);
	}

    /** @test */
	public function it_replaces_slideshow_tokens_with_3_columns()
	{
        $grid = new Grid;

	    $content = $grid->text('foo
	    	@grid(cols:3)
	    	bar
	    	@endgrid
	    	baz');

	    $this->assertStringNotContainsString('@grid(cols:3)', $content);
	    $this->assertStringNotContainsString('@endgrid', $content);

	    $this->assertStringContainsString('<div class="Post-grid Post-grid--cols-3">', $content);
	    $this->assertStringContainsString('</div>', $content);
	}

    /** @test */
	public function it_replaces_slideshow_tokens_with_2_columns()
	{
        $grid = new Grid;

	    $content = $grid->text('foo
	    	@grid(cols:2)
	    	bar
	    	@endgrid
	    	baz');

	    $this->assertStringNotContainsString('@grid(cols:2)', $content);
	    $this->assertStringNotContainsString('@endgrid', $content);

	    $this->assertStringContainsString('<div class="Post-grid Post-grid--cols-2">', $content);
	    $this->assertStringContainsString('</div>', $content);
	}
}