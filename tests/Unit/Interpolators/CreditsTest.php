<?php

namespace Tests\Unit\Interpolators;

use App\Interpolators\Interpolator;
use App\Interpolators\Credits;
use Tests\TestCase;

class CreditsTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Credits);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Credits);
	}

	/** @test */
	public function it_replaces_credits_tokens()
	{
	    $credits = new Credits;

	    $content = $credits->text('foo
	    	{credits}
	    	bar
	    	{/credits}
	    	baz');

	    $this->assertStringNotContainsString('{credits}', $content);
	    $this->assertStringNotContainsString('{/credits}', $content);

	    $this->assertStringContainsString('<div class="Credits">', $content);
	    $this->assertStringContainsString('</div>', $content);
	}
}