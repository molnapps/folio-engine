<?php

namespace Tests\Unit\Interpolators;

use Tests\TestCase;
use App\Domain\Post;
use App\Domain\Images\Image;
use App\Interpolators\Images;
use org\bovigo\vfs\vfsStream;
use App\Interpolators\Interpolator;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

class ImagesTest extends TestCase
{
	private $baseSourcePath = 'vfs://folio/contents/images';
	private $baseUrlImages;
	private $root;
	private $basePath;

	/** @before */
	public function setUpBaseUrlImages()
	{
		$this->baseUrlImages = env('IMAGES_DESTINATION_URL');

		$this->assertNotNull($this->baseUrlImages);
	}
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'images' => [
					'yolo' => [
						'foo.jpg' => 'foo',
						'bar.jpg' => 'bar',
						'foo-foo.jpg' => 'foo-foo',
						'foo.foo.jpg' => 'foo.foo',
						'foo-2017.jpg' => 'foo-2017',
						'foobar.gif' => 'foobar',
						'foobaz.png' => 'foobaz',
						'foobarbaz.svg' => 'foobarbaz',
					]
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Images);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Images);
	}

	/** @test */
	public function it_handles_no_iamges()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('foobar');

	    $this->assertEquals('foobar', $content);
	}

	/** @test */
	public function it_finds_one_image()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foo]');

	    $this->assertStringContainsString(
	    	'[foo]: ' . $this->baseUrlImages . '/yolo/foo.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_one_image_dash()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foo-foo]');

	    $this->assertStringContainsString(
	    	'[foo-foo]: ' . $this->baseUrlImages . '/yolo/foo-foo.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_one_image_dot()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foo.foo]');

	    $this->assertStringContainsString(
	    	'[foo.foo]: ' . $this->baseUrlImages . '/yolo/foo.foo.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_one_image_numbers()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foo-2017]');

	    $this->assertStringContainsString(
	    	'[foo-2017]: ' . $this->baseUrlImages . '/yolo/foo-2017.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_two_images()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foo]
	    ![bar]');

	    $this->assertStringContainsString(
	    	'[foo]: ' . $this->baseUrlImages . '/yolo/foo.jpg', 
	    	$content
	    );
	    $this->assertStringContainsString(
	    	'[bar]: ' . $this->baseUrlImages . '/yolo/bar.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_handles_image_that_cannot_be_resolved()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![baz]');

	    $this->assertEquals('![baz]', $content);
	}

	/** @test */
	public function it_handles_image_that_cannot_be_resolved_and_existing_image()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![baz]
	    ![foo]');

	    $this->assertStringContainsString(
	    	'[foo]: ' . $this->baseUrlImages . '/yolo/foo.jpg', 
	    	$content
	    );
	    $this->assertStringNotContainsString(
	    	'[baz]: ' . $this->baseUrlImages . '/yolo/baz.jpg', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_gif_image()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foobar]');

	    $this->assertStringContainsString(
	    	'[foobar]: ' . $this->baseUrlImages . '/yolo/foobar.gif', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_png_image()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foobaz]');

	    $this->assertStringContainsString(
	    	'[foobaz]: ' . $this->baseUrlImages . '/yolo/foobaz.png', 
	    	$content
	    );
	}

	/** @test */
	public function it_finds_svg_image()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('![foobarbaz]');

	    $this->assertStringContainsString(
	    	'[foobarbaz]: ' . $this->baseUrlImages . '/yolo/foobarbaz.svg', 
	    	$content
	    );
	}

	/** @test */
	public function it_returns_all_used_images()
	{
	    $content = new Images(
	    	new Post(['slug' => 'yolo'])
	    );

	    $usedImages = $content->getUsedImages('![foo] ![bar]');

		$this->assertIsArray($usedImages);
		$this->assertCount(2, $usedImages);

		$this->assertInstanceOf(UsedImagesProvider::class, $usedImages[0]);
		$this->assertInstanceOf(Image::class, $usedImages[0]);
		$this->assertEquals( $this->baseSourcePath . '/yolo/foo.jpg', $usedImages[0]->getSourcePath());

		$this->assertInstanceOf(UsedImagesProvider::class, $usedImages[1]);
		$this->assertInstanceOf(Image::class, $usedImages[1]);
		$this->assertEquals( $this->baseSourcePath . '/yolo/bar.jpg', $usedImages[1]->getSourcePath());
	}
}