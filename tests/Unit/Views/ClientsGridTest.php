<?php

namespace Tests\Unit\Views;

use App\App;
use Tests\TestCase;
use App\Domain\Client;
use App\Views\ClientsGrid;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Repositories\Array\ArrayRepository;
use App\Repositories\FileSystem\ClientsRepository;

use function PHPUnit\Framework\assertLessThan;
use function PHPUnit\Framework\assertStringContainsString;

class ClientsGridTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;
    
	/** @test */
    public function it_can_be_instantiated()
    {
        $this->assertNotNull( new ClientsGrid );
    }

    /** @test */
    public function it_can_be_instantiated_from_static_method()
    {
        $this->assertInstanceOf(
            ClientsGrid::class, 
            ClientsGrid::fromRegistry()
        );
    }

    /** @test */
    public function it_can_be_instantiated_from_container_as_singleton()
    {
        $this->assertInstanceOf(
            ClientsGrid::class, 
            app(ClientsGrid::class)
        );

        $this->assertSame(
            app(ClientsGrid::class),
            app(ClientsGrid::class)
        );
    }

    /** @test */
    public function it_returns_null_html_if_empty()
    {
        $instance = new ClientsGrid;

        $this->assertEquals(null, $instance->getHtml());
    }

    /** @test */
    public function it_returns_html_with_one_client()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />', 
            $result
        );

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />', 
            $result
        );
    }

    /** @test */
    public function it_allows_to_specify_custom_area_and_returns_self()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance->withCustomArea(100 * 100);

        $this->assertSame($instance, $result);
    }

    /** @test */
    public function it_returns_html_with_one_client_with_custom_area()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance
            ->withCustomArea(100 * 100)
            ->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="100" height="100" />', 
            $result
        );

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/bar.svg" width="100" height="100" />', 
            $result
        );
    }

    /** @test */
    public function it_allows_to_specify_wrapper_and_returns_self()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance->withWrapper('<span>%s</span>');

        $this->assertSame($instance, $result);
    }

    /** @test */
    public function it_returns_html_with_the_specified_wrapper()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance
            ->withWrapper('<span>%s</span>')
            ->getHtml();

        $this->assertStringContainsString(
            '<span><img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" /></span>', 
            $result
        );

        $this->assertStringContainsString(
            '<span><img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" /></span>', 
            $result
        );
    }

    /** @test */
    public function it_allows_to_specify_whitelist()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $result = $instance
            ->withWhitelist(['foo'])
            ->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />', 
            $result
        );

        $this->assertStringNotContainsString(
            '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />', 
            $result
        );
    }

    /** @test */
    public function it_allows_to_specify_order_through_the_whitelist()
    {
        $this->createClientImages();

        $instance = new ClientsGrid(
            $this->getArrayOfClientInstances()
        );

        $defaultResult = $instance->getHtml();

        $foo = stripos($defaultResult, '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />'); 
        $bar = stripos($defaultResult, '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />');

        $this->assertNotFalse($foo);
        $this->assertNotFalse($bar);
        $this->assertLessThan($bar, $foo);

        $sortedResult = $instance->withWhitelist(['bar', 'foo'])->getHtml();

        $foo = stripos($sortedResult, '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />'); 
        $bar = stripos($sortedResult, '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />');

        $this->assertNotFalse($foo);
        $this->assertNotFalse($bar);
        $this->assertGreaterThan($bar, $foo);
    }

    /** @test */
    public function it_takes_into_account_a_factor_for_resizing_the_logo()
    {
        $this->createClientImages();

        $instance = new ClientsGrid([
            new Client([
                'slug' => 'foo', 
                'logo' => '_clients/foo.svg'
            ])
        ]);

        $defaultResult = $instance->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />',
            $defaultResult
        ); 
        
        $instance = new ClientsGrid([
            new Client([
                'slug' => 'foo', 
                'logo' => '_clients/foo.svg', 
                'scaleFactor' => 1.5
            ])
        ]);

        $sortedResult = $instance->withWhitelist(['bar', 'foo'])->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="75" height="75" />',
            $sortedResult
        ); 
    }

    /** @test */
    public function it_can_be_instantiated_from_registry()
    {
        $this
            ->createClientsRegistry()
            ->createClientImages()
            ->publishClientImages();

        $result = ClientsGrid::fromRegistry()
            ->getHtml();

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />',
            $result
        );

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />',
            $result
        );
    }

    /** @test */
    public function it_handles_not_existing_image()
    {
        $this
            ->createClientsRegistry()
            ->createClientImages();

        unlink(vfsStream::url('folio/contents/images/_clients/foo.svg'));

        $this->publishClientImages();

        $result = ClientsGrid::fromRegistry()->getHtml();

        $this->assertStringNotContainsString(
            '<img src="https://folio-engine.test/images/_clients/foo.svg" width="50" height="50" />',
            $result
        );

        $this->assertStringContainsString(
            '<img src="https://folio-engine.test/images/_clients/bar.svg" width="50" height="50" />',
            $result
        );
    }

    private function getArrayOfClientInstances() : array
    {
        return array_map(
            function ($client) {
                return new Client($client);
            },
            $this->getClientsArray()
        );
    }

    private function createClientsRegistry()
    {
        file_put_contents(
            vfsStream::url('folio/contents/clients.json'),
            json_encode($this->getClientsArray())
        );

        return $this;
    }

    private function createClientImages()
    {
        $this->unlinkDefaultImage();

        $this->createSourceImageFolderForSlug('_clients');

        foreach ($this->getClientsArray() as $client) {
            $this->createDummyImage(
                vfsStream::url("folio/contents/images/{$client['logo']}")
            );
        }

        return $this;
    }

    private function publishClientImages()
    {
        App::bootstrapImagesWhitelist([
            '_clients'
        ]);
        
        App::bootstrapImagesFormats([
            '_clients' => ['skip' => true]
        ]);
        
        $this->publishImages();

        return $this;
    }

    private function getClientsArray()
    {
        return [
            ['title' => 'Foo', 'logo' => '_clients/foo.svg', 'slug' => 'foo'],
            ['title' => 'Bar', 'logo' => '_clients/bar.svg', 'slug' => 'bar'],
        ];
    }
}