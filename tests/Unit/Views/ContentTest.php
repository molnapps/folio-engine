<?php

namespace Tests\Unit\Views;

use App\Context;
use Carbon\Carbon;
use Tests\TestCase;
use App\Domain\Post;
use App\Views\Content;
use App\Testing\VideoTest;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;

class ContentTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithManifest;
	use InteractsWithImage;
	
	/** @before */
	public function setUpTheme()
	{
		vfsStream::copyFromFileSystem(
			__DIR__ . '/../../../themes/Folio/', 
			$this->root->getChild('theme')
		);
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Content(new Post));
	}

	/** @test */
	public function it_adds_post_type_css_class()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'type' => 'foo'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'Post--isFoo', 
	    	$content
	    );
	}

	/** @test */
	public function it_adds_wrapper_div_with_css_class()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post,
	    ]);

	    $this->assertStringContainsString(
	    	'class="Post', 
	    	$content
	    );
	}

	/** @test */
	public function it_adds_post_slug_css_class()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'slug' => 'bar'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'Post--bar', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_title()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'title' => 'Foobar'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<h1>Foobar</h1>', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_markdown_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => 'Foobar'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<p>Foobar</p>', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_text_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
				'texts' => [
					'foo' => 'FOO'
				],
	    		'content' => "@text('foo')"
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'FOO', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_vimeo_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => '{vimeo:12345}'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<div class="Template__video', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_youtube_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => '{youtube:hPr-Yc92qaY}'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<div class="Template__video', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_slideshow_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => '{slideshow}{/slideshow}'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<span class="Slideshow"></span>', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_grid_content()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => '@grid@endgrid'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<div class="Post-grid Post-grid--cols-3"></div>', 
	    	$content
	    );
	}

	/** @test */
	public function it_does_not_render_credits_if_post_has_no_credits()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post,
	    ]);

	    $this->assertStringNotContainsString(
	    	'Credits', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_credits_if_post_has_credits()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'credits' => 'Foobar'
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<div class="Credits">', 
	    	$content
	    );

	    $this->assertStringContainsString(
	    	'<h3>Credits</h3>', 
	    	$content
	    );
	}

	/** @test */
	public function it_renders_date()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post,
	    ]);

	    $this->assertStringContainsString(
	    	'<h4 class="Post__date">' . Carbon::now()->format('F Y') . '</h4>', 
	    	$content
	    );
	}

	/** @test */
	public function it_does_not_render_date_if_post_is_page()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post(['type' => 'page']),
	    ]);

	    $this->assertStringNotContainsString(
	    	'<h4 class="Post__date">', 
	    	$content
	    );
	}

	/** @test */
	public function it_interpolates_vimeo_before_markdown_in_order_not_to_wrap_div_within_p_tag()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => '{vimeo:12345}'
	    	]),
	    ]);

	    $this->assertStringNotContainsString(
	    	'<p><div class="Template__video', 
	    	$content
	    );
	}

	/** @test */
	public function it_interpolates_slideshow_after_markdown_for_markdown_be_interpolated()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
		    	'content' => '{slideshow}{/slideshow}'
		    ]),
	    ]);

	    $this->assertStringContainsString(
	    	'<p><span class="Slideshow">', 
	    	$content
	    );
	}

	/** @test */
	public function it_interpolates_text_content_before_markdown()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
				'texts' => [
					'foo' => '###FOO###'
				],
	    		'content' => "@text('foo')"
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<h3>FOO</h3>', 
	    	$content
	    );
	}

	/** @test */
	public function it_returns_html_lead_defined_in_content()
	{
	    $content = new Content(
	    	new Post([
    			'lead_separator' => '<!--more-->',
	    		'content' => 'Foo<!--more-->Bar'
	    	])
	    );

	    $this->assertEquals(
	    	'<p>Foo</p>', 
	    	$content->getLead()
	    );
	}

	/** @test */
	public function it_returns_html_lead_defined_in_content_only_if_exists()
	{
	    $content = new Content(
	    	new Post([
    			'lead_separator' => '<!--more-->',
	    		'content' => '<!--more-->Bar'
	    	])
	    );

	    $this->assertEquals(
	    	null, 
	    	$content->getLead()
	    );
	}

	/** @test */
	public function it_returns_html_lead_defined_in_texts()
	{
	    $content = new Content(
	    	new Post([
    			'texts' => [
					'lead' => 'Lorem ipsum dolor sit amet'
				]
	    	])
	    );

	    $this->assertEquals(
	    	'<p>Lorem ipsum dolor sit amet</p>', 
	    	$content->getLead()
	    );
	}

	/** @test */
	public function it_gives_precedence_to_lead_defined_in_texts_but_trims_lead_defined_in_content()
	{
	    $content = new Content(
	    	new Post([
    			'texts' => [
					'lead' => 'Lorem ipsum dolor sit amet',
				],
				'lead_separator' => '<!--more-->',
	    		'content' => 'Foo<!--more-->Bar'
	    	])
	    );

	    $this->assertEquals(
	    	'<p>Lorem ipsum dolor sit amet</p>', 
	    	$content->getLead()
	    );

		$this->assertEquals(
	    	'<p>Bar</p>', 
	    	$content->getBody()
	    );
	}

	/** @test */
	public function it_renders_regular_images_if_manifest_does_not_exist()
	{
		$this->createImageFolderForSlug('foo');

		$this->createDummyImage(
			vfsStream::url("folio/contents/images/foo/bar.png"),
			['width' => 50, 'height' => 50]
		);

	    $content = new Content(
	    	new Post([
	    		'slug' => 'foo', 
	    		'content' => '![bar]'
	    	])
	    );

	    $this->assertStringContainsString(
	    	sprintf(
				'<img src="%s" alt="bar" />', 
				"{$this->baseUrlImages}/foo/bar.png"
			),
	    	$content->getBody()
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideValidFileNames
	 * */
	public function it_renders_responsive_images_if_manifest_exists($filename)
	{
		$this->createImageFolderForSlug('foo');

		$this->createDummyImage(
			vfsStream::url("folio/contents/images/foo/{$filename}.png"),
			['width' => 50, 'height' => 50]
		);

		$manifest = $this->getPublishedManifestInstance();
		
		$content = new Content(
	    	new Post([
	    		'slug' => 'foo', 
	    		'content' => "![{$filename}]"
	    	])
	    );

	    $this->assertStringContainsString(
	    	sprintf(
				'<source srcset="%s" width="50" height="50" type="image/webp"></source>',
				"{$this->baseUrlImages}/foo/{$filename}-compressed.webp"
			), 
	    	$content->getBody()
	    );
	    
	    $this->assertStringContainsString(
	    	sprintf(
				'<img src="%s" width="50" height="50" />', 
				"{$this->baseUrlImages}/foo/{$filename}-compressed.jpg"
			),
	    	$content->getBody()
	    );
	}

	public function provideValidFileNames()
	{
		return [
			['foo'],
			['FOO'],
			['FoO'],
			['foo-bar'],
			['foo_bar'],
			['foo-1'],
			['foo.bar']
		];
	}

	/** @test */
	public function it_accepts_custom_interpolator()
	{
	    $content = new Content(
	    	new Post([
	    		'slug' => 'foo', 
	    		'content' => 'foobar'
	    	])
	    );

	    $this->assertEquals('<p>foobar</p>', $content->getBody());

	    Content::interpolators()->prepend(new ReverseInterpolator);
	    $this->assertEquals('<p>raboof</p>', $content->getBody());

	    Content::interpolators()->append(new ReverseInterpolator);
	    $this->assertEquals('>p/<foobar>p<', $content->getBody());

	    Content::interpolators()->register(new ReverseInterpolator);
	    $this->assertEquals('<p>raboof</p>', $content->getBody());
	}

	/** @test */
	public function it_renders_vimeo_content_from_videos()
	{
	    $content = view('content', [
	    	'context' => new Context,
	    	'post' => new Post([
	    		'content' => "@video('foo-video')",
				'videos' => [
					VideoTest::vimeoVideo()
				]
	    	]),
	    ]);

	    $this->assertStringContainsString(
	    	'<div class="Template__video', 
	    	$content
	    );
	}
}

class ReverseInterpolator implements \App\Interpolators\Interpolator
{
	public function text($content)
	{
		return strrev($content);
	}
}