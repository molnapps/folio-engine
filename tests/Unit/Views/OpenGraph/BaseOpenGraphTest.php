<?php

namespace Tests\Unit\Views\OpenGraph;

use App\App;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Views\OpenGraph\OpenGraph;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithConsole;
use App\Views\OpenGraph\BaseOpenGraph;
use App\Testing\InteractsWithFileSystem;

class BaseOpenGraphTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithConsole;
    use InteractsWithImage;
	
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = new BaseOpenGraph;

	    $this->assertInstanceOf(OpenGraph::class, $instance);
	}

    /** @test */
	public function it_returns_default_values()
	{
        $instance = new BaseOpenGraph;
        
        $this->assertEquals(env('BASE_URL'), $instance->getUrl());
        $this->assertEquals('', $instance->getImageUrl());
        $this->assertEquals('website', $instance->getType());
        $this->assertEquals('My portfolio', $instance->getTitle());
        $this->assertEquals('Welcome to my portfolio', $instance->getDescription());
	}

    /** @test */
	public function it_can_be_configured()
	{
        $instance = new BaseOpenGraph;

        $instance->bootstrap([
            'image' => '_icons/website-feature.png',
            'title' => 'Foobar',
            'description' => 'My description'
        ]);
        
        $this->assertEquals(
            'https://folio-engine.test/images/_icons/website-feature.png', 
            $instance->getImageUrl()
        );

        $this->assertEquals('website', $instance->getType());
        $this->assertEquals('Foobar', $instance->getTitle());
        $this->assertEquals('My description', $instance->getDescription());
	}

    /** @test */
	public function it_accept_custom_image_with_leading_slash_relative_path()
	{
        $instance = new BaseOpenGraph;

        $instance->bootstrap([
            'image' => '/_icons/website-feature.png',
        ]);
        
        $this->assertEquals(
            'https://folio-engine.test/images/_icons/website-feature.png', 
            $instance->getImageUrl()
        );
	}

    /** @test */
	public function it_accept_respnsive_image()
	{
        $this->createImageFolderForSlug('_icons');
        $this->createDummyImage(
            vfsStream::url('folio/contents/images/_icons/website-feature.png')
        );

        App::bootstrapImagesWhitelist([
            '_icons'
        ]);

        $this->runCommand('publish:images', ['--publisher' => 'optimized']);

        $instance = new BaseOpenGraph;

        $instance->bootstrap([
            'image' => '/_icons/website-feature.png',
        ]);
        
        $this->assertEquals(
            'https://folio-engine.test/images/_icons/website-feature-compressed.jpg', 
            $instance->getImageUrl()
        );
	}
}