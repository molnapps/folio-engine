<?php

namespace Tests\Unit\Views\OpenGraph;

use App\Portfolio;
use Tests\TestCase;
use App\Domain\Post;
use App\Testing\ProjectBuilder;
use App\Views\OpenGraph\OpenGraph;
use App\Views\OpenGraph\BaseOpenGraph;
use App\Testing\InteractsWithFileSystem;
use App\Views\OpenGraph\CombinedOpenGraph;

class CombinedOpenGraphTest extends TestCase
{
	use InteractsWithFileSystem;
	
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = new CombinedOpenGraph();

	    $this->assertInstanceOf(OpenGraph::class, $instance);
	}

    /** @test */
	public function it_can_be_instantiated_with_one_item()
	{
        $delegate = new BaseOpenGraph;

        $instance = new CombinedOpenGraph([
            $delegate
        ]);

	    $this->assertEquals($delegate->getUrl(), $instance->getUrl());
        $this->assertEquals($delegate->getImageUrl(), $instance->getImageUrl());
        $this->assertEquals($delegate->getType(), $instance->getType());
        $this->assertEquals($delegate->getTitle(), $instance->getTitle());
        $this->assertEquals($delegate->getDescription(), $instance->getDescription());
	}

    /** @test */
	public function it_can_be_instantiated_with_two_items()
	{
        $delegate = new BaseOpenGraph([
            'url' => 'foo',
            'image_base_url' => 'foo',
            'image' => 'foo.jpg',
            'type' => 'foo',
            'title' => 'Foo',
            'description' => 'Foo description',
        ]);

        $default = new BaseOpenGraph([
            'url' => 'bar',
            'image_base_url' => 'bar',
            'image' => 'bar.jpg',
            'type' => 'bar',
            'title' => 'Bar',
            'description' => 'Bar description',
        ]);

        $instance = new CombinedOpenGraph([
            $delegate,
            $default
        ]);

	    $this->assertEquals($delegate->getUrl(), $instance->getUrl());
        $this->assertEquals($delegate->getImageUrl(), $instance->getImageUrl());
        $this->assertEquals($delegate->getType(), $instance->getType());
        $this->assertEquals($delegate->getTitle(), $instance->getTitle());
        $this->assertEquals($delegate->getDescription(), $instance->getDescription());
	}

    /** @test */
	public function it_can_be_instantiated_with_two_items_and_returns_default()
	{
        $delegate = new BaseOpenGraph([
            'url' => '',
            'image_base_url' => '',
            'image' => '',
            'type' => '',
            'title' => '',
            'description' => '',
        ]);

        $default = new BaseOpenGraph([
            'url' => 'bar',
            'image_base_url' => 'bar',
            'image' => 'bar.jpg',
            'type' => 'bar',
            'title' => 'Bar',
            'description' => 'Bar description',
        ]);

        $instance = new CombinedOpenGraph([
            $delegate,
            $default
        ]);

	    $this->assertEquals($default->getUrl(), $instance->getUrl());
        $this->assertEquals($default->getImageUrl(), $instance->getImageUrl());
        $this->assertEquals($default->getType(), $instance->getType());
        $this->assertEquals($default->getTitle(), $instance->getTitle());
        $this->assertEquals($default->getDescription(), $instance->getDescription());
	}
}