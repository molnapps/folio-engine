<?php

namespace Tests\Unit\Views\OpenGraph;

use App\Context;
use App\Portfolio;
use Tests\TestCase;
use App\Domain\Post;
use App\Domain\Preview;
use App\Testing\ProjectBuilder;
use App\Views\OpenGraph\OpenGraph;
use App\Views\OpenGraph\PostOpenGraph;
use App\Testing\InteractsWithFileSystem;
use App\Views\OpenGraph\CombinedOpenGraph;
use org\bovigo\vfs\vfsStream;

class PostOpenGraphTest extends TestCase
{
	use InteractsWithFileSystem;
	
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = PostOpenGraph::fromPost(new Post);

	    $this->assertInstanceOf(OpenGraph::class, $instance);
        $this->assertInstanceOf(CombinedOpenGraph::class, $instance);
	}

    /** @test */
	public function it_can_be_instantiated_from_post_with_hero_image()
	{
        ProjectBuilder::fromSlug('foo')
            ->withNotUsedImages('hero')
            ->withMeta([
                'title' => 'Foobar', 
                'excerpt' => 'My excerpt'
            ])
            ->create()
            ->publishImages();

        $post = app(Portfolio::class)->getProject('foo');
        
        $instance = PostOpenGraph::fromPost($post);
        
        $this->assertEquals(
            'https://folio-engine.test/project/foo', 
            $instance->getUrl()
        );

        $this->assertEquals(
            'https://folio-engine.test/images/foo/hero-website-feature-compressed.jpg', 
            $instance->getImageUrl()
        );

        $this->assertEquals('website', $instance->getType());
        $this->assertEquals('Foobar', $instance->getTitle());
        $this->assertEquals('My excerpt', $instance->getDescription());
	}

    /** @test */
	public function it_can_be_instantiated_from_post_with_website_feature_image()
	{
        ProjectBuilder::fromSlug('foo')
            ->withWebsiteFeatureImage('bar')
            ->withMeta([
                'title' => 'Foobar', 
                'excerpt' => 'My excerpt'
            ])
            ->create()
            ->publishImages();

        $post = app(Portfolio::class)->getProject('foo');

        $instance = PostOpenGraph::fromPost($post);
        
        $this->assertEquals(
            'https://folio-engine.test/project/foo', 
            $instance->getUrl()
        );

        $this->assertEquals(
            'https://folio-engine.test/images/foo/bar-website-feature-compressed.jpg', 
            $instance->getImageUrl()
        );

        $this->assertEquals('website', $instance->getType());
        $this->assertEquals('Foobar', $instance->getTitle());
        $this->assertEquals('My excerpt', $instance->getDescription());
	}

    /** @test */
	public function it_returns_url_according_to_context()
	{
        ProjectBuilder::fromSlug('foo')
            ->withNotUsedImages('hero')
            ->create()
            ->publishImages();

        $post = app(Portfolio::class)->getProject('foo');
        
        $this->assertEquals(
            'https://folio-engine.test/project/foo', 
            PostOpenGraph::fromPost($post)->getUrl()
        );

        app(Context::class)->isStatic();
        
        $this->assertEquals(
            'https://folio-engine.test/project/foo.html', 
            PostOpenGraph::fromPost($post)->getUrl()
        );
	}

    /** @test */
	public function it_returns_url_according_to_context_homepage()
	{
        ProjectBuilder::fromSlug('foo')
            ->withNotUsedImages('hero')
            ->create()
            ->publishImages();

        $post = app(Portfolio::class)->getProject('foo');
        
        $this->assertEquals(
            'https://folio-engine.test/project/foo', 
            PostOpenGraph::fromPost($post)->getUrl()
        );

        app(Context::class)->isHome();
        
        $this->assertEquals(
            'https://folio-engine.test', 
            PostOpenGraph::fromPost($post)->getUrl()
        );
	}

    /** @test */
	public function it_can_be_instantiated_from_empty_post()
	{
        $default = app(OpenGraph::class);

        $instance = PostOpenGraph::fromPost(new Post);
        
        $this->assertEquals($default->getUrl(), $instance->getUrl());
        $this->assertEquals($default->getImageUrl(), $instance->getImageUrl());
        $this->assertEquals($default->getTitle(), $instance->getTitle());
        $this->assertEquals($default->getDescription(), $instance->getDescription());
	}
}