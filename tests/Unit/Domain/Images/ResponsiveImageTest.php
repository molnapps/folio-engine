<?php

namespace Tests\Unit\Domain\Images;

use Tests\TestCase;
use App\Domain\Images\Image;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Domain\Images\LocalImage;
use App\Domain\Images\RemoteImage;
use App\Testing\InteractsWithImage;
use App\Domain\Images\ResponsiveImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Manifests\Manifest;
use App\Repositories\FileSystem\PathInspector;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;

class ResponsiveImageTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithManifest;

	/** 
	 * @test 
	 * @dataProvider provideValidRelativePaths
	 */
	public function it_can_be_instantiated_with_relative_path($relativePath)
	{
		$instance = new ResponsiveImage(
			$this->getManifestInstance(), 
			$relativePath
		);

	    $this->assertNotNull($instance);
	}

	public function provideValidRelativePaths()
	{
		return [
			['foo.jpg'],
			[new LocalImage('foo.jpg')],
			[new RemoteImage('https://loremflickr.com/30/30')],
			[''],
			[null]
		];
	}

	/** @test */
	public function it_implements_images_interface()
	{
		$instance = new ResponsiveImage(
			$this->getManifestInstance(), 
			$relativePath = ''
		);

	    $this->assertInstanceOf(Image::class, $instance);
	}

	/** @test */
	public function it_implements_arrayable_interface()
	{
		$instance = new ResponsiveImage(
			$this->getManifestInstance(), 
			$relativePath = ''
		);

	    $this->assertInstanceOf(
	    	\Illuminate\Contracts\Support\Arrayable::class, 
	    	$instance
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideNotExistingImages
	 */
	public function it_returns_null_if_image_does_not_exist($relativePath)
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );

	    $this->assertNull(
	    	$instance->getHtml()
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_returns_html_for_image_with_output($relativePath)
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );

	    $html = $instance->getHtml();
	    
	    $this->assertStringContainsString('<picture>', $html);

	    $this->assertStringContainsString(
	    	'<img src="' . $this->baseUrlImages . '/foo-compressed.jpg" width="50" height="50" />', 
	    	$html
	    );

	    $this->assertStringContainsString(
	    	'<source srcset="' . $this->baseUrlImages . '/foo-compressed.webp" width="50" height="50" type="image/webp"></source>', 
	    	$html
	    );

	    $this->assertStringContainsString('</picture>', $html);
	}

	/** @test */
	public function it_returns_html_for_image_with_output_png()
	{
	    $this->createDummyImageWithAlpha(vfsStream::url('folio/contents/images/foo.png'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.png'
	    );

	    $html = $instance->getHtml();
	    
	    $this->assertStringContainsString(
	    	'<img src="' . $this->baseUrlImages . '/foo-compressed.png" width="50" height="50" />', 
	    	$html
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_returns_url_for_image_with_output($relativePath)
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );
	    
	    $this->assertEquals($this->baseUrlImages . '/foo-compressed.jpg', $instance->getUrl());
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_returns_path_for_image_with_output($relativePath)
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );
	    
	    $this->assertEquals(
	    	vfsStream::url('folio/public/images/foo-compressed.jpg'), 
	    	$instance->getPath()
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_can_be_converted_to_array($relativePath)
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );
	    
	    $outputs = $instance->toResponsiveArray();

	    $this->assertCount(2, $outputs);
	    $this->assertEquals(vfsStream::url('folio/public/images/foo-compressed.jpg'), $outputs[0]['path']);
	    $this->assertEquals(vfsStream::url('folio/public/images/foo-compressed.webp'), $outputs[1]['path']);
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_returns_html_for_image_with_output_with_attributes($relativePath)
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );

	    $html = $instance->getHtml(['foo' => 'bar']);
	    
	    $this->assertStringContainsString('<picture>', $html);
	    $this->assertStringContainsString('<source srcset="' . $this->baseUrlImages . '/foo-compressed.webp" width="50" height="50" foo="bar" type="image/webp">', $html);
	    $this->assertStringContainsString('<img src="' . $this->baseUrlImages . '/foo-compressed.jpg" width="50" height="50" foo="bar" />', $html);
	}

	/** @test */
	public function it_returns_html_for_remote_images()
	{
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	new RemoteImage('https://loremflickr.com/30/30')
	    );

	    $html = $instance->getHtml(['foo' => 'bar']);
	    
	    $this->assertEquals(
	    	'<img src="https://loremflickr.com/30/30" width="30" height="30" foo="bar" />', 
	    	$html
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingImagesWithOutput
	 */
	public function it_renders_webp_image_before_than_jpg($relativePath)
	{
		$this->unlinkDefaultImage();
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));

	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );

	    $html = $instance->getHtml();
	    
	    $this->assertStringContainsString('<picture>', $html);

	    $this->assertLessThan(
	    	stripos($html, '/images/foo-compressed.jpg'),
	    	stripos($html, '/images/foo-compressed.webp')
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingUncompressedImagesWithOutput
	 */
	public function it_returns_html_for_uncompressed_images()
	{
	    $this->unlinkDefaultImage();
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.gif'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.gif'
	    );
	    
	    $this->assertEquals(
	    	'<img src="' . $this->baseUrlImages . '/foo.gif" width="50" height="50" />',
	    	$instance->getHtml()
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingUncompressedImagesWithOutput
	 */
	public function it_removes_double_slashes($relativePath)
	{
	    $this->unlinkDefaultImage();
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.gif'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );
	    
	    $this->assertEquals(
	    	'<img src="' . $this->baseUrlImages . '/foo.gif" width="50" height="50" />',
	    	$instance->getHtml()
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideExistingNonImages
	 */
	public function it_returns_null_for_non_image_files($relativePath)
	{
	    $this->unlinkDefaultImage();
	    $this->createEmptyFile(vfsStream::url('folio/contents/images/foo.json'));
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	$relativePath
	    );
	    
	    $this->assertNull(
	    	$instance->getHtml()
	    );
	}

	/** @test */
	public function it_asserts_file_exists()
	{
		$notExisting = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'not_existing.jpg'
	    );

	    $this->assertFalse( $notExisting->exists() );

	    $existing = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

	    $this->assertTrue( $existing->exists() );
	}

	/** @test */
	public function it_returns_base_path()
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

	    $this->assertEquals(
	    	env('IMAGES_DESTINATION_PATH'), 
	    	$instance->getBasePath()
	    );
	}

	/** @test */
	public function it_returns_base_url()
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

	    $this->assertEquals(
	    	env('IMAGES_DESTINATION_URL'), 
	    	$instance->getBaseUrl()
	    );
	}

	/** @test */
	public function it_returns_size_for_jpg_image()
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

	    $this->assertEquals(120, $instance->getWidth());
	    $this->assertEquals(20, $instance->getHeight());
	}

	/** @test */
	public function it_returns_size_for_svg_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/circle.svg'), 
			['width' => 100, 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'circle.svg'
	    );

	    $this->assertEquals(100, $instance->getWidth());
	    $this->assertEquals(100, $instance->getHeight());
	}

	/** @test */
	public function it_returns_html_for_svg_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/circle.svg'), 
			['width' => 100, 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'circle.svg'
	    );

	    $this->assertEquals(
			'<img src="' . $this->baseUrlImages . '/circle.svg" width="100" height="100" />', 
			$instance->getHtml()
		);
	}

	/** @test */
	public function it_returns_html_for_svg_image_with_attributes()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/circle.svg'), 
			['width' => 100, 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'circle.svg'
	    );

	    $this->assertEquals(
			'<img src="' . $this->baseUrlImages . '/circle.svg" width="50" height="50" />', 
			$instance->getHtml(['width' => 50, 'height' => 50])
		);
	}

	/** @test */
	public function it_returns_size_for_corrupted_svg_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/circle-corrupted.svg'), 
			['corrupted' => true]
		);
		
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'circle-corrupted.svg'
	    );

	    $this->assertEquals(null, $instance->getWidth());
	    $this->assertEquals(null, $instance->getHeight());
	}

	/** @test */
	public function it_throws_if_no_desination_url_is_set()
	{
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage('Please provide a IMAGES_DESTINATION_URL environment variable');

		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg',
			['destinationUrl' => null]
	    );
	}

	/** @test */
	public function it_throws_if_no_desination_path_is_set()
	{
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage('Please provide a IMAGES_DESTINATION_PATH environment variable');

		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg',
			['destinationPath' => null]
	    );
	}

	/** @test */
	public function it_throws_if_no_source_path_is_set()
	{
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage('Please provide a IMAGES_SOURCE_PATH environment variable');

		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg',
			['sourcePath' => null]
	    );
	}

	/** @test */
	public function it_returns_source_path_for_responsive_image()
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

		$this->assertEquals(
			vfsStream::url('folio/contents/images/foo.jpg'), 
			$instance->getSourcePath()
		);
	}

	/** @test */
	public function it_returns_source_path_for_non_responsive_image()
	{
		$instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'non-existing.jpg'
	    );

		$this->assertEquals(
			vfsStream::url('folio/contents/images/non-existing.jpg'), 
			$instance->getSourcePath()
		);
	}

	/** @test */
	public function it_provides_a_way_to_normalize_the_size_of_multiple_images()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/square.svg'), 
			['width' => 200, 'height' => 200]
		);

		$this->createDummyImage(
			vfsStream::url('folio/contents/images/horizontal.svg'), 
			['width' => 100, 'height' => 50]
		);

		$this->createDummyImage(
			vfsStream::url('folio/contents/images/vertical.svg'), 
			['width' => 50, 'height' => 100]
		);
		
		$square = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'square.svg'
	    );

		$horizontal = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'horizontal.svg'
	    );

		$vertical = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'vertical.svg'
	    );

	    $this->assertEquals(200, $square->getWidth());
	    $this->assertEquals(200, $square->getHeight());
		
		$this->assertEquals(100, $horizontal->getWidth());
	    $this->assertEquals(50, $horizontal->getHeight());

		$this->assertEquals(50, $vertical->getWidth());
	    $this->assertEquals(100, $vertical->getHeight());

		$expectedArea = 100 * 100;

		// 1:1
		$expectedWidth = round(sqrt($expectedArea * $square->getWidth() / $square->getHeight()));
		$expectedHeight = round(sqrt($expectedArea * $square->getHeight() / $square->getWidth()));
		$this->assertEquals($expectedArea, $square->normalizeSize()->getArea());
		$this->assertEquals($expectedWidth, $square->normalizeSize()->getWidth());
		$this->assertEquals($expectedHeight, $square->normalizeSize()->getHeight());
		
		// 2:1
		$expectedWidth = round(sqrt($expectedArea * $horizontal->getWidth() / $horizontal->getHeight()));
		$expectedHeight = round(sqrt($expectedArea * $horizontal->getHeight() / $horizontal->getWidth()));
		$this->assertEquals($expectedArea, $horizontal->normalizeSize()->getArea());
		$this->assertEquals($expectedWidth, $horizontal->normalizeSize()->getWidth());
		$this->assertEquals($expectedHeight, $horizontal->normalizeSize()->getHeight());
		
		// 1:2
		$expectedWidth = round(sqrt($expectedArea * $vertical->getWidth() / $vertical->getHeight()));
		$expectedHeight = round(sqrt($expectedArea * $vertical->getHeight() / $vertical->getWidth()));
		$this->assertEquals($expectedArea, $vertical->normalizeSize()->getArea());
		$this->assertEquals($expectedWidth, $vertical->normalizeSize()->getWidth());
	    $this->assertEquals($expectedHeight, $vertical->normalizeSize()->getHeight());
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_in_storage_folder()
	{
		$manifest = $this->publishImageFormats(
			vfsStream::url('folio/contents/images/foo.jpg'),
			['width' => 200, 'height' => 100]
		);

		$instance = new ResponsiveImage(
	    	$manifest, 
	    	'foo.jpg'
	    );

		$html = $instance->getHtml();

		$expectedOrdered = [
			'<picture>',
			'<source srcset="https://folio-engine.test/images/foo-portrait-compressed.webp" width="80" height="100" media="(max-aspect-ratio: 4/5)" type="image/webp"></source>',
			'<source srcset="https://folio-engine.test/images/foo-square-compressed.webp" width="100" height="100" media="(max-aspect-ratio: 1/1)" type="image/webp"></source>',
			'<source srcset="https://folio-engine.test/images/foo-compressed.webp" width="200" height="100" media="(min-aspect-ratio: 1/1)" type="image/webp"></source>',
			'<img src="https://folio-engine.test/images/foo-compressed.jpg" width="200" height="100" />',
			'</picture>',
		];

		$previousPos = 0;
		foreach ($expectedOrdered as $tag) {
			$this->assertStringContainsString($tag, $html);
			$this->assertGreaterThanOrEqual($previousPos, $currentPos = stripos($html, $tag));
			$previousPos = $currentPos;
		}
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_in_storage_folder_with_attributes()
	{
		$manifest = $this->publishImageFormats(
			vfsStream::url('folio/contents/images/foo.jpg'),
			['width' => 200, 'height' => 100]
		);

		$instance = new ResponsiveImage(
	    	$manifest, 
	    	'foo.jpg'
	    );

		$html = $instance->getHtml(['class' => 'foo']);

		$expectedOrdered = [
			'<picture>',
			'<source srcset="https://folio-engine.test/images/foo-portrait-compressed.webp" width="80" height="100" class="foo" media="(max-aspect-ratio: 4/5)" type="image/webp"></source>',
			'<source srcset="https://folio-engine.test/images/foo-square-compressed.webp" width="100" height="100" class="foo" media="(max-aspect-ratio: 1/1)" type="image/webp"></source>',
			'<source srcset="https://folio-engine.test/images/foo-compressed.webp" width="200" height="100" class="foo" media="(min-aspect-ratio: 1/1)" type="image/webp"></source>',
			'<img src="https://folio-engine.test/images/foo-compressed.jpg" width="200" height="100" class="foo" />',
			'</picture>',
		];

		$previousPos = 0;
		foreach ($expectedOrdered as $tag) {
			$this->assertStringContainsString($tag, $html);
			$this->assertGreaterThanOrEqual($previousPos, $currentPos = stripos($html, $tag));
			$previousPos = $currentPos;
		}
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_uncompressed()
	{
		$manifest = $this->publishImageFormats(
			vfsStream::url('folio/contents/images/foo.gif'),
			['width' => '200', 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$manifest, 
	    	'foo.gif'
	    );

		$html = $instance->getHtml();

		$expectedOrdered = [
			'<picture>',
			'<source srcset="https://folio-engine.test/images/foo-portrait.gif" width="80" height="100" media="(max-aspect-ratio: 4/5)" type="image/gif"></source>',
			'<source srcset="https://folio-engine.test/images/foo-square.gif" width="100" height="100" media="(max-aspect-ratio: 1/1)" type="image/gif"></source>',
			'<source srcset="https://folio-engine.test/images/foo.gif" width="200" height="100" media="(min-aspect-ratio: 1/1)" type="image/gif"></source>',
			'<img src="https://folio-engine.test/images/foo.gif" width="200" height="100" />',
			'</picture>',
		];

		$previousPos = 0;
		foreach ($expectedOrdered as $tag) {
			$this->assertStringContainsString($tag, $html);
			$this->assertGreaterThanOrEqual($previousPos, $currentPos = stripos($html, $tag));
			$previousPos = $currentPos;
		}
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_svg()
	{
		$manifest = $this->publishImageFormats(
			vfsStream::url('folio/contents/images/foo.svg'),
			['width' => 200, 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$manifest, 
	    	'foo.svg'
	    );

		$html = $instance->getHtml();

		$expectedOrdered = [
			'<picture>',
			'<source srcset="https://folio-engine.test/images/foo-portrait.svg" width="80" height="100" media="(max-aspect-ratio: 4/5)" type="image/svg+xml"></source>',
			'<source srcset="https://folio-engine.test/images/foo-square.svg" width="100" height="100" media="(max-aspect-ratio: 1/1)" type="image/svg+xml"></source>',
			'<source srcset="https://folio-engine.test/images/foo.svg" width="200" height="100" media="(min-aspect-ratio: 1/1)" type="image/svg+xml"></source>',
			'<img src="https://folio-engine.test/images/foo.svg" width="200" height="100" />',
			'</picture>',
		];

		$previousPos = 0;
		foreach ($expectedOrdered as $tag) {
			$this->assertStringContainsString($tag, $html);
			$this->assertGreaterThanOrEqual($previousPos, $currentPos = stripos($html, $tag));
			$previousPos = $currentPos;
		}
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_skipped()
	{
		$this->unlinkDefaultImage();
	    
		$this->createEmptyFile(
			vfsStream::url('folio/contents/images/foo.json')
		);
	    
	    $instance = new ResponsiveImage(
	    	$this->getPublishedManifestInstance(), 
	    	'foo.json'
	    );
	    
	    $this->assertNull($instance->getHtml());
	}

	/** @test */
	public function it_provides_picture_in_different_formats_if_available_skipped_2()
	{
		$manifest = $this->publishImageFormats(
			vfsStream::url('folio/contents/images/foo.jpg'),
			['width' => 100, 'height' => 100]
		);

		$instance = new ResponsiveImage(
	    	$manifest = $this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

		$html = $instance->getHtml();

		$expectedOrdered = [
			'<picture>',
			'<source srcset="https://folio-engine.test/images/foo-portrait-compressed.webp" width="80" height="100" media="(max-aspect-ratio: 4/5)" type="image/webp"></source>',
			'<source srcset="https://folio-engine.test/images/foo-compressed.webp" width="100" height="100" media="(min-aspect-ratio: 1/1)" type="image/webp"></source>',
			'<img src="https://folio-engine.test/images/foo-compressed.jpg" width="100" height="100" />',
			'</picture>',
		];

		$previousPos = 0;
		foreach ($expectedOrdered as $tag) {
			$this->assertStringContainsString($tag, $html);
			$this->assertGreaterThanOrEqual($previousPos, $currentPos = stripos($html, $tag));
			$previousPos = $currentPos;
		}
	}

	/** @test */
	public function it_provides_picture_in_different_formats_with_crop_limit()
	{
		$images = [
			'landscape' => vfsStream::url('folio/contents/images/foo/foo.jpg'),
			'square' => vfsStream::url('folio/storage/images/foo/foo-square.jpg'),
			'portrait' => vfsStream::url('folio/storage/images/foo/foo-portrait.jpg'),
		];

		ProjectBuilder::fromSlug('foo')
			->withLandscapeUsedImage('foo')
			->create();

		app(CroppedImagesMap::class)->add(
			$images['landscape'], 
			['crop-limit' => 'square']
		);

		$this->runPublishImageAssetsCommand();

		$this->assertFileExists($images['square']);
		$this->assertFileDoesNotExist($images['portrait']);
		
		$instance = new ResponsiveImage(
	    	$this->getManifestInstance()->loadIfNotLoaded(), 
	    	'foo/foo.jpg'
	    );
		
		$html = $instance->toHtml();
		
		$this->assertStringContainsString(
			'<source srcset="https://folio-engine.test/images/foo/foo-square-compressed.webp" width="100" height="100" media="(max-aspect-ratio: 4/5)" type="image/webp">',
			$html
		);

		$this->assertStringContainsString(
			'<source srcset="https://folio-engine.test/images/foo/foo-square-compressed.webp" width="100" height="100" media="(max-aspect-ratio: 1/1)" type="image/webp">',
			$html
		);

		$this->assertStringContainsString(
			'<source srcset="https://folio-engine.test/images/foo/foo-compressed.webp" width="200" height="100" media="(min-aspect-ratio: 1/1)" type="image/webp">',
			$html
		);

		$this->assertStringContainsString(
			'<img src="https://folio-engine.test/images/foo/foo-compressed.jpg" width="200" height="100" />',
			$html
		);
	}

	/** @test */
	public function it_implements_provides_used_images_interface()
	{
		$instance = new ResponsiveImage(
			$this->getManifestInstance(), 
			'foo.jpg'
		);

		$this->assertInstanceOf(\App\Themes\Publishers\Images\Used\UsedImagesProvider::class, $instance);
	}

	/** @test */
	public function it_finds_source_image()
	{
		$this->createDummyImage(
			$sourcePath = vfsStream::url('folio/contents/images/foo.jpg')
		);
		
		$instance = new ResponsiveImage(
	    	$manifest = $this->getPublishedManifestInstance(), 
	    	'foo.jpg'
	    );

		$usedImagesFinder = new UsedImagesFinder;

		$this->assertFalse($usedImagesFinder->isUsed($sourcePath));

		$instance->findUsedImages($usedImagesFinder);

		$this->assertTrue($usedImagesFinder->isUsed($sourcePath));
	}

	/** @test */
	public function it_finds_source_images_for_formats()
	{
		$manifest = $this->publishImageFormats(
			$sourcePath = vfsStream::url('folio/contents/images/foo.jpg'),
			['width' => 200, 'height' => 100]
		);
		
		$instance = new ResponsiveImage(
	    	$manifest, 
	    	'foo.jpg'
	    );

		$usedImagesFinder = new UsedImagesFinder;

		$this->assertFalse($usedImagesFinder->isUsed($sourcePath));
		$this->assertFalse($usedImagesFinder->isUsed($squareSourcePath = vfsStream::url('folio/storage/images/foo-square.jpg')));
		$this->assertFalse($usedImagesFinder->isUsed($portraitSourcePath = vfsStream::url('folio/storage/images/foo-portrait.jpg')));
		
		$instance->findUsedImages($usedImagesFinder);

		$this->assertTrue($usedImagesFinder->isUsed($sourcePath));
		$this->assertTrue($usedImagesFinder->isUsed($squareSourcePath));
		$this->assertTrue($usedImagesFinder->isUsed($portraitSourcePath));
	}

	

	public function provideNotExistingImages()
	{
		return [
			['not_exists.jpg'],
			[new LocalImage('not_exists.jpg')],
		];
	}

	public function provideExistingImagesWithOutput()
	{
		return [
			['foo.jpg'],
			[new LocalImage('foo.jpg')]
		];
	}

	public function provideExistingUncompressedImagesWithOutput()
	{
		return [
			['foo.gif'],
			[new LocalImage('foo.gif')],
		];
	}

	public function provideExistingNonImages()
	{
		return [
			['foo.json'],
			[new LocalImage('foo.json')]
		];
	}

	private function unlinkDefaultImage()
	{
		unlink(vfsStream::url('folio/contents/images/foo.jpg'));
	}

	private function createEmptyFile($filePath)
	{
		file_put_contents($filePath, '');
	}

	private function publishImageFormats($path, array $attributes) : Manifest
	{
		$path = PathInspector::fromPath($path);

		app(UsedImagesFinder::class)
			->whitelist()
			->set([$path->getFileName()]);
		
		$this->createDummyImage(
			$path->toString(),
			$attributes
		);

		return $this->publishImages();
	}

	private function runPublishImageAssetsCommand()
	{
		$command = new \App\Console\Commands\PublishImageAssets();
		$command->run(
			new \Symfony\Component\Console\Input\ArrayInput(
				['--publisher' => 'optimized']
			),
			new \Symfony\Component\Console\Output\NullOutput
		);
		return $command;
	}
}