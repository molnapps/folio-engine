<?php

namespace Tests\Unit\Domain\Images;

use App\Domain\Images\Image;
use App\Domain\Images\LocalImage;
use App\Domain\Images\RemoteImage;
use App\Domain\Images\NormalizedImage;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class NormalizedImageTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
        $this->assertNotNull(new NormalizedImage(new LocalImage('test.svg')));
    }

    /** @test */
	public function it_implements_image_interface()
	{
        $this->assertInstanceOf(Image::class, new NormalizedImage(new LocalImage('test.svg')));
    }

    /** @test */
	public function it_provides_default_target_area()
	{
        $instance = new NormalizedImage(new LocalImage('test.svg'));

        $this->assertEquals(100 * 100, $instance->getTargetArea());
    }

     /** @test */
	public function it_accepts_custom_target_area()
	{
        $instance = new NormalizedImage(new LocalImage('test.svg'), 200 * 200);

        $this->assertEquals(200 * 200, $instance->getTargetArea());
    }
    
    /** @test */
	public function it_normalizes_square_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/square.svg'), 
			['width' => 200, 'height' => 200]
		);

		$instance = new NormalizedImage(
            new LocalImage('square.svg')
        );

		$this->assertEquals($instance->getTargetArea(), $instance->getArea());
		$this->assertEquals(100, $instance->getWidth());
		$this->assertEquals(100, $instance->getHeight());
	}

    /** @test */
	public function it_normalizes_horizontal_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/horizontal.svg'), 
			['width' => 200, 'height' => 100]
		);

		$instance = new NormalizedImage(
            new LocalImage('horizontal.svg')
        );

		$this->assertEquals($instance->getTargetArea(), $instance->getArea());
		$this->assertEquals(141, $instance->getWidth());
		$this->assertEquals(71, $instance->getHeight());
	}

    /** @test */
	public function it_normalizes_vertical_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/vertical.svg'), 
			['width' => 100, 'height' => 300]
		);

		$instance = new NormalizedImage(
            new LocalImage('vertical.svg')
        );

		$this->assertEquals($instance->getTargetArea(), $instance->getArea());
		$this->assertEquals(58, $instance->getWidth());
		$this->assertEquals(173, $instance->getHeight());
	}

     /** @test */
	public function it_normalizes_multiple_images()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/square.svg'), 
			['width' => 200, 'height' => 200]
		);

		$this->createDummyImage(
			vfsStream::url('folio/contents/images/horizontal.svg'), 
			['width' => 100, 'height' => 50]
		);

		$this->createDummyImage(
			vfsStream::url('folio/contents/images/vertical.svg'), 
			['width' => 50, 'height' => 100]
		);
		
		$square = new LocalImage('square.svg');
		$horizontal = new LocalImage('horizontal.svg');
		$vertical = new LocalImage('vertical.svg');

	    $expectedArea = 100 * 100;

		$this->assertEquals($expectedArea, (new NormalizedImage($square))->getArea());
		$this->assertEquals($expectedArea, (new NormalizedImage($horizontal))->getArea());
		$this->assertEquals($expectedArea, (new NormalizedImage($vertical))->getArea());
	}

    /** @test */
	public function it_returns_html_local_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/square.svg'), 
			['width' => 200, 'height' => 200]
		);

		$instance = new NormalizedImage(
            new LocalImage('square.svg')
        );

		$this->assertEquals(
            '<img src="' . $this->baseUrlImages . '/square.svg" width="100" height="100" />', 
            $instance->getHtml()
        );
	}

	/** @test */
	public function it_returns_html_remote_image()
	{
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/square.svg'), 
			['width' => 200, 'height' => 200]
		);

		$instance = new NormalizedImage(
            new RemoteImage(
				vfsStream::url('folio/contents/images/square.svg')
			)
        );

		$this->assertEquals(
            '<img src="vfs://folio/contents/images/square.svg" width="100" height="100" />', 
            $instance->getHtml()
        );
	}
}