<?php

namespace Tests\Unit\Domain;

use Carbon\Carbon;
use Tests\TestCase;
use App\Domain\Post;
use App\Domain\Client;
use App\Domain\Images\Image;
use App\Testing\PreviewTest;
use org\bovigo\vfs\vfsStream;
use App\Repositories\Registry;
use App\Testing\ProjectBuilder;
use App\Views\OpenGraph\OpenGraph;
use App\Testing\InteractsWithImage;
use App\Repositories\PreviewCollection;
use App\Testing\InteractsWithFileSystem;
use App\Domain\Images\WebsiteFeatureImage;

class PostTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_asserts_whether_it_should_render()
	{
	    $this->assertTrue((new Post)->shouldRender());
	    $this->assertFalse((new Post(['disableRender' => true]))->shouldRender());
	    $this->assertTrue((new Post(['disableRender' => false]))->shouldRender());
	}

	/** @test */
	public function it_asserts_whether_it_is_visible_aka_published()
	{
	    $this->assertFalse((new Post)->isVisible());

	    $yesterday = new Post([
	    	'published_at' => Carbon::yesterday()->format('Y-m-d')
	    ]);

	    $now = new Post([
	    	'published_at' => Carbon::now()->format('Y-m-d')
	    ]);

	    $tomorrow = new Post([
	    	'published_at' => Carbon::tomorrow()->format('Y-m-d')
	    ]);
	    
	    $this->assertTrue($yesterday->isVisible());
	    $this->assertTrue($yesterday->isPublished());
	    
	    $this->assertTrue($now->isVisible());
	    $this->assertTrue($now->isPublished());
	    
	    $this->assertFalse($tomorrow->isVisible());
	    $this->assertFalse($tomorrow->isPublished());
	}

	/** @test */
	public function it_provides_accessor_to_carbon_instance_if_published()
	{
	    $yesterday = Carbon::yesterday();

	    $post = new Post([
    		'published_at' => $yesterday->format('Y-m-d')
    	]);

	    $this->assertInstanceOf(Carbon::class, $post->getDate());

	    $this->assertTrue(
	    	$yesterday->isSameDay($post->getDate())
	    );
	}

	/** @test */
	public function it_provides_accessor_to_carbon_instance_of_now_if_not_published()
	{
		$date = (new Post)->getDate();

		$this->assertInstanceOf(Carbon::class, $date);

	    $this->assertTrue(
	    	Carbon::now()->isSameDay($date)
	    );
	}

	/** @test */
	public function it_returns_image_object_for_preview_not_set()
	{
	    $post = new Post;

		$this->assertInstanceOf(Image::class, $post->preview);
	}

	/** @test */
	public function it_returns_image_object_for_preview()
	{
	    $post = new Post([
			'previews' => [
				PreviewTest::previewImage('foo.jpg')
			]
		]);

		$this->assertInstanceOf(Image::class, $post->preview);
		$this->assertEquals('foo.jpg', $post->preview->getRelativePath());
	}

	/** @test */
	public function it_returns_image_object_for_hero()
	{
	    $post = new Post([
			'previews' => [
				PreviewTest::heroImage('foo.jpg')
			]
		]);

		$this->assertInstanceOf(Image::class, $post->hero);
		$this->assertEquals('foo.jpg', $post->hero->getRelativePath());
	}

	/** @test */
	public function it_returns_image_url_for_website_feature_if_exists()
	{
		ProjectBuilder::fromSlug('foo')
			->withUsedImages('hero')
			->create()
			->publishImages();

		$post = new Post([
			'previews' => [
				PreviewTest::heroImage('/foo/hero.jpg')
			]
		]);

		app(OpenGraph::class)->bootstrap([
			'image' => '_icons/foo.jpg'
		]);
		
		$this->assertEquals(
			'https://folio-engine.test/images/foo/hero-website-feature-compressed.jpg', 
			$post->getOpenGraph()->getImageUrl()
		);
	}

	/** @test */
	public function it_returns_default_image_url_for_website_feature_if_does_not_exists()
	{
		$post = new Post;

		app(OpenGraph::class)->bootstrap([
			'image' => '_icons/foo.jpg'
		]);
		
		$this->assertEquals(
			'https://folio-engine.test/images/_icons/foo.jpg', 
			$post->getOpenGraph()->getImageUrl()
		);
	}

	/** @test */
	public function it_returns_lead()
	{
	    $post = new Post([
	    	'lead_separator' => '<!--more-->', 
	    	'content' => 'foo<!--more-->bar'
	    ]);

	    $this->assertEquals('<p>foo</p>', $post->html()->getLead());
	    $this->assertStringNotContainsString('foo', $post->html()->getBody());
	    $this->assertEquals('<p>bar</p>', $post->html()->getBody());
	}

	/** @test */
	public function it_returns_empty_lead_if_not_specified()
	{
	    $post = new Post([
	    	'lead_separator' => '<!--more-->', 
	    	'content' => 'foo bar'
	    ]);

	    $this->assertEquals('', $post->html()->getLead());
	    $this->assertEquals('<p>foo bar</p>', $post->html()->getBody());
	}

	/** @test */
	public function it_returns_empty_lead_if_separator_not_specified()
	{
	    $post = new Post([
	    	'content' => 'foo <!--more--> bar'
	    ]);

	    $this->assertEquals('', $post->html()->getLead());
	    $this->assertEquals('<p>foo <!--more--> bar</p>', $post->html()->getBody());
	}

	/** @test */
	public function it_returns_null_if_client_is_not_set()
	{
	    $post = new Post([]);

		$this->assertNull($post->client);
	}

	/** @test */
	public function it_returns_client_object_if_client_is_set()
	{
	    $post = new Post([
	    	'client' => 'foo'
	    ]);

		$client = $post->client;

	    $this->assertInstanceOf(Client::class, $client);
	}

	/** @test */
	public function it_returns_default_client_object_if_cannot_be_found()
	{
		$post = new Post([
	    	'client' => 'foo'
	    ]);

		$client = $post->client;

	    $this->assertInstanceOf(Client::class, $client);
		$this->assertEquals('foo', $client->slug);
		$this->assertEquals('Foo', $client->title);
		$this->assertFalse($client->logo->exists());
	}

	/** @test */
	public function it_returns_client_object_from_the_registry()
	{
		app(Registry::class)
			->clients()
			->merge([
				['slug' => 'foo', 'title' => 'Bar', 'logo' => '/_clients/foo.svg']
			]);
		
		$this->createImageFolderForSlug('_clients');
		$this->createDummyImage(vfsStream::url('folio/contents/images/_clients/foo.svg'));

		$post = new Post(['client' => 'foo']);

		$client = $post->client;

	    $this->assertInstanceOf(Client::class, $client);
		$this->assertEquals('foo', $client->slug);
		$this->assertEquals('Bar', $client->title);
		$this->assertTrue($client->logo->exists());
	}

	/** @test */
	public function it_returns_empty_preview_collection_of_previews_if_none_is_set()
	{
		$post = new Post();

		$this->assertInstanceOf(PreviewCollection::class, $post->previews);
		$this->assertCount(0, $post->previews);
	}

	/** @test */
	public function it_returns_preview_collection_if_multiple_previews_are_set()
	{
		$this->createImageFolderForSlug('foo');
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/bar.jpg'));
		
		$post = new Post([
			'previews' => [
				PreviewTest::previewImage('foo/foo.jpg'),
				PreviewTest::heroImage('foo/bar.jpg'),
				PreviewTest::vimeoVideo('https://vimeo.com/1235'),
			]
		]);

		$previews = $post->previews;

		$this->assertInstanceOf(PreviewCollection::class, $previews);
		$this->assertCount(3, $previews);
		
		$this->assertEquals($this->baseUrlImages . '/foo/foo.jpg', $previews->getRow(0)->getResource());
		$this->assertEquals('image', $previews->getRow(0)->type);
		
		$this->assertEquals($this->baseUrlImages . '/foo/bar.jpg', $previews->getRow(1)->getResource());
		$this->assertEquals('image', $previews->getRow(1)->type);
		
		$this->assertEquals('https://vimeo.com/1235', $previews->getRow(2)->getResource());
		$this->assertEquals('video', $previews->getRow(2)->type);
	}

	/** @test */
	public function it_returns_excerpt_defined_as_attribute()
	{
		$post = new Post([
			'excerpt' => 'foo'
		]);

		$this->assertEquals('foo', $post->excerpt);
	}

	/** @test */
	public function it_returns_excerpt_defined_in_texts()
	{
		$post = new Post([
			'texts' => [
				'excerpt' => 'foo',
			]
		]);

		$this->assertEquals('foo', $post->excerpt);
	}

	/** @test */
	public function it_gives_precedence_to_excerpt_defined_in_texts()
	{
		$post = new Post([
			'excerpt' => 'foo',
			'texts' => [
				'excerpt' => 'bar',
			]
		]);

		$this->assertEquals('bar', $post->excerpt);
	}

	/** @test */
	public function it_returns_texts_collection()
	{
		$post = new Post;

		$this->assertInstanceOf(
			\App\Repositories\TextCollection::class, 
			$post->texts
		);
	}

	/** @test */
	public function it_returns_videos_collection()
	{
		$post = new Post;

		$this->assertInstanceOf(
			\App\Repositories\VideoCollection::class, 
			$post->videos
		);
	}

	/** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new Post;

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

	    $this->assertInstanceOf(
	    	\App\Resources\PostResource::class, 
	    	$instance->apiResource()
	    );
	}
}