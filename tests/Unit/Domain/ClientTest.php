<?php

namespace Tests\Unit\Domain;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Images\ResponsiveImage;
use App\Domain\Client;
use Carbon\Carbon;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ClientTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Client);
	}

    /** @test */
	public function it_returns_slug()
	{
	    $client = new Client([
	    	'slug' => 'foo'
	    ]);

		$this->assertEquals('foo', $client->slug);
	}

    /** @test */
	public function it_returns_title_based_on_slug_by_default()
	{
	    $client = new Client([
	    	'slug' => 'foo'
	    ]);

		$this->assertEquals('Foo', $client->title);
	}

    /** @test */
	public function it_returns_title_specified_by_attribute()
	{
	    $client = new Client([
	    	'slug' => 'foo',
            'title' => 'Bar'
	    ]);

		$this->assertEquals('Bar', $client->title);
	}

    /** @test */
	public function it_returns_image_instance_if_no_image_is_specified()
	{
	    $client = new Client([
	    	'slug' => 'foo'
	    ]);

		$this->assertInstanceOf(ResponsiveImage::class, $client->logo);
        $this->assertFalse($client->logo->exists());
	}

    /** @test */
	public function it_returns_image_instance_if_image_is_specified()
	{
        $this->createImageFolderForSlug('_clients');
        $this->createDummyImage(vfsStream::url('folio/contents/images/_clients/foo.svg'));
	    
        $client = new Client([
	    	'slug' => 'foo',
            'logo' => '_clients/foo.svg'
	    ]);

		$this->assertInstanceOf(ResponsiveImage::class, $client->logo);
        $this->assertTrue($client->logo->exists());
	}

	/** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new Client;

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

        $this->assertInstanceOf(
	    	\App\Resources\ClientResource::class, 
	    	$instance->apiResource()
	    );
	}

	/** @test */
	public function it_can_be_converted_to_string()
	{
	    $client = new Client([
	    	'slug' => 'foo'
	    ]);

		$this->assertEquals('Foo', (string)$client);
	}
}