<?php

namespace Tests\Unit\Themes\Manifests;

use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ManifestTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

	/** @test */
    public function it_can_be_instantiated()
    {
        $path = vfsStream::url('folio/manifest.json');
        $this->assertNotNull(new Manifest($path));
    }

    /** @test */
    public function it_implements_countable_interface()
    {
        $path = vfsStream::url('folio/manifest.json');
        $this->assertInstanceOf(\Countable::class, new Manifest($path));
    }

    /** @test */
    public function it_returns_filepath()
    {
        $instance = new Manifest(
            $manifestPath = vfsStream::url('folio/manifest.json')
        );

        $this->assertEquals($manifestPath, $instance->getFilePath());

        $instance = new Manifest(
            $anotherManifestPath = vfsStream::url('folio/public/images/another-manifest.json')
        );

        $this->assertEquals($anotherManifestPath, $instance->getFilePath());
    }

    /** @test */
    public function it_returns_filename()
    {
        $instance = new Manifest(
            vfsStream::url('folio/manifest.json')
        );
        $this->assertEquals('manifest.json', $instance->getFileName());

        $instance = new Manifest(
            vfsStream::url('folio/public/images/bar.json')
        );
        $this->assertEquals('bar.json', $instance->getFileName());
    }

    /** @test */
    public function it_asserts_it_exists()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertFalse($instance->exists());

        mkdir(vfsStream::url('folio/public/images'));
        file_put_contents($path, '');

        $this->assertTrue($instance->exists());
    }

    /** @test */
    public function it_adds_a_file()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertCount(0, $instance);

        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $result = $instance->addEntry($sourceImagePath, []);

        $this->assertSame($instance, $result);
        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_asserts_it_has_a_file()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $this->assertFalse($instance->has($sourceImagePath));

        $instance->addEntry($sourceImagePath, []);

        $this->assertTrue($instance->has($sourceImagePath));
    }

    /** @test */
    public function it_removes_a_file_and_returns_false()
    {
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertCount(0, $instance);

        $result = $instance->removeEntry($sourceImagePath);
        $this->assertSame($instance, $result);
        $this->assertCount(0, $instance);

        $instance->addEntry($sourceImagePath, []);
        $this->assertCount(1, $instance);

        $result = $instance->removeEntry($sourceImagePath);
        $this->assertSame($instance, $result);
        $this->assertCount(0, $instance);
    }

    /** @test */
    public function it_returns_outputs_for_a_file()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertCount(0, $instance);

        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');

        $this->assertEquals([], $instance->getEntry($sourceImagePath)->getOutputs());
        
        $instance->addEntry($sourceImagePath, ['output' => []]);

        $this->assertEquals([], $instance->getEntry($sourceImagePath)->getOutputs());
    }

    /** @test */
    public function it_returns_a_file()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertCount(0, $instance);

        $existingImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $this->createDummyImage($existingImagePath);
        $this->assertFileExists($existingImagePath);

        $instance->addEntry(
            $existingImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($existingImagePath)->get(),
                'output' => []
            ]
        );

        $this->assertEquals(
            $expected, 
            $instance->getEntry($existingImagePath)->toArray()
        );
    }

    /** @test */
    public function it_can_be_converted_to_array()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertEquals([], $instance->toArray());

        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $instance->addEntry(
            $sourceImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($sourceImagePath)->get(),
                'output' => []
            ]
        );

        $this->assertEquals([$sourceImagePath => $expected], $instance->toArray());
    }

    /** @test */
    public function it_can_be_converted_to_json()
    {
        $path = vfsStream::url('folio/manifest.json');
        $instance = new Manifest($path);

        $this->assertEquals(json_encode([]), $instance->toJson());

        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $destinationImagePath = vfsStream::url('folio/public/images/foo.jpg');
        
        $instance->addEntry(
            $sourceImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($sourceImagePath)->get(),
                'output' => [
                    ImageManifestData::fromFilePath($destinationImagePath)->get(),
                ]
            ]
        );

        $jsonManifest = json_decode($instance->toJson(), $assoc = true);
        $sourceRelativePath = 'contents/images/foo.jpg';
        $destinationRelativePath = 'public/images/foo.jpg';
        $this->assertEquals([$sourceRelativePath], array_keys($jsonManifest));
        $this->assertEquals(
            $sourceRelativePath, 
            $jsonManifest[$sourceRelativePath]['input']['path']
        );
        $this->assertEquals(
            $destinationRelativePath, 
            $jsonManifest[$sourceRelativePath]['output'][0]['path']
        );
    }

    /** @test */
    public function it_publishes_a_manifest_file()
    {
        mkdir(vfsStream::url('folio/public/images'));
        $path = vfsStream::url('folio/manifest.json');
        
        $instance = new Manifest($path);

        $this->assertFalse($instance->exists());

        $instance->publish();

        $this->assertTrue($instance->exists());
    }

    /** @test */
    public function it_publishes_only_existing_files()
    {
        mkdir(vfsStream::url('folio/public/images'));
        $path = vfsStream::url('folio/manifest.json');

        $existingImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $this->createDummyImage($existingImagePath);
        $this->assertFileExists($existingImagePath);

        $notExistingImagePath = vfsStream::url('folio/contents/images/bar.jpg');
        $this->assertFileDoesNotExist($notExistingImagePath);
        
        $instance = new Manifest($path);
        
        $instance->addEntry(
            $existingImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($existingImagePath)->get(),
                'output' => []
            ]
        );

        $instance->addEntry(
            $notExistingImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($notExistingImagePath)->get(),
                'output' => []
            ]
        );
        
        $this->assertCount(2, $instance);

        $instance->publish();

        $this->assertCount(1, $instance);
        $this->assertCount(1, json_decode(file_get_contents($instance->getFilePath()), $assoc = true));
    }

    /** @test */
    public function it_publishes_only_files_relative_paths()
    {
        mkdir(vfsStream::url('folio/public/images'));
        $path = vfsStream::url('folio/manifest.json');

        $existingImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $this->createDummyImage($existingImagePath);
        $this->assertFileExists($existingImagePath);

        $existingDestinationImagePath = vfsStream::url('folio/public/images/foo.jpg');
        $this->createDummyImage($existingDestinationImagePath);
        $this->assertFileExists($existingDestinationImagePath);
        
        $instance = new Manifest($path);
        
        $instance->addEntry(
            $existingImagePath, 
            $expected = [
                'input' => ImageManifestData::fromFilePath($existingImagePath)->get(),
                'output' => [
                    ImageManifestData::fromFilePath($existingDestinationImagePath)->get()
                ]
            ]
        );
        
        $instance->publish();

        $this->assertCount(1, $instance);
        $jsonManifest = json_decode(file_get_contents($instance->getFilePath()), $assoc = true);
        $this->assertEquals(
            ['contents/images/foo.jpg'], 
            array_keys($jsonManifest)
        );
        $this->assertEquals(
            'contents/images/foo.jpg', 
            $jsonManifest['contents/images/foo.jpg']['input']['path']
        );
        $this->assertEquals(
            'public/images/foo.jpg', 
            $jsonManifest['contents/images/foo.jpg']['output'][0]['path']
        );
    }

    /** @test */
    public function it_loads_a_file_if_it_exists()
    {
        $path = vfsStream::url('folio/public/foo.json');

        $expected = $this->publishTestManifest($path);

        $this->assertFileExists($path);

        $instance = new Manifest($path);
        
        $this->assertCount(0, $instance);
        
        $instance->load();
        
        $this->assertCount(1, $instance);
        $this->assertEquals($expected, $instance->toArray());
    }

    /** @test */
    public function it_loads_a_file_if_it_does_not_exist()
    {
        $path = vfsStream::url('folio/public/foo.json');

        $this->assertFileDoesNotExist($path);

        $instance = new Manifest($path);
        
        $this->assertCount(0, $instance);
        
        $instance->load();
        
        $this->assertCount(0, $instance);
        $this->assertEquals([], $instance->toArray());
    }

    /** @test */
    public function it_refreshes_a_loaded_file()
    {
        $path = vfsStream::url('folio/public/foo.json');

        $this->publishTestManifest($path);

        $instance = new Manifest($path);
        $this->assertCount(0, $instance);
        
        $instance->load();
        $this->assertCount(1, $instance);

        $anotherInstance = new Manifest($path);
        $anotherInstance->load();
        $this->addExistingFile(
            $anotherInstance, 
            vfsStream::url('folio/contents/images/bar.jpg'),
            vfsStream::url('folio/public/images/bar.jpg')
        );
        $anotherInstance->publish();

        $instance->refresh();
        $this->assertCount(2, $instance);
    }

    /** @test */
    public function it_loads_only_if_not_loaded()
    {
        $path = vfsStream::url('folio/public/foo.json');

        $this->publishTestManifest($path);

        $instance = new Manifest($path);
        $this->assertCount(0, $instance);
        
        $result = $instance->loadIfNotLoaded();
        $this->assertSame($instance, $result);
        $this->assertCount(1, $instance);

        $anotherInstance = (new Manifest($path));
        $anotherInstance->load();
        $this->addExistingFile(
            $anotherInstance, 
            vfsStream::url('folio/contents/images/bar.jpg'),
            vfsStream::url('folio/public/images/bar.jpg')
        );
        $anotherInstance->publish();

        $result = $instance->loadIfNotLoaded();
        $this->assertSame($instance, $result);
        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_provides_a_way_to_compare_checksum()
    {
        $manifest = new Manifest(
            vfsStream::url('folio/public/foo.json')
        );

        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $this->assertFileExists($sourceImage);

        $this->assertFalse(
            $manifest->getEntry($sourceImage)->isSameChecksum()
        );

        $manifest->addEntry($sourceImage, [
            'input' => ImageManifestData::fromFilePath($sourceImage)->get(), 
            'output' => []
        ]);

        $this->assertTrue(
            $manifest->getEntry($sourceImage)->isSameChecksum()
        );

        file_put_contents($sourceImage, '');

        $this->assertFalse(
            $manifest->getEntry($sourceImage)->isSameChecksum()
        );
    }

    private function publishTestManifest($path)
    {
        $instance = new Manifest($path);
        $this->assertFalse($instance->exists());
        
        $expected = $this->addExistingFile($instance);

        $instance->publish();

        return $expected;
    }

    private function addExistingFile($instance, $sourcePath = null, $destinationPath = null)
    {
        if ( ! file_exists(vfsStream::url('folio/public/images'))) {
            mkdir(vfsStream::url('folio/public/images'));
        }
        
        $sourcePath = $sourcePath ?: vfsStream::url('folio/contents/images/foo.jpg');
        $destinationPath = $destinationPath ?: vfsStream::url('folio/public/images/foo.jpg');

        $this->createDummyImage($sourcePath);
        $this->assertFileExists($sourcePath);

        $this->createDummyImage($destinationPath);
        $this->assertFileExists($destinationPath);
        
        $instance->addEntry(
            $sourcePath, 
            $data = [
                'input' => ImageManifestData::fromFilePath($sourcePath)->get(),
                'output' => [
                    ImageManifestData::fromFilePath($destinationPath)->get()
                ]
            ]
        );

        return [
            $sourcePath => $data
        ];
    }
}