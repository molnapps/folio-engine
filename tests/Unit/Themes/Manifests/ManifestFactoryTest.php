<?php

namespace Tests\Unit\Themes\Manifests;

use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\Manifest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ManifestFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_images_manifest()
    {
        $manifest = ManifestFactory::images();

        $this->assertInstanceOf(Manifest::class, $manifest);
        $this->assertEquals('manifest.img.json', $manifest->getFileName());
        $this->assertEquals('vfs://folio/manifests/manifest.img.json', $manifest->getFilePath());
    }

    /** @test */
    public function it_returns_assets_manifest()
    {
        $manifest = ManifestFactory::assets();

        $this->assertInstanceOf(Manifest::class, $manifest);
        $this->assertEquals('manifest.assets.json', $manifest->getFileName());
        $this->assertEquals('vfs://folio/manifests/manifest.assets.json', $manifest->getFilePath());
    }
}