<?php

namespace Tests\Unit\Themes\Publishers;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\BackupEraser;
use App\Themes\Publishers\ProcessedPublisher;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;

class BackupEraserTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;
    use InteractsWithManifest;

    private $sourceFolder;
    private $storageFolder;
    private $publicFolder;
    private $backupFolder;

    /** @before */
    public function setupPaths()
    {
        $this->sourceFolder = vfsStream::url('folio/contents/images');
        $this->storageFolder = vfsStream::url('folio/storage/images');
        $this->publicFolder = vfsStream::url('folio/public/images');
        $this->backupFolder = vfsStream::url('folio/public/images_backup');

        $this->createPublicFolder();
    }

	/** @test */
    public function it_can_be_instantiated()
    {
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            null
        );

        $this->assertNotNull($instance);
    }

    /** @test */
    public function it_does_not_erase_if_no_manifest_is_provided()
    {
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            null
        );

        $this->assertNull($instance->erase());
    }

    /** @test */
    public function it_erases_manifest()
    {
        $manifest = $this->getPublishedManifestInstance();

        $this->createBackupFolder();
        
        $this->assertTrue(ManifestFactory::images()->exists());

        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertFalse(ManifestFactory::images()->exists());
    }

    /** @test */
    public function it_erases_image_in_root()
    {
        $sourceImage = "{$this->sourceFolder}/foo.jpg";
        $backupImage = "{$this->backupFolder}/foo-compressed.jpg";

        $this->createDummyImage($sourceImage);
        
        $manifest = $this->getPublishedManifestInstance();
        
        $this->createBackupFolder();
        
        $this->assertFileExists($sourceImage);
        $this->assertFileExists($backupImage);
        $this->assertFileExists($this->backupFolder);

        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertFileExists($sourceImage);
        $this->assertFileDoesNotExist($backupImage);
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    /** @test */
    public function it_erases_image_in_folder()
    {
        $image = [
            'source' => "{$this->sourceFolder}/foo/foo.jpg",
            'backup' => "{$this->backupFolder}/foo/foo-compressed.jpg"
        ];

        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($image['source']);

        $manifest = $this->getPublishedManifestInstance();

        $this->createBackupFolder();
        
        $this->assertFileExists($image['source']);
        $this->assertFileExists($image['backup']);
        $this->assertFileExists($this->backupFolder);
        
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertFileExists($image['source']);
        $this->assertFileDoesNotExist($image['backup']);
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    /** @test */
    public function it_erases_multiple_images_in_folder()
    {
        $images = [
            [
                'source' => "{$this->sourceFolder}/foo/foo.jpg",
                'backup' => "{$this->backupFolder}/foo/foo-compressed.jpg"
            ],
            [
                'source' => "{$this->sourceFolder}/foo/bar.jpg",
                'backup' => "{$this->backupFolder}/foo/bar-compressed.jpg"
            ]
        ];

        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($images[0]['source']);
        $this->createDummyImage($images[1]['source']);

        $manifest = $this->getPublishedManifestInstance();

        $this->createBackupFolder();
        
        $this->assertFileExists($images[0]['source']);
        $this->assertFileExists($images[0]['backup']);
        
        $this->assertFileExists($images[1]['source']);
        $this->assertFileExists($images[1]['backup']);
        
        $this->assertFileExists($this->backupFolder);
        
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertFileExists($images[0]['source']);
        $this->assertFileDoesNotExist($images[0]['backup']);
        
        $this->assertFileExists($images[1]['source']);
        $this->assertFileDoesNotExist($images[1]['backup']);
        
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    /** @test */
    public function it_erases_multiple_files_in_manifest()
    {
        $images = $this->createWorld();

        $manifest = $this->getPublishedManifestInstance();

        $this->createBackupFolder();
        
        $this->assertSourceFilesExist($images);
        $this->assertBackupFilesExist($images);
        $this->assertFileExists($this->backupFolder);

        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertSourceFilesExist($images);
        $this->assertBackupFilesDoNotExist($images);
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    /** @test */
    public function it_does_not_erases_files_if_not_in_manifest()
    {
        $images = $this->createWorld();

        $manifest = $this->getPublishedManifestInstance();

        $foreignImage = [
            'public' => vfsStream::url('folio/public/images/bar/joe.jpg'),
            'backup' => vfsStream::url('folio/public/images_backup/bar/joe.jpg'),
        ];

        $this->createDummyImage($foreignImage['public']);

        $this->createBackupFolder();

        $this->assertSourceFilesExist($images);
        $this->assertBackupFilesExist($images);
        $this->assertFileExists($this->backupFolder);
        $this->assertFileExists($foreignImage['backup']);

        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertSourceFilesExist($images);
        $this->assertBackupFilesDoNotExist($images);
        $this->assertFileExists($this->backupFolder);
        $this->assertFileExists($foreignImage['backup']);
    }

    /** @test */
    public function it_erases_files_in_a_relative_folder()
    {
        $images = $this->createWorld();

        $manifest = $this->getPublishedManifestInstance();

        $localBackupFolder = $this->createBackupFolder('bar');
        $localBackupFile = "{$localBackupFolder}/bar-compressed.jpg";

        $this->assertSourceFilesExist($images);
        
        $this->assertFileExists($this->publicFolder);
        $this->assertFileExists($localBackupFolder);

        $this->assertFileExists($images['foo']['public']);
        $this->assertFileExists($localBackupFile);
        $this->assertFileExists($images['baz']['public']);

        $instance = new BackupEraser(
            $this->sourceFolder . '/bar', 
            $this->publicFolder . '/bar', 
            $localBackupFolder, 
            $manifest
        );

        $instance->erase();

        $this->assertSourceFilesExist($images);
        
        $this->assertFileExists($images['foo']['public']);
        $this->assertFileDoesNotExist($localBackupFile);
        $this->assertFileExists($images['baz']['public']);
        
        $this->assertFileExists($this->publicFolder);
        $this->assertFileDoesNotExist($localBackupFolder);
    }

    /** @test */
    public function it_erases_files_from_storage_folders_relative_path_storage()
    {
        // Given that I have images from source folder
        $sourceImage = "{$this->sourceFolder}/bar/bar.jpg";
        $sourceImagePublic = "{$this->publicFolder}/bar/bar-compressed.jpg";
        $sourceImageBackup = "{$this->publicFolder}/bar_backup/bar-compressed.jpg";
        
        // And images in the storage folder
        $storageImage = "{$this->storageFolder}/bar/bar-portrait.jpg";
        $storageImagePublic = "{$this->publicFolder}/bar/bar-portrait-compressed.jpg";
        $storageImageBackup = "{$this->publicFolder}/bar_backup/bar-portrait-compressed.jpg";

        // And they are in the manifest
        $this->unlinkDefaultImage();
        $this->createImageFolderForSlug('bar');
        $this->createDummyImage($sourceImage);

        $manifest = $this->getPublishedManifestInstanceWithFormats();

        $this->assertTrue( $manifest->has($sourceImage) );
        $this->assertTrue( $manifest->has($storageImage) );
        
        // When I have a backup
        $this->createBackupFolder('bar');
        
        $this->assertFileExists($sourceImage);
        $this->assertFileExists($storageImage);
        
        $this->assertFileExists($sourceImageBackup);
        $this->assertFileExists($storageImageBackup);

        // When I erase the files
        $instance = new BackupEraser(
            $this->sourceFolder . '/bar', 
            $this->publicFolder . '/bar', 
            $this->publicFolder . '/bar_backup', 
            $manifest
        );
        $instance->erase();
        
        // Then they both should be erased
        $this->assertFileExists($sourceImage);
        $this->assertFileExists($storageImage);
        
        $this->assertFileDoesNotExist($sourceImagePublic);
        $this->assertFileDoesNotExist($storageImageBackup);

        $this->assertFileExists($this->publicFolder);
    }

    /** @test */
    public function it_returns_list_of_folders_to_delete()
    {
        $this->createWorld();

        $manifest = $this->getPublishedManifestInstanceWithFormats();

        $this->createBackupFolder();
        
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $manifest
        );
        
        $result = $instance->getBackupFolderPathsToDelete();

        $this->assertCount(2, $result);
        $this->assertEquals([
            vfsStream::url('folio/public/images_backup/bar'),
            vfsStream::url('folio/public/images_backup/baz')
        ], $result);
    }

    /** @test */
    public function it_handles_presence_of_ds_store_file_in_backup_folder()
    {
        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $this->getPublishedManifestInstanceWithFormats()
        );

        $this->createBackupFolder();

        $this->createDSStoreFileInFolder($this->backupFolder);

        $this->assertFileExists($this->backupFolder);
        
        $instance->erase();
        
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    /** @test */
    public function it_handles_presence_of_ds_store_file_in_project_folder()
    {
        $this->createWorld();

        $instance = new BackupEraser(
            $this->sourceFolder, 
            $this->publicFolder, 
            $this->backupFolder, 
            $this->getPublishedManifestInstanceWithFormats()
        );

        $this->createDSStoreFileInFolder(
            vfsStream::url('folio/public/images/bar')
        );
        
        $this->createBackupFolder();

        $this->assertFileExists($this->backupFolder);
        
        $instance->erase();
        
        $this->assertFileDoesNotExist($this->backupFolder);
    }

    private function createDSStoreFileInFolder($folderPath)
    {
        file_put_contents(
            $folderPath . '/.DS_Store', 
            ''
        );
    }

    private function createWorld()
    {
        $images = [
            'foo' => [
                'source' => "{$this->sourceFolder}/foo.jpg",
                'public' => "{$this->publicFolder}/foo-compressed.jpg",
                'backup' => "{$this->backupFolder}/foo-compressed.jpg"
            ],
            'bar' => [
                'source' => "{$this->sourceFolder}/bar/bar.jpg",
                'public' => "{$this->publicFolder}/bar/bar-compressed.jpg",
                'backup' => "{$this->backupFolder}/bar/bar-compressed.jpg"
            ],
            'baz' => [
                'source' => "{$this->sourceFolder}/baz/baz.jpg",
                'public' => "{$this->publicFolder}/baz/baz-compressed.jpg",
                'backup' => "{$this->backupFolder}/baz/baz-compressed.jpg"
            ],
        ];

        $this->createImageFolderForSlug('bar');
        $this->createDummyImage($images['bar']['source']);
        
        $this->createImageFolderForSlug('baz');
        $this->createDummyImage($images['baz']['source']);

        return $images;
    }

    private function assertSourceFilesExist(array $images)
    {
        $this->assertFileExists($images['foo']['source']);
        $this->assertFileExists($images['bar']['source']);
        $this->assertFileExists($images['baz']['source']);
    }

    private function assertBackupFilesExist(array $images)
    {
        $this->assertFileExists($images['foo']['backup']);
        $this->assertFileExists($images['bar']['backup']);
        $this->assertFileExists($images['baz']['backup']);
    }

    private function assertBackupFilesDoNotExist(array $images)
    {
        $this->assertFileDoesNotExist($images['foo']['backup']);
        $this->assertFileDoesNotExist($images['bar']['backup']);
        $this->assertFileDoesNotExist($images['baz']['backup']);
    }

    private function createPublicFolder()
    {
        mkdir($this->publicFolder);
    }

    private function createBackupFolder($relativePath = '')
    {
        if ($relativePath) {
            rename(
                $localPublicFolder = "{$this->publicFolder}/{$relativePath}", 
                $localBackupFolder = "{$localPublicFolder}_backup"
            );

            return $localBackupFolder;
        }

        rename(
            $this->publicFolder, 
            $this->backupFolder
        );

        return $this->backupFolder;
    }

    private function getPublishedManifestInstanceWithFormats()
    {
        $manifest = $this->getManifestInstance();

		(new ProcessedPublisher)
            ->publish(
				$manifest,
				$this->sourceFolder,
				$this->publicFolder
			);

		$manifest->publish();

        return $manifest;
    }
}