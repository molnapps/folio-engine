<?php

namespace Tests\Unit\Themes\Publishers\Images\Crops;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Domain\Images\ImageSizeCalculator;
use App\Themes\Publishers\Manifests\CropsManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;
use App\Themes\Publishers\Images\Crops\CroppedImagesCreator;
use App\Themes\Publishers\Images\Sources\SourceProviderFactory;

class CroppedImagesCreatorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

    /** @test */
	public function it_can_be_instantiated()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');

        $instance = new CroppedImagesCreator($sourceImagePath, ManifestFactory::croppedImages());

	    $this->assertNotNull($instance);
	}

    /** @test */
	public function it_creates_different_formats_for_raster_image()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.jpg'),
        ];

        $this->createDummyImage($sourceImagePath, $this->getLandscapeSize());

        $this->assertFileExists($sourceImagePath);
        $this->assertFileDoesNotExist($destinationImagePaths['square']);
        $this->assertFileDoesNotExist($destinationImagePaths['portrait']);

        CroppedImagesCreator::fromSourceImagePath($sourceImagePath)->create();

        $this->assertFileExists($sourceImagePath);
        list ($sourceWidth, $sourceHeight) = getimagesize($sourceImagePath);

        $this->assertFormat($destinationImagePaths['square'], [
            'width' => $sourceHeight, 
            'height' => $sourceHeight
        ]);
        
        $this->assertFormat($destinationImagePaths['portrait'], [
            'width' => $sourceHeight * 4/5, 
            'height' => $sourceHeight
        ]);
	}

    /** @test */
	public function it_centers_the_crops()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $this->createDummyImage($sourceImagePath, $originalSize = $this->getLandscapeSize());

        $rectangle = CroppedImagesCreator::fromSourceImagePath($sourceImagePath)->getCropRectangle(
            ['suffix' => 'square', 'aspectRatio' => [1, 1]]
        );

        $this->assertIsArray($rectangle);
        $this->assertEquals([
            'x' => ($originalSize['width'] - $originalSize['height']) / 2, 
            'y' => 0, 
            'width' => $originalSize['height'], 
            'height' => $originalSize['height']
        ], $rectangle);
	}

    /** 
     * @test 
     * @dataProvider provideSupportedRasterFormats
     * */
	public function it_handles_all_supported_raster_formats($extension)
	{
        $sourceImagePath = vfsStream::url("folio/contents/images/foo.{$extension}");
        
        $destinationImagePaths = [
            'square' => vfsStream::url("folio/contents/images/foo-square.{$extension}"),
            'portrait' => vfsStream::url("folio/contents/images/foo-portrait.{$extension}"),
        ];

        $this->createDummyImage($sourceImagePath, $this->getLandscapeSize());

        $this->assertFileExists($sourceImagePath);
        $this->assertFileDoesNotExist($destinationImagePaths['square']);
        $this->assertFileDoesNotExist($destinationImagePaths['portrait']);

        CroppedImagesCreator::fromSourceImagePath($sourceImagePath)->create();

        $this->assertFileExists($sourceImagePath);
        $this->assertFileExists($destinationImagePaths['square']);
        $this->assertFileExists($destinationImagePaths['portrait']);
	}

    public function provideSupportedRasterFormats()
    {
        return array_map(
            function ($extension) {
                return [ $extension ];
            },
            SourceProviderFactory::supportedExtensions()
        );
    }

    /** @test */
	public function it_creates_different_formats_for_svg_image()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.svg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.svg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.svg'),
        ];

        $this->createDummyImage($sourceImagePath, $this->getLandscapeSize());

        $this->assertFileExists($sourceImagePath);
        $this->assertFileDoesNotExist($destinationImagePaths['square']);
        $this->assertFileDoesNotExist($destinationImagePaths['portrait']);

        CroppedImagesCreator::fromSourceImagePath($sourceImagePath)->create();

        $this->assertFileExists($sourceImagePath);
        $sourceSize = $this->getImageSize($sourceImagePath);
        
        $this->assertFormat($destinationImagePaths['square'], [
            'width' => $sourceSize['height'], 
            'height' => $sourceSize['height']
        ]);
        
        $this->assertFormat($destinationImagePaths['portrait'], [
            'width' => $sourceSize['height'] * 4/5, 
            'height' => $sourceSize['height']
        ]);

        $this->assertStringContainsString(
            'width="540" height="540"', 
            file_get_contents($destinationImagePaths['square'])
        );

        $this->assertStringContainsString(
            'width="432" height="540"', 
            file_get_contents($destinationImagePaths['portrait'])
        );
	}

    /** @test */
	public function it_skips_unsupported_formats()
	{
        $this->unlinkDefaultImage();

        $sourceFilePath = vfsStream::url('folio/contents/images/foo.json');
        
        file_put_contents($sourceFilePath, '');

        $this->assertFileExists($sourceFilePath);
        $this->assertEquals(['foo.json'], $this->getFilesInSourceFolder());

        CroppedImagesCreator::fromSourceImagePath($sourceFilePath)->create();

        $this->assertFileExists($sourceFilePath);
        $this->assertEquals(['foo.json'], $this->getFilesInSourceFolder());
	}

    /** @test */
	public function it_allows_to_specify_a_different_destination()
	{
        $destinationPath = vfsStream::url('folio/contents/images_cache');
        mkdir($destinationPath);
        
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images_cache/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images_cache/foo-portrait.jpg'),
        ];

        $this->createDummyImage($sourceImagePath, $this->getLandscapeSize());

        $this->assertFileExists($sourceImagePath);
        $this->assertEquals(['foo.jpg'], $this->getFilesInSourceFolder());
        $this->assertFileDoesNotExist($destinationImagePaths['square']);
        $this->assertFileDoesNotExist($destinationImagePaths['portrait']);

        CroppedImagesCreator::fromSourceImagePath($sourceImagePath)
            ->setDestinationPath($destinationPath)
            ->create();

        $this->assertFileExists($sourceImagePath);
        $this->assertEquals(['foo.jpg'], $this->getFilesInSourceFolder());
        $this->assertFileExists($destinationImagePaths['square']);
        $this->assertFileExists($destinationImagePaths['portrait']);
	}

    /** @test */
	public function it_does_not_create_crops_if_crops_already_exist()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.jpg'),
        ];

        $this->createDummyImage($sourceImagePath, ['width' => 960, 'height' => 540]);

        $manifest = ManifestFactory::croppedImages();
        
        CroppedImagesCreator::fromSourceImagePath($sourceImagePath, $manifest)->create();

        file_put_contents($destinationImagePaths['square'], 'foo');
        file_put_contents($destinationImagePaths['portrait'], 'bar');

        $originalSquare = $this->getCheckSum( $destinationImagePaths['square'] );
        $originalPortrait = $this->getCheckSum( $destinationImagePaths['portrait'] );

        CroppedImagesCreator::fromSourceImagePath($sourceImagePath, $manifest)->create();

        $this->assertEquals($originalSquare, $this->getCheckSum( $destinationImagePaths['square'] ));
        $this->assertEquals($originalPortrait, $this->getCheckSum( $destinationImagePaths['portrait'] ));
	}

    /** @test */
	public function it_creates_crops_if_crops_already_exist_but_source_image_was_changed()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.jpg'),
        ];

        $this->createDummyImage($sourceImagePath, ['width' => 960, 'height' => 540]);

        $manifest = ManifestFactory::croppedImages();
        
        CroppedImagesCreator::fromSourceImagePath($sourceImagePath, $manifest)->create();

        file_put_contents($destinationImagePaths['square'], 'foo');
        file_put_contents($destinationImagePaths['portrait'], 'bar');

        $originalSquare = $this->getCheckSum( $destinationImagePaths['square'] );
        $originalPortrait = $this->getCheckSum( $destinationImagePaths['portrait'] );

        $this->createDummyImage($sourceImagePath, ['width' => 720, 'height' => 440]);
        CroppedImagesCreator::fromSourceImagePath($sourceImagePath, $manifest)->create();

        $this->assertNotEquals($originalSquare, $this->getCheckSum( $destinationImagePaths['square'] ));
        $this->assertNotEquals($originalPortrait, $this->getCheckSum( $destinationImagePaths['portrait'] ));
	}

    /** @test */
	public function it_does_not_create_crops_for_picture_if_already_present_in_the_source_folder()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo/foo.png');
        $sourceImageSquare = vfsStream::url('folio/contents/images/foo/foo-square.png');
        $sourceImagePortrait = vfsStream::url('folio/contents/images/foo/foo-portrait.png');

        $storageImage = vfsStream::url('folio/storage/images/foo/foo.png');
        $storageImageSquare = vfsStream::url('folio/storage/images/foo/foo-square.png');
        $storageImagePortrait = vfsStream::url('folio/storage/images/foo/foo-portrait.png');

        $this->createSourceImageFolderForSlug('foo');
        $this->createStorageImageFolderForSlug('foo');
        $this->createDummyImage($sourceImage, $this->getLandscapeSize());
        $this->createDummyImage($sourceImageSquare, $this->getSquareSize());
        
        $this->assertFileExists($sourceImageSquare);
        
        $this->assertFileExists($sourceImage);
        $this->assertFileDoesNotExist($sourceImagePortrait);

        $this->assertFileDoesNotExist($storageImageSquare);
        $this->assertFileDoesNotExist($storageImagePortrait);
        
        CroppedImagesCreator::fromSourceImagePath($sourceImage)
            ->usesStorage()
            ->create();

        $this->assertFileExists($storageImageSquare);
        $this->assertFileExists($storageImagePortrait);
        
        $this->assertEquals(
            file_get_contents($storageImageSquare),
            file_get_contents($sourceImageSquare)
        );
	}

    /** @test */
	public function it_does_not_create_crops_if_image_is_narrower()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.jpg'),
        ];

        $this->createDummyImage(
            $sourceImagePath, 
            $this->getSquareSize()
        );
        
        CroppedImagesCreator::fromSourceImagePath($sourceImagePath)->create();

        $this->assertFileExists($sourceImagePath);
        $this->assertFileDoesNotExist($destinationImagePaths['square']);
        $this->assertFileExists($destinationImagePaths['portrait']);
	}

    /** @test */
	public function it_adds_image_to_manifest()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $destinationImagePaths = [
            'square' => vfsStream::url('folio/contents/images/foo-square.jpg'),
            'portrait' => vfsStream::url('folio/contents/images/foo-portrait.jpg'),
        ];

        $this->createDummyImage(
            $sourceImagePath, 
            $this->getSquareSize()
        );

        $manifest = ManifestFactory::croppedImages();

        $this->assertCount(0, $manifest);
        
        CroppedImagesCreator::fromSourceImagePath($sourceImagePath, $manifest)->create();

        $this->assertCount(1, $manifest);
        $this->assertTrue($manifest->has($sourceImagePath));

        $expected = [
            'input' => ImageManifestData::fromFilePath($sourceImagePath)->get(),
            'output' => [
                ImageManifestData::fromFilePath($destinationImagePaths['portrait'])->get(),
            ]
        ];

        $this->assertEquals(
            $expected,
            $manifest->getEntry($sourceImagePath)->toArray()
        );
	}

    /** @test */
	public function it_allows_to_specify_a_map_and_returns_self()
	{
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = CroppedImagesCreator::fromSourceImagePath(
            $sourceImagePath, 
            ManifestFactory::croppedImages()
        );

        $result = $instance->setMap(new CroppedImagesMap);
        
        $this->assertSame($instance, $result);
	}

    /** @test */
	public function it_crops_images_according_to_map()
	{
        $processedImage = vfsStream::url('folio/contents/images/foo.jpg');
        $skippedImage = vfsStream::url('folio/contents/images/skipped.jpg');
        $limitedImage = vfsStream::url('folio/contents/images/limited.jpg');
        
        $this->createDummyImage($processedImage, $this->getLandscapeSize());
        $this->createDummyImage($skippedImage, $this->getLandscapeSize());
        $this->createDummyImage($limitedImage, $this->getLandscapeSize());
        
        $manifest = ManifestFactory::croppedImages();
        
        $map = new CroppedImagesMap;
        $map->add($skippedImage, ['skip' => true]);
        $map->add($limitedImage, ['crop-limit' => 'square']);

        $this->assertCount(0, $manifest);

        CroppedImagesCreator::fromSourceImagePath(
            $processedImage, 
            $manifest
        )->setMap($map)->create();
        
        $this->assertCount(1, $manifest);

        CroppedImagesCreator::fromSourceImagePath(
            $skippedImage, 
            $manifest
        )->setMap($map)->create();

        $this->assertCount(1, $manifest);

        CroppedImagesCreator::fromSourceImagePath(
            $limitedImage, 
            $manifest
        )->setMap($map)->create();

        $this->assertCount(2, $manifest);
        
        $this->assertEquals(
            [
                'foo-portrait.jpg', 
                'foo-square.jpg', 
                'foo.jpg', 
                'limited-square.jpg', 
                'limited.jpg', 
                'skipped.jpg'
            ], 
            $this->getFilesInSourceFolder()
        );
	}

    private function getCheckSum($path)
    {
        return md5(
            file_get_contents($path)
        );
    }

    private function getLandscapeSize()
    {
        return ['width' => 960, 'height' => 540];
    }

    private function getSquareSize()
    {
        return ['width' => 540, 'height' => 540];
    }

    private function getFilesInSourceFolder()
    {
        return array_values(
            array_diff(
                scandir($this->baseSourcePathImages), 
                ['.', '..']
            )
        );
    }

    private function assertFormat($destinationImagePath, array $expected)
    {
        $this->assertFileExists($destinationImagePath);
        
        $size = $this->getImageSize($destinationImagePath);
        
        $this->assertEquals($expected['width'], $size['width']);
        $this->assertEquals($expected['height'], $size['height']);
    }

    private function getImageSize($path)
    {
        return (new ImageSizeCalculator($path))->get();
    }
}