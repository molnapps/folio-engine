<?php

namespace Tests\Unit\Themes\Publishers\Images\Crops;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;

class CroppedImagesMapTest extends TestCase
{
    use InteractsWithFileSystem;
    
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CroppedImagesMap);
	}

    /** @test */
	public function it_returns_all_formats_if_empty()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $result = $instance->getFormats($sourceImage);

        $this->assertEquals(['square', 'portrait'], $result);
	}

    /** @test */
	public function it_returns_no_formats_if_skipped()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceImage, ['skip' => true]);

        $result = $instance->getFormats($sourceImage);

        $this->assertEquals([], $result);
	}

    /** @test */
	public function it_returns_format_if_crop_limit_is_defined_as_square()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceImage, ['crop-limit' => 'square']);

        $result = $instance->getFormats($sourceImage);

        $this->assertEquals(['square'], $result);
	}

    /** @test */
	public function it_returns_formats_if_crop_limit_is_defined_landscape()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceImage, ['crop-limit' => 'landscape']);

        $result = $instance->getFormats($sourceImage);

        $this->assertEquals([], $result);
	}

    /** @test */
	public function it_returns_format_if_crop_limit_is_defined_as_portrait()
	{
        $sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceImage, ['crop-limit' => 'portrait']);

        $result = $instance->getFormats($sourceImage);

        $this->assertEquals(['square', 'portrait'], $result);
	}

    /** @test */
	public function it_returns_formats_for_folders()
	{
        $sourceFolder = vfsStream::url('folio/contents/images/foo');
        $anotherSourceFolder = vfsStream::url('folio/contents/images/bar');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceFolder, ['skip' => true]);

        $this->assertEquals(
            [], 
            $instance->getFormats($sourceFolder . '/foo-1.jpg')
        );
        $this->assertEquals(
            [], 
            $instance->getFormats($sourceFolder . '/foo-2.jpg')
        );
        
        $this->assertEquals(
            ['square', 'portrait'], 
            $instance->getFormats($anotherSourceFolder . '/bar.jpg')
        );
	}

    /** @test */
	public function it_returns_formats_for_folders_2()
	{
        $sourceFolder = vfsStream::url('folio/contents/images/foo');
        $anotherSourceFolder = vfsStream::url('folio/contents/images/bar');
        
        $instance = new CroppedImagesMap;

        $instance->add($sourceFolder, ['crop-limit' => 'square']);

        $this->assertEquals(
            ['square'], 
            $instance->getFormats($sourceFolder . '/foo-1.jpg')
        );
        $this->assertEquals(
            ['square'], 
            $instance->getFormats($sourceFolder . '/foo-2.jpg')
        );
        
        $this->assertEquals(
            ['square', 'portrait'], 
            $instance->getFormats($anotherSourceFolder . '/bar.jpg')
        );
	}

    /** @test */
	public function it_provides_load_method_and_returns_self()
	{
        $configFile = vfsStream::url('folio/contents/images/images.json');
        $sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');

        file_put_contents(
            $configFile,
            json_encode([
                'foo.jpg' => ['skip' => true]
            ])
        );

        $instance = new CroppedImagesMap;

        $this->assertFalse(
            $instance->hasExactly($sourceImagePath)
        );

        $result = $instance->load();

        $this->assertSame($instance, $result);

        $this->assertTrue(
            $instance->hasExactly($sourceImagePath)
        );
	}

    /** @test */
	public function it_provides_add_array_method_and_returns_self()
	{
        $fooImagePath = vfsStream::url('folio/contents/images/foo/foo.jpg');
        $barImagePath = vfsStream::url('folio/contents/images/bar/bar.jpg');

        $instance = new CroppedImagesMap;

        $this->assertFalse(
            $instance->hasExactly($fooImagePath)
        );
        $this->assertFalse(
            $instance->hasExactly($barImagePath)
        );

        $result = $instance->addArrayMap([
            'foo/foo.jpg' => ['skip' => true],
            'bar/bar.jpg' => ['skip' => true]
        ]);

        $this->assertSame($instance, $result);

        $this->assertTrue(
            $instance->hasExactly($fooImagePath)
        );
        $this->assertTrue(
            $instance->hasExactly($barImagePath)
        );
	}

    /** @test */
	public function it_provides_skip_method_and_returns_self()
	{
        $fooImagePath = vfsStream::url('folio/contents/images/foo/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $this->assertFalse(
            $instance->hasExactly($fooImagePath)
        );
        
        $result = $instance->skip($fooImagePath);

        $this->assertSame($instance, $result);

        $this->assertTrue(
            $instance->hasExactly($fooImagePath)
        );
        $this->assertEquals(
            [],
            $instance->getFormats($fooImagePath)
        );
	}

    /** @test */
	public function it_provides_add_with_relative_path_method_and_returns_self()
	{
        $fooImagePath = vfsStream::url('folio/contents/images/foo/foo.jpg');
        
        $instance = new CroppedImagesMap;

        $this->assertFalse(
            $instance->hasExactly($fooImagePath)
        );
        
        $result = $instance->addWithRelativePath('foo/foo.jpg', []);

        $this->assertSame($instance, $result);

        $this->assertTrue(
            $instance->hasExactly($fooImagePath)
        );
	}

    /** @test */
	public function it_provides_add_with_relative_path_method_and_returns_self_with_slash()
	{
        $barImagePath = vfsStream::url('folio/contents/images/bar/bar.jpg');
        
        $instance = new CroppedImagesMap;

        $this->assertFalse(
            $instance->hasExactly($barImagePath)
        );
        
        $result = $instance->addWithRelativePath('/bar/bar.jpg', []);

        $this->assertSame($instance, $result);

        $this->assertTrue(
            $instance->hasExactly($barImagePath)
        );
	}
}