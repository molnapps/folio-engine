<?php

namespace Tests\Unit\Themes\Publishers\Images\Crops;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Manifests\CropsManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

class CropsManifestTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;

    private $sourceImagePath;
	private $filePath;

    /** @before */
    public function setUpManifest()
    {
        $this->sourceImagePath = vfsStream::url('folio/contents/images/foo.jpg');
        $this->filePath = vfsStream::url('folio/manifests/manifest.crops.json');
    }
	
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = ManifestFactory::croppedImages();

	    $this->assertNotNull($instance);
	}

    /** @test */
	public function it_implements_countable_interface()
	{
        $instance = ManifestFactory::croppedImages();

	    $this->assertInstanceOf(\Countable::class, $instance);
	}

    /** @test */
	public function it_accepts_a_file_if_it_exists()
	{
        $instance = ManifestFactory::croppedImages();

	    $this->assertCount(0, $instance);

        $instance->addEntry($this->sourceImagePath);

        $this->assertCount(1, $instance);
	}

    /** @test */
	public function it_asserts_it_has_a_file()
	{
        $instance = ManifestFactory::croppedImages();

	    $this->assertFalse(
            $instance->has($this->sourceImagePath)
        );

        $instance->addEntry($this->sourceImagePath);

        $this->assertTrue(
            $instance->has($this->sourceImagePath)
        );
	}

    /** @test */
	public function it_asserts_if_source_file_was_changed_without_file()
	{
        $instance = ManifestFactory::croppedImages();

        $this->assertFalse(
            $instance->getEntry($this->sourceImagePath)->isSameChecksum()
        );
	}

    /** @test */
	public function it_asserts_if_source_file_was_not_changed()
	{
        $instance = ManifestFactory::croppedImages();
        $instance->addFormats($this->sourceImagePath);

        $this->assertTrue(
            $instance->getEntry($this->sourceImagePath)->isSameChecksum()
        );
	}

    /** @test */
	public function it_asserts_if_source_file_was_changed()
	{
        $instance = ManifestFactory::croppedImages();

        $instance->addFormats($this->sourceImagePath);

        $this->assertTrue(
            $instance->getEntry($this->sourceImagePath)->isSameChecksum()
        );

        file_put_contents($this->sourceImagePath, 'foobar');

        $this->assertFalse(
            $instance->getEntry($this->sourceImagePath)->isSameChecksum()
        );
	}

    /** @test */
	public function it_writes_empty_manifest_file()
	{
        $instance = ManifestFactory::croppedImages();
        
        $this->assertFileDoesNotExist($this->filePath);

        $instance->publish();

        $this->assertFileExists($this->filePath);

        $expectedContent = [];
        
        $this->assertEquals(
            json_encode($expectedContent), 
            file_get_contents($this->filePath)
        );
	}

    /** @test */
	public function it_writes_manifest_file()
	{
        $instance = ManifestFactory::croppedImages();
        $instance->addFormats($this->sourceImagePath);
        
        $this->assertFileDoesNotExist($this->filePath);

        $instance->publish();

        $this->assertFileExists($this->filePath);

        $expectedContent = [
            'contents/images/foo.jpg' => [
                'input' => [
                    'checksum' => '4b21b2343268b5c99210c3b4310b2eb0',
                    'bytes' => 752,
                    'width' => 120,
                    'height' => 20,
                    'path' => 'contents/images/foo.jpg',
                    'format' => 'jpg'
                ],
                'output' => []
            ]
        ];

        $this->assertEquals(
            json_encode($expectedContent), 
            file_get_contents($this->filePath)
        );
	}

    /** @test */
	public function it_reads_manifest_file()
	{
        (ManifestFactory::croppedImages())
            ->addFormats($this->sourceImagePath)
            ->publish();

        $instance = ManifestFactory::croppedImages();
        $this->assertCount(0, $instance);
        
        $result = $instance->loadIfNotLoaded();
        
        $this->assertSame($instance, $result);
        $this->assertCount(1, $instance);
	}

    /** @test */
	public function it_reads_manifest_file_if_does_not_exist()
	{
        $this->assertFileDoesNotExist($this->filePath);

        $instance = ManifestFactory::croppedImages();
        
        $this->assertCount(0, $instance);
        
        $result = $instance->loadIfNotLoaded();
        
        $this->assertSame($instance, $result);
        $this->assertCount(0, $instance);
	}
}