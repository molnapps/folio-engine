<?php

namespace Tests\Unit\Themes\Publishers\Images\Exports;

use App\Themes\Publishers\Images\Exports\ConflictResolver;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ConflictResolverTest extends TestCase
{
	/** @before */
	public function setUpVfsStream()
	{
		vfsStream::setup('folio', null, $structure = []);
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ConflictResolver);
	}

	/** @test */
	public function it_resolves_not_existing_file_name()
	{
	    $instance = new ConflictResolver;

	    $this->assertEquals(
	    	vfsStream::url('folio/foo.jpg'), 
	    	$instance->resolve(vfsStream::url('folio/foo.jpg'))
	    );
	}

	/** @test */
	public function it_resolves_existing_file_name()
	{
		$this->writeEmptyFile($source = vfsStream::url('folio/foo.jpg'));

	    $instance = new ConflictResolver;

	    $this->assertEquals(
	    	vfsStream::url('folio/foo-1.jpg'), 
	    	$instance->resolve($source)
	    );
	}

	/** @test */
	public function it_resolves_existing_file_name_incrementally()
	{
		$this->writeEmptyFile($source = vfsStream::url('folio/foo.jpg'));
		$this->writeEmptyFile(vfsStream::url('folio/foo-1.jpg'));

	    $instance = new ConflictResolver;

	    $this->assertEquals(
	    	vfsStream::url('folio/foo-2.jpg'), 
	    	$instance->resolve($source)
	    );
	}

	/** @test */
	public function it_resolves_existing_file_name_incrementally_with_limit()
	{
		$this->writeEmptyFile($source = vfsStream::url('folio/foo.jpg'));
		$this->writeEmptyFile(vfsStream::url('folio/foo-1.jpg'));

	    $instance = new ConflictResolver($maximumIterations = 1);

	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage('Too many iterations');

	    $instance->resolve($source);
	}

	private function writeEmptyFile($path)
	{
		file_put_contents($path, '');
	}
}