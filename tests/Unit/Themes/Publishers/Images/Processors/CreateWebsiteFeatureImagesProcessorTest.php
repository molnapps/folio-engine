<?php

namespace Tests\Unit\Themese\Publishers\Images\Processors;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Processors\Processor;
use App\Themes\Publishers\Images\Processors\ProcessorProvider;
use App\Themes\Publishers\Images\Processors\WebsiteFeatureImagesProvider;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use App\Themes\Publishers\Images\Processors\ArrayImagesProvider;
use App\Themes\Publishers\Images\Processors\CreateWebsiteFeatureImagesProcessor;

class CreateWebsiteFeatureImagesProcessorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;
	use InteractsWithManifest;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CreateWebsiteFeatureImagesProcessor);
	}

	/** @test */
	public function it_is_instance_of_processor()
	{
	    $this->assertInstanceOf(Processor::class, new CreateWebsiteFeatureImagesProcessor);
	}
	
	/** @test */
	public function it_is_instance_of_processor_provider()
	{
	    $this->assertInstanceOf(ProcessorProvider::class, new CreateWebsiteFeatureImagesProcessor);
	}

	/** @test */
	public function it_creates_website_feature_images()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();

        $this->createStorageImageFolderForSlug('foo');
        $sourceFile = vfsStream::url('folio/contents/images/foo/hero.jpg');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        $dumpFile = vfsStream::url('folio/storage/images/dump.jpg');
        $expectedRect = [
            'width' => 1200,
            'height' => 630,
            'x' => 0,
            'y' => 0
        ];
        
        $this->createRandomNoiseImage($sourceFile);
        $this->createCroppedImage($sourceFile, $dumpFile, $expectedRect);

        $instance = new CreateWebsiteFeatureImagesProcessor;
        
        $this->assertFileDoesNotExist($targetFile);

		$instance->process(new WebsiteFeatureImagesProvider);

        $this->assertFileExists($targetFile);
        
        list ($width, $height) = getimagesize($targetFile);

        $this->assertEquals($width, $expectedRect['width']);
        $this->assertEquals($height, $expectedRect['height']);

        $this->assertEquals(
            file_get_contents($dumpFile), 
            file_get_contents($targetFile)
        );

	}

    /** @test */
	public function it_returns_images()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        
        $this->createStorageImageFolderForSlug('foo');
        $sourceFile = vfsStream::url('folio/contents/images/foo/hero.jpg');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertEquals([], $instance->getImages());

		$instance->process(new WebsiteFeatureImagesProvider);

        $this->assertEquals([
            $targetFile => [ $targetFile ]
        ], $instance->getImages());
	}

    /** @test */
	public function it_create_manifest_for_website_feature_images()
	{
        $manifestFile = ManifestFactory::websiteFeatureImages()->getFilePath();
        
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertFileDoesNotExist($manifestFile);

		$instance->process(new WebsiteFeatureImagesProvider);

        $this->assertFileExists($manifestFile);
	}

    /** @test */
	public function it_adds_images_to_manifest()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        $this->createStorageImageFolderForSlug('foo');

        $sourceImagePath = vfsStream::url('folio/contents/images/foo/hero.jpg');

        $manifest = ManifestFactory::websiteFeatureImages();
        
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertFalse($manifest->load()->has($sourceImagePath));

		$instance->process(new WebsiteFeatureImagesProvider);
        
        $this->assertTrue($manifest->refresh()->has($sourceImagePath));
        $this->assertCount(1, $manifest->getEntry($sourceImagePath)->getOutputs());
        $this->assertEquals(
            vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg'), 
            $manifest->getEntry($sourceImagePath)->getOutputs()[0]['path']
        );
	}

    /** @test */
	public function it_does_not_create_website_feature_images_twice()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        
        $this->createStorageImageFolderForSlug('foo');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertFileDoesNotExist($targetFile);

		$instance->process(new WebsiteFeatureImagesProvider);

        $this->assertFileExists($targetFile);
        $this->changeTargetFileToVerifyProcessing($targetFile);

        $instance->process(new WebsiteFeatureImagesProvider);
        
        $this->assertTargetFileWasNotProcessed($targetFile);
	}

    /** @test */
	public function it_creates_website_feature_images_twice_if_source_file_changed()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        
        $this->createStorageImageFolderForSlug('foo');
        $sourceFile = vfsStream::url('folio/contents/images/foo/hero.jpg');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertFileDoesNotExist($targetFile);

		$instance->process(new WebsiteFeatureImagesProvider);

        $this->assertFileExists($targetFile);
        $this->changeTargetFileToVerifyProcessing($targetFile);
        $this->changeSourceFileToVerifyProcessing($sourceFile);

        $instance->process(new WebsiteFeatureImagesProvider);
        
        $this->assertTargetFileWasProcessed($targetFile);
	}

    /** @test */
	public function it_creates_website_feature_images_twice_if_target_file_does_not_exist()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        
        $this->createStorageImageFolderForSlug('foo');
        $sourceFile = vfsStream::url('folio/contents/images/foo/hero.jpg');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        $instance = new CreateWebsiteFeatureImagesProcessor;

        $this->assertFileDoesNotExist($targetFile);

		$instance->process(new WebsiteFeatureImagesProvider);
        $this->assertFileExists($targetFile);
        
        $this->deleteTargetFile($targetFile);
        $this->assertFileDoesNotExist($targetFile);
        
        $instance->process(new WebsiteFeatureImagesProvider);
        $this->assertFileExists($targetFile);
	}

    /** @test */
	public function it_returns_images_even_if_images_were_already_processed()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        
        $this->createStorageImageFolderForSlug('foo');
        $targetFile = vfsStream::url('folio/storage/images/foo/hero-website-feature.jpg');
        
        $expectedImages = [
            $targetFile => [ $targetFile ]
        ];

        $instance = new CreateWebsiteFeatureImagesProcessor;
        $instance->process(new WebsiteFeatureImagesProvider);
        $this->assertEquals($expectedImages, $instance->getImages());

        $instance = new CreateWebsiteFeatureImagesProcessor;
        $instance->process(new WebsiteFeatureImagesProvider);
        $this->assertEquals($expectedImages, $instance->getImages());
	}

    /** @test */
	public function it_handles_dsstore_files()
	{
        $instance = new CreateWebsiteFeatureImagesProcessor;
		
        $instance->process(
            new ArrayImagesProvider([
                vfsStream::url('folio/contents/images/.DS_Store')
            ])
        );
	}

    private function changeTargetFileToVerifyProcessing($targetFile)
    {
        $this->assertNotEquals('', file_get_contents($targetFile));
        file_put_contents($targetFile, '');
    }

    private function changeSourceFileToVerifyProcessing($sourceFile)
    {
        $this->createDummyImage($sourceFile, ['width' => 100, 'height' => 50]);
    }

    private function assertTargetFileWasNotProcessed($targetFile)
    {
        $this->assertEquals('', file_get_contents($targetFile));
    }

    private function assertTargetFileWasProcessed($targetFile)
    {
        $this->assertNotEquals('', file_get_contents($targetFile));
    }

    private function deleteTargetFile($targetFile)
    {
        unlink($targetFile);
    }
}