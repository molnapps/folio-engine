<?php

namespace Tests\Unit\Themese\Publishers\Images\Processors;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Processors\ProcessorProvider;
use App\Themes\Publishers\Images\Processors\WebsiteFeatureImagesProvider;

class WebsiteFeatureImagesProviderTest extends TestCase
{
	use InteractsWithFileSystem;
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertInstanceOf(ProcessorProvider::class, new WebsiteFeatureImagesProvider);
	}

    /** @test */
	public function it_returns_empty_array_with_no_projects_and_pages()
	{
        $instance = new WebsiteFeatureImagesProvider;

	    $this->assertEquals([], $instance->getImages());
	}

    /** @test */
	public function it_returns_array_with_a_project_with_hero()
	{
        ProjectBuilder::fromSlug('foo')->withUsedImages('hero')->create();
        ProjectBuilder::fromSlug('bar')->withUsedImages('foo')->create();

        $heroSourcePath = vfsStream::url('folio/contents/images/foo/hero.jpg');

        $instance = new WebsiteFeatureImagesProvider;

        $result = $instance->getImages();
        
        $this->assertCount(1, $result);
	    $this->assertEquals([
            $heroSourcePath => [
                $heroSourcePath
            ]
        ], $result);
	}

    /** @test */
	public function it_returns_array_with_a_project_with_website_feature()
	{
        ProjectBuilder::fromSlug('foo')->withWebsiteFeatureImage('img')->create();
        ProjectBuilder::fromSlug('bar')->withUsedImages('foo')->create();

        $imageSourcePath = vfsStream::url('folio/contents/images/foo/img.jpg');

        $instance = new WebsiteFeatureImagesProvider;

        $result = $instance->getImages();
        
        $this->assertCount(1, $result);
	    $this->assertEquals([
            $imageSourcePath => [
                $imageSourcePath
            ]
        ], $result);
	}

    /** @test */
	public function it_returns_array_with_a_page_with_website_feature()
	{
        ProjectBuilder::fromSlug('foo')->isPage()->withWebsiteFeatureImage('img')->create();
        ProjectBuilder::fromSlug('bar')->isPage()->withUsedImages('foo')->create();

        $imageSourcePath = vfsStream::url('folio/contents/images/foo/img.jpg');

        $instance = new WebsiteFeatureImagesProvider;

        $result = $instance->getImages();
        
        $this->assertCount(1, $result);
	    $this->assertEquals([
            $imageSourcePath => [
                $imageSourcePath
            ]
        ], $result);
	}

	/** @test */
	public function it_returns_array_with_a_project_with_hero_with_source_path()
	{
        ProjectBuilder::fromSlug('foo')->withWebsiteFeatureImage('img')->create();
        ProjectBuilder::fromSlug('bar')->withWebsiteFeatureImage('img')->create();

        $imageSourcePath = vfsStream::url('folio/contents/images/foo/img.jpg');

        $instance = new WebsiteFeatureImagesProvider(
			vfsStream::url('folio/contents/images/foo')
		);

        $result = $instance->getImages();
        
        $this->assertCount(1, $result);
	    $this->assertEquals([
            $imageSourcePath => [
                $imageSourcePath
            ]
        ], $result);
	}

	/** @test */
	public function it_returns_only_published_projects()
	{
        ProjectBuilder::fromSlug('foo')
			->withWebsiteFeatureImage('img')
			->create();
        
		ProjectBuilder::fromSlug('bar')
			->withWebsiteFeatureImage('img')
			->isNotPublished()
			->create();

        $imageSourcePath = vfsStream::url('folio/contents/images/foo/img.jpg');

        $instance = new WebsiteFeatureImagesProvider();

        $result = $instance->getImages();
        
        $this->assertCount(1, $result);
	    $this->assertEquals([
            $imageSourcePath => [
                $imageSourcePath
            ]
        ], $result);
	}
}