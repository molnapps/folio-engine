<?php

namespace Tests\Unit\Themese\Publishers\Images\Processors;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use App\Themes\Publishers\Images\Options\Options;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Testing\TestImage;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Output\OutputFactory;
use App\Themes\Publishers\Images\Processors\Processor;
use App\Themes\Publishers\Images\Processors\AllImagesProvider;
use App\Themes\Publishers\Images\Processors\ProcessorProvider;
use App\Themes\Publishers\Images\Processors\UsedImagesProvider;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use App\Themes\Publishers\Images\Processors\CompressImagesProcessor;

class CompressImagesProcessorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;
	use InteractsWithManifest;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CompressImagesProcessor);
	}

	/** @test */
	public function it_is_instance_of_processor()
	{
	    $this->assertInstanceOf(Processor::class, new CompressImagesProcessor);
	}
	
	/** @test */
	public function it_is_instance_of_processor_provider()
	{
	    $this->assertInstanceOf(ProcessorProvider::class, new CompressImagesProcessor);
	}

	/** @test */
	public function it_compresses_images()
	{
		$this->createPublishedProjectWithImage('foo');
		
		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$this->assertFilesDoNotExist($targetFiles);
		
		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);

		$this->assertFilesCount('foo', $targetFiles);
		$this->assertFilesExist($targetFiles);
	}

	/** @test */
	public function it_returns_self_as_provider()
	{
		$this->createPublishedProjectWithImage('foo');
		
		$instance = new CompressImagesProcessor;
		$result = $instance->process(new UsedImagesProvider);

		$this->assertInstanceOf(ProcessorProvider::class, $result);
        $this->assertSame($instance, $result);
	}

	/** @test */
	public function it_returns_images()
	{
		$this->createPublishedProjectWithImage('foo');
		
		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$instance = new CompressImagesProcessor;
		$result = $instance->process(new UsedImagesProvider);

		$this->assertIsArray($instance->getImages());
		$this->assertEquals([
			$this->getSourceImage('foo') => $targetFiles
		], $instance->getImages());
	}

	/** @test */
	public function it_returns_images_for_specific_path_if_manifest_was_previously_published()
	{
		$this->createPublishedProjectWithImage('foo');
		$this->createPublishedProjectWithImage('bar');
		
		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		(new CompressImagesProcessor)->process(
			new UsedImagesProvider
		);

		$instance = new CompressImagesProcessor;
		
		$result = $instance->process(
			new UsedImagesProvider(
				vfsStream::url('folio/contents/images/foo')
			)
		);

		$this->assertIsArray($instance->getImages());
		$this->assertEquals([
			$this->getSourceImage('foo') => $targetFiles
		], $instance->getImages());
	}

	/** @test */
	public function it_handles_uncompressed_images()
	{
		$slug = 'foo';

		$this->createImageFolderForSlug($slug);
		
		$this->createDummyImage(
			vfsStream::url("folio/contents/images/{$slug}/{$slug}.svg")
		);
		
		$this->createPublishedProjectFile([
			'slug' => $slug, 
			'markdown' => "![{$slug}]"
		]);

		mkdir($this->getTargetFolder($slug));
		
        $destinationImages = [
			vfsStream::url('folio/storage/images/foo/foo.svg'),
        ];

        $instance = new CompressImagesProcessor;
		
        foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileDoesNotExist($destinationImagePath);
        }
	    
	    $result = $instance->process(new UsedImagesProvider);

	    foreach ($destinationImages as $destinationImagePath) {
            $this->assertFileExists($destinationImagePath);
        }

		$this->assertCount(1, $this->getAllFiles(vfsStream::url('folio/storage/images')));
		$this->assertCount(1, $this->getAllFiles(vfsStream::url('folio/storage/images/foo')));
	}

	/** @test */
	public function it_compresses_only_used_images()
	{
		$this->createPublishedProjectWithImage('foo');
		$this->createPublishedProjectWithoutImage('bar');
		
		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$this->assertFilesDoNotExist($targetFiles);
		
		$instance = new CompressImagesProcessor;
		$result = $instance->process(new UsedImagesProvider);

		$this->assertFilesCount('foo', $targetFiles);
		$this->assertFilesExist($targetFiles);
		
		$this->assertFilesCount('bar', []);
	}

	/** @test */
	public function it_compresses_images_once_if_source_file_has_not_changed()
	{
		$this->createPublishedProjectWithImage('foo');

		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$this->assertFilesDoNotExist($targetFiles);
				
		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);
		
		$this->alterFilesToVerifyChanges($targetFiles);
		
		$instance->process(new UsedImagesProvider);

		$this->assertFilesAreNotChanged('foo', $targetFiles);
	}

	/** @test */
	public function it_compresses_images_twice_if_source_file_has_changed()
	{
		$this->createPublishedProjectWithImage('foo');

		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$this->assertFilesDoNotExist($targetFiles);
		
		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);
		
		$this->alterFilesToVerifyChanges($targetFiles);
		$this->changeProjectImage('foo');

		$instance->process(new UsedImagesProvider);

		$this->assertFilesAreChanged('foo', $targetFiles);
	}

	/** @test */
	public function it_compresses_images_twice_if_output_file_has_been_deleted()
	{
		$this->createPublishedProjectWithImage('foo');

		$targetFiles = [
			vfsStream::url('folio/storage/images/foo/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo/foo-compressed.webp')
		];
		
		$this->assertFilesDoNotExist($targetFiles);
				
		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);
		
		unlink($targetFiles[1]);
		$this->assertFileDoesNotExist($targetFiles[1]);
		
		$instance->process(new UsedImagesProvider);

		$this->assertFilesExist($targetFiles);
	}

	//

	/** @test */
	public function it_publishes_jpg_image()
	{
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/images/foo.jpg'));

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists(vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	}

	/** @test */
	public function it_publishes_jpg_image_only_if_used_by_project()
	{
	    $this->createImageFolderForSlug('foo');
		$this->createStorageImageFolderForSlug('foo');
		$this->createDummyImage($sourceFoo = vfsStream::url('folio/contents/images/foo/foo.jpg'));
	    $this->createDummyImage($sourceBar = vfsStream::url('folio/contents/images/foo/bar.jpg'));
		$this->createDummyImage($sourceBaz = vfsStream::url('folio/contents/images/foo/baz.png'));
		$this->createPublishedProjectFile(['markdown' => '![foo] ![baz]']);

		$this->assertFileDoesNotExist(
			$destinationFoo = vfsStream::url('folio/storage/images/foo/foo-compressed.jpg')
		);
		$this->assertFileDoesNotExist(
			$destinationBar = vfsStream::url('folio/storage/images/foo/bar-compressed.jpg')
		);
		$this->assertFileDoesNotExist(
			$destinationBaz = vfsStream::url('folio/storage/images/foo/baz-compressed.jpg')
		);

		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);

		$this->assertFileExists($destinationFoo);
		$this->assertFileDoesNotExist($destinationBar);
		$this->assertFileExists($destinationBaz);
	}

	/** @test */
	public function it_will_copy_a_whitelisted_folder()
	{
	    $this->createSourceImageFolderForSlug('_icons');
		$this->createStorageImageFolderForSlug('_icons');
		$this->createDummyImage(vfsStream::url('folio/contents/images/_icons/icon.jpg'));
		
		$this->createSourceImageFolderForSlug('foo');
		$this->createStorageImageFolderForSlug('foo');
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo/bar.jpg'));
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/baz.png'));

		$this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/_icons/icon.jpg'));
		$this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo/foo.jpg'));
		
		$usedImagesFinder = app(UsedImagesFinder::class);
		$usedImagesFinder
			->whitelist()
			->set(['_icons']);
			
		$instance = new CompressImagesProcessor;
		$instance->process(new UsedImagesProvider);
		
		$this->assertFileExists(vfsStream::url('folio/storage/images/_icons/icon-compressed.jpg'));
		$this->assertFileDoesNotExist(vfsStream::url('folio/public/images/foo/foo-compressed.jpg'));
	}

	/** @test */
	public function it_publishes_multiple_jpg_images()
	{
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/bar.jpg'));
	    
	    $this->assertFileDoesNotExist($foo = vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	    $this->assertFileDoesNotExist($bar = vfsStream::url('folio/storage/images/bar-compressed.jpg'));

		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($foo);
	    $this->assertFileExists($bar);
	}

	/** @test */
	public function it_publishes_multiple_jpg_images_nested()
	{
	    $this->createSourceImageFolderForSlug('nested');
		$this->createStorageImageFolderForSlug('nested');

	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/nested/bar.jpg'));
	    
	    $this->assertFileDoesNotExist($foo = vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	    $this->assertFileDoesNotExist($bar = vfsStream::url('folio/storage/images/nested/bar-compressed.jpg'));

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($foo);
	    $this->assertFileExists($bar);
	}

	/** @test */
	public function it_publishes_jpg_image_with_default_compression()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
		$storageImage = vfsStream::url('folio/storage/images/foo-compressed.jpg');
	    
		$this->createDummyImage($sourceImage, ['quality' => 100]);
	    
	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertLessThan(
	    	filesize($sourceImage),
	    	filesize($storageImage)
	    );
	}

	/** @test */
	public function it_publishes_jpg_image_with_specified_compression()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
		$storageImage = vfsStream::url('folio/storage/images/foo-compressed.jpg');

	    $this->createDummyImage($sourceImage, ['quality' => 80]);
	    
	    $instance = new CompressImagesProcessor(['quality' => 80]);
		$instance->process(new AllImagesProvider);

	    $this->assertEquals(
	    	file_get_contents($sourceImage), 
	    	file_get_contents($storageImage)
	    );

	    $this->assertEquals(
	    	filesize($sourceImage),
	    	filesize($storageImage)
	    );
	}

	/** @test */
	public function it_publishes_jpg_image_in_webp_format()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.jpg');
		$storageImage = vfsStream::url('folio/storage/images/foo-compressed.webp');

	    $this->createDummyImage($sourceImage);
	    
	    $this->assertFileDoesNotExist($storageImage);

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($storageImage);
	}

	/** @test */
	public function it_publishes_png_image_in_jpg_format_if_does_not_have_alpha()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.png');
		$notExpectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.png');
		$expectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.jpg');

	    $this->createDummyImage($sourceImage);

	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	    $this->assertFileDoesNotExist($expectedStorageImage);
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	    $this->assertFileExists($expectedStorageImage);
	}

	/** @test */
	public function it_publishes_png_image_in_png_format_if_has_alpha()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.png');
		$notExpectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.jpg');
		$expectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.png');

	    $this->unlinkDefaultImage();
	    $this->createDummyImageWithAlpha($sourceImage);

	    $this->assertFileDoesNotExist($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	}

	/** @test */
	public function it_publishes_png_image_in_webp_format()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.png');
		$notExpectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.png');
		$expectedStorageImage = vfsStream::url('folio/storage/images/foo-compressed.webp');

	    $this->createDummyImage($sourceImage);

	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	    $this->assertFileDoesNotExist($expectedStorageImage);
	    
	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileDoesNotExist($notExpectedStorageImage);
	    $this->assertFileExists($expectedStorageImage);
	}

	/** @test */
	public function it_publishes_webp_image_in_jpg_and_webp_format()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.webp');
		$expectedStorageImages = [
			vfsStream::url('folio/storage/images/foo-compressed.jpg'),
			vfsStream::url('folio/storage/images/foo-compressed.webp')
		];

	    $this->unlinkDefaultImage();
	    $this->createDummyImage($sourceImage);
	    
	    $this->assertFileDoesNotExist($expectedStorageImages[0]);
	    $this->assertFileDoesNotExist($expectedStorageImages[1]);
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($expectedStorageImages[0]);
	    $this->assertFileExists($expectedStorageImages[1]);
	}

	/** @test */
	public function it_publishes_svg_image_as_it_is()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.svg');
		$notExpectedStorageImages = [
			vfsStream::url('folio/storage/images/foo.jpg'),
			vfsStream::url('folio/storage/images/foo.webp'),
		];
		$expectedStorageImage = vfsStream::url('folio/storage/images/foo.svg');

	    $this->unlinkDefaultImage();
	    file_put_contents($sourceImage, '');

	    $this->assertFileDoesNotExist($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[0]);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[1]);
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[0]);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[1]);
	}

	/** @test */
	public function it_publishes_gif_image_as_it_is()
	{
		$sourceImage = vfsStream::url('folio/contents/images/foo.gif');
		$notExpectedStorageImages = [
			vfsStream::url('folio/storage/images/foo.jpg'),
			vfsStream::url('folio/storage/images/foo.webp'),
		];
		$expectedStorageImage = vfsStream::url('folio/storage/images/foo.gif');

	    $this->unlinkDefaultImage();
	    file_put_contents($sourceImage, '');

	    $this->assertFileDoesNotExist($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[0]);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[1]);
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($expectedStorageImage);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[0]);
	    $this->assertFileDoesNotExist($notExpectedStorageImages[1]);
	}

	/** @test */
	public function it_skips_non_image_files()
	{
	    $this->unlinkDefaultImage();
	    file_put_contents(vfsStream::url('folio/contents/images/foo.json'), '');

	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.jpg'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.webp'));
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.json'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.jpg'));
	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo.webp'));
	}

	/** @test */
	public function it_creates_a_manifest_file()
	{
		$this->assertFileDoesNotExist(ManifestFactory::compressedImages()->getFilePath());
	    
		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
	    $this->assertFileExists(ManifestFactory::compressedImages()->getFilePath());
	}

	/** @test */
	public function it_creates_manifest_for_jpg_image()
	{
		$original = vfsStream::url('folio/contents/images/foo.jpg');
	    
	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
	    $manifestArray = $this->getManifestInstance()->toArray();

	    $this->assertManifestKey($original, $manifestArray);
	    
	    $this->assertManifestInput(
	    	['folio/contents/images/foo.jpg', 'jpg'], 
	    	$manifestArray[$original]
	    );
	    
	    $this->assertManifestOutput('compressed', [
	    	['folio/storage/images/foo-compressed.jpg', 'jpg'],
	    	['folio/storage/images/foo-compressed.webp', 'webp']
	    ], $manifestArray[$original]);
	}

	/** @test */
	public function it_creates_manifest_for_png_image()
	{
		$this->unlinkDefaultImage();
		$this->createDummyImage($original = vfsStream::url('folio/contents/images/foo.png'));

		$instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
	    $manifestArray = $this->getManifestInstance()->toArray();

	    $this->assertManifestKey($original, $manifestArray);

	    $this->assertManifestInput(
	    	[ 'folio/contents/images/foo.png', 'png' ], 
	    	$manifestArray[$original]
	    );

	    $this->assertManifestOutput('compressed', [
	    	[ 'folio/storage/images/foo-compressed.jpg', 'jpg' ],
	    	[ 'folio/storage/images/foo-compressed.webp', 'webp' ]
	    ], $manifestArray[$original]);
	}

	/** @test */
	public function it_creates_manifest_for_gif_image()
	{
		$this->createDummyImage($original = vfsStream::url('folio/contents/images/foo.gif'));

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
	    $manifest = $this->getManifestInstance()->toArray();

	    $this->assertManifestKey($original, $manifest);

	    $this->assertManifestInput(
	    	[ 'folio/contents/images/foo.gif', 'gif' ], 
	    	$manifest[$original]
	    );

	    $this->assertManifestOutput('copy', [
	    	[ 'folio/storage/images/foo.gif', 'gif' ],
	    ], $manifest[$original]);
	}

	/** @test */
	public function it_creates_manifest_for_svg_image()
	{
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo.svg'));

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
		$manifest = $this->getManifestInstance()->toArray();

	    $this->assertArrayHasKey(
			vfsStream::url('folio/contents/images/foo.svg'), 
			$manifest
		);
	}

	/** @test */
	public function it_creates_manifest_for_non_image_files()
	{
		$this->createDummyImage(
			$original = vfsStream::url('folio/contents/images/foo.json')
		);

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);
	    
	    $manifestArray = $this->getManifestInstance()->toArray();

	    $this->assertManifestKey($original, $manifestArray);
	    
	    $this->assertManifestInput(
	    	['folio/contents/images/foo.json', 'json'], 
	    	$manifestArray[$original]
	    );

	    $this->assertArrayHasKey('output', $manifestArray[$original]);
	    $this->assertEquals([], $manifestArray[$original]['output']);
	}

	/** @test */
	public function it_creates_manifest_for_multiple_jpg_images()
	{
	    $this->createDummyImage($original1 = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($original2 = vfsStream::url('folio/contents/images/bar.jpg'));
	    
	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $manifestArray = $this->getManifestInstance()->toArray();
	    $this->assertArrayHasKey($original1, $manifestArray);
	    $this->assertArrayHasKey($original2, $manifestArray);
	}

	/** @test */
	public function it_creates_manifest_for_multiple_jpg_images_nested()
	{
		$this->createImageFolderForSlug('nested');
	    $this->createDummyImage($fooSource = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/nested/bar.jpg'));

	    $instance = new CompressImagesProcessor;
		$instance->process(new AllImagesProvider);

	    $manifestArray = $this->getManifestInstance()->toArray();

	    $this->assertArrayHasKey($fooSource, $manifestArray);
	    $this->assertArrayHasKey($barSource, $manifestArray);
	}

	/** @test */
	public function it_accepts_options_object()
	{
	    $this->createDummyImage($fooSource = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/bar.jpg'));

		$options = (new Options(['quality' => 10]))
	    	->override($fooSource, ['quality' => 100]);

	    $instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertGreaterThan(
	    	filesize($fooSource),
	    	filesize(vfsStream::url('folio/storage/images/foo-compressed.jpg'))
	    );

	    $this->assertLessThan(
	    	filesize($barSource),
	    	filesize(vfsStream::url('folio/storage/images/bar-compressed.jpg'))
	    );
	}

	/** @test */
	public function it_allows_to_skip_a_file()
	{
	    $this->createDummyImage($original1 = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($original2 = vfsStream::url('folio/contents/images/bar.jpg'));

	    $options = new Options(['quality' => 10]);
    	
    	$options
    		->output()
    		->files()
    		->file($original1)
    		->skip();

		$instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.jpg'));
	}

	/** @test */
	public function it_allows_to_publish_a_raw_file()
	{
	    $this->createDummyImage($fooSource = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/bar.jpg'));

		$fooStorage = vfsStream::url('folio/storage/images/foo.jpg');

	    $options = new Options(['quality' => 10]);
    	
    	$options
    		->output()
    		->files()
    		->file($fooSource)
    		->raw();

		$instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists($fooStorage);
	    $this->assertEquals(
	    	file_get_contents($fooSource), 
	    	file_get_contents($fooStorage)
	    );
	    $this->assertFileExists($barSource);
	}

	/** @test */
	public function it_allows_to_publish_a_file_with_different_compression()
	{
	    $this->createDummyImage($fooSource = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/bar.jpg'));

	    $options = new Options(['quality' => 10]);
    	
    	$options
    		->output()
    		->files()
    		->file($fooSource)
    		->compress(['png']);

		$instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists(vfsStream::url('folio/storage/images/foo-compressed.png'));
	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.jpg'));
	}

	/** @test */
	public function it_allows_to_publish_image_with_alpha_with_different_compression()
	{
	    $this->createDummyImageWithAlpha($fooSource = vfsStream::url('folio/contents/images/foo.png'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/bar.jpg'));

	    $options = new Options(['quality' => 10]);
    	
    	$options
    		->output()
    		->files()
    		->file($fooSource)
    		->compressAlpha(['jpg']);

		$instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists(vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.jpg'));
	}

	/** @test */
	public function it_allows_to_publish_image_with_alpha_with_generic_compression()
	{
	    $this->createDummyImageWithAlpha($sourceFoo = vfsStream::url('folio/contents/images/foo.png'));
	    $this->createDummyImage($sourceBar = vfsStream::url('folio/contents/images/bar.jpg'));

	    $options = new Options(['quality' => 10]);
    	
    	$options
    		->output()
    		->files()
    		->file($sourceFoo)
    		->compress(['jpg']);

		$instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $this->assertFileExists(vfsStream::url('folio/storage/images/foo-compressed.jpg'));
	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.jpg'));
	}

	/** @test */
	public function it_loads_options_from_json_file()
	{
	    $this->createDummyImage($fooSource = vfsStream::url('folio/contents/images/foo.jpg'));
	    $this->createDummyImage($barSource = vfsStream::url('folio/contents/images/bar.jpg'));

	    file_put_contents(
	    	vfsStream::url('folio/images.config.json'), 
	    	json_encode([
		    	$fooSource => [
		    		'quality' => 100,
		    		'output' => [
		    			'method' => 'compressed',
		    			'exports' => ['jpg']
		    		]
		    	]
		    ])
	    );

	    $options = new Options(['quality' => 10]);
    	
	    $instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

		$this->assertFileExists(vfsStream::url('folio/storage/images/foo-compressed.jpg'));
		$this->assertFileDoesNotExist(vfsStream::url('folio/storage/images/foo-compressed.webp'));
		
	    $this->assertGreaterThan(
	    	filesize($fooSource),
	    	filesize(vfsStream::url('folio/storage/images/foo-compressed.jpg'))
	    );

	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.jpg'));
	    $this->assertFileExists(vfsStream::url('folio/storage/images/bar-compressed.webp'));
	    $this->assertLessThan(
	    	filesize($barSource),
	    	filesize(vfsStream::url('folio/storage/images/bar-compressed.jpg'))
	    );
	}

	/** @test */
	public function it_logs_warnings()
	{
		$this->unlinkDefaultImage();
		$this->createDummyImage(
			$source = vfsStream::url('folio/contents/images/foo.jpg')
		);

	    $output = (new OutputFactory)
	    	->export('jpg')->compress([new TestImage]);

		$options = new Options(['quality' => 10], $output);

	    $instance = new CompressImagesProcessor($options);
		$instance->process(new AllImagesProvider);

	    $outputs = $this->getManifestInstance()->getEntry($source)->getOutputs();
	    
	    $this->assertArrayHasKey('errors', $outputs[0]);
	    $this->assertIsArray($outputs[0]['errors']);
	    $this->assertCount(2, $outputs[0]['errors']);
	    $this->assertEquals('warning triggered by test image', $outputs[0]['errors'][0]);
	    $this->assertEquals('notice triggered by test image', $outputs[0]['errors'][1]);
	}

	//

	protected function getManifestInstance()
	{
		return ManifestFactory::compressedImages()->loadIfNotLoaded();
	}

	private function createPublishedProjectWithoutImage($slug)
	{
		return $this->setupPublishedProjectWorld($slug, [ 'markdown' => '' ]);
	}

	private function createPublishedProjectWithImage($slug)
	{
		return $this->setupPublishedProjectWorld($slug, [ 'markdown' => "![{$slug}]" ]);
	}

	private function setupPublishedProjectWorld($slug, $attributes)
	{
		@$this->unlinkDefaultImage();

		$attributes['slug'] = $slug;

		$this->createImageFolderForSlug($slug);
		
		$this->createDummyImage(
			$this->getSourceImage($slug)
		);

		$this->createPublishedProjectFile($attributes);
		
		mkdir($this->getTargetFolder($slug));
	}

	private function alterFilesToVerifyChanges(array $files)
	{
		foreach ($files as $file) {
			$this->assertNotEquals('', file_get_contents($file));
			file_put_contents($file, '');
		}
	}

	private function assertFilesExist(array $files)
	{
		foreach ($files as $file) {
			$this->assertFileExists($file);
		}
	}

	private function assertFilesDoNotExist(array $files)
	{
		foreach ($files as $file) {
			$this->assertFileDoesNotExist($file);
		}
	}

	private function assertFilesAreNotChanged($slug, array $files)
	{
		$this->assertFilesCount($slug, $files);

		foreach ($files as $file) {
			$this->assertFileExists($file);
			$this->assertEquals('', file_get_contents($file));
		}
	}

	private function assertFilesAreChanged($slug, array $files)
	{
		$this->assertCount(
			count($files), 
			$this->getAllFiles(
				$this->getTargetFolder($slug)
			)
		);

		foreach ($files as $file) {
			$this->assertFileExists($file);
			$this->assertNotEquals('', file_get_contents($file));
		}
	}

	private function assertFilesCount($slug, array $files)
	{
		$this->assertCount(
			count($files), 
			$this->getAllFiles(
				$this->getTargetFolder($slug)
			)
		);
	}

	private function changeProjectImage($slug)
	{
		$this->createDummyImage(
			$this->getSourceImage($slug), 
			['width' => 200, 'height' => 200]
		);
	}

	private function getSourceImage($slug)
	{
		return vfsStream::url("folio/contents/images/{$slug}/{$slug}.jpg");
	}

	private function getTargetFolder($slug)
	{
		return vfsStream::url("folio/storage/images/{$slug}");
	}

	private function getAllFiles($path)
	{
		return array_diff(
			scandir($path), 
			['.', '..']
		);
	}
}