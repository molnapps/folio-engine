<?php

namespace Tests\Unit\Themese\Publishers\Images\Processors;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use App\Themes\Publishers\Images\Processors\Processor;
use App\Themes\Publishers\Images\Processors\ProcessorProvider;
use App\Themes\Publishers\Images\Processors\UsedImagesProvider;
use App\Themes\Publishers\Images\Processors\CreateImageFoldersProcessor;

class CreateImageFoldersProcessorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CreateImageFoldersProcessor);
	}

	/** @test */
	public function it_is_instance_of_processor()
	{
	    $this->assertInstanceOf(Processor::class, new CreateImageFoldersProcessor);
	}

	/** @test */
	public function it_is_instance_of_processor_provider()
	{
	    $this->assertInstanceOf(ProcessorProvider::class, new CreateImageFoldersProcessor);
	}

	/** @test */
	public function it_creates_folder_for_project()
	{
		$this->createImageFolderForSlug('foo');
        $this->createDummyImage(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );
        $this->createPublishedProjectFile(['markdown' => '![foo]']);

		$this->assertFileDoesNotExist(
            $targetFolder = vfsStream::url('folio/storage/images/foo')
        );

        $instance = new CreateImageFoldersProcessor;
		$result = $instance->process($provider = new UsedImagesProvider);

        $this->assertInstanceOf(ProcessorProvider::class, $result);
		$this->assertSame($provider, $result);
	}

	/** @test */
	public function it_returns_images()
	{
		$this->createImageFolderForSlug('foo');
        $this->createDummyImage(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );
        $this->createPublishedProjectFile(['markdown' => '![foo]']);

		$instance = new CreateImageFoldersProcessor;
		$result = $instance->process($provider = new UsedImagesProvider);

		$this->assertIsArray($result->getImages());
		$this->assertEquals($provider->getImages(), $instance->getImages());
	}

	/** @test */
	public function it_does_not_create_folders_if_project_has_no_image()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );
        $this->createPublishedProjectFile([]);

		$this->assertFileDoesNotExist(
            $targetFolder = vfsStream::url('folio/storage/images/foo')
        );

        $instance = new CreateImageFoldersProcessor;
		$instance->process(new UsedImagesProvider);

        $this->assertFileDoesNotExist($targetFolder);
	}

    /** @test */
	public function it_does_not_create_folders_if_project_has_image_but_is_not_published()
	{
		$this->createImageFolderForSlug('foo');
        $this->createDummyImage(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );
        $this->createProjectFile([], $markdown = '![foo]');

		$this->assertFileDoesNotExist(
            $targetFolder = vfsStream::url('folio/storage/images/foo')
        );

        $instance = new CreateImageFoldersProcessor;
		$instance->process(new UsedImagesProvider);

        $this->assertFileDoesNotExist($targetFolder);
	}
}