<?php

namespace App\Themes;

use App\App;
use App\Testing\InteractsWithFileSystem;
use App\Generators\FolioManifest;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\FileManifestData;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ThemeAssetsTest extends TestCase
{
	use InteractsWithFileSystem;

	/** @before */
	public function setUpThemes()
	{
		App::bootstrapThemes([
			'foobar' => \Folio\Themes\Test\ThemeProvider::class,
		]);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(
	    	new ThemeAssets('foobar')
	    );
	}

	/** @test */
	public function it_publishes_assets()
	{
		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$publisher
			->expects($this->once())
			->method('publish')
			->with(
				$this->isInstanceOf(Manifest::class),
				$this->equalTo('vfs://folio/theme/assets'),
				$this->equalTo('vfs://folio/public/_assets/foobar')
			);

		$instance = new ThemeAssets('foobar', $publisher);

	    $instance->publish();
	}

	/** @test */
	public function it_creates_destination_folder_if_does_not_exist()
	{
		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);

		$instance = new ThemeAssets('foobar', $publisher);

		$assetsFolder = vfsStream::url('folio/public/_assets');
		$this->assertFileDoesNotExist($assetsFolder);
		
	    $instance->publish();

		$this->assertFileExists($assetsFolder);
	}

	/** @test */
	public function it_adds_destination_folder_to_manifest()
	{
		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);

		$instance = new ThemeAssets('foobar', $publisher);

		$assetsFolder = vfsStream::url('folio/public/_assets');
		$this->assertFalse(app(FolioManifest::class)->hasFolder($assetsFolder));

	    $instance->publish();

		$this->assertTrue(app(FolioManifest::class)->hasFolder($assetsFolder));
	}

	/** @test */
	public function it_deletes_previously_published_assets_folder_if_has_manifest()
	{
		mkdir(vfsStream::url('folio/public/_assets'));
		mkdir($dest = vfsStream::url('folio/public/_assets/foobar'));
		file_put_contents(vfsStream::url('folio/public/_assets/foobar/app.css'), '');

		$manifest = ManifestFactory::assets();

		$manifest->addEntry(
			vfsStream::url('folio/theme/assets'), 
			[
				'input' => FileManifestData::fromFilePath(vfsStream::url('folio/theme/assets'))->get(),
				'output' => [
					FileManifestData::fromFilePath(vfsStream::url('folio/public/_assets/foobar'))->get(),
				]
			]
		);

		$manifest->addEntry(
			vfsStream::url('folio/theme/assets/app.css'), 
			[
				'input' => FileManifestData::fromFilePath(vfsStream::url('folio/theme/assets/app.css'))->get(),
				'output' => [
					FileManifestData::fromFilePath(vfsStream::url('folio/public/_assets/foobar/app.css'))->get()
				]
			]
		);

		$manifest->publish();

		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$instance = new ThemeAssets('foobar', $publisher);
	    $instance->publish();

		$this->assertFileDoesNotExist($dest);
		$this->assertFileDoesNotExist($dest . '_backup');
	}

	/** @test */
	public function it_deletes_previously_published_assets_file()
	{
		$dest = vfsStream::url('folio/public/_assets/foobar');

		$this->assertFileDoesNotExist($dest);

		mkdir(vfsStream::url('folio/public/_assets'));
		file_put_contents($dest, '');

		$this->assertFileExists($dest);

		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$instance = new ThemeAssets('foobar', $publisher);
	    $instance->publish();

		$this->assertFileDoesNotExist($dest);
		$this->assertFileDoesNotExist($dest . '_backup');
	}

	/** @test */
	public function it_backups_previously_published_assets_folder()
	{
		$dest = vfsStream::url('folio/public/_assets/foobar');

		$this->assertFileDoesNotExist($dest);

		mkdir(vfsStream::url('folio/public/_assets'));
		mkdir($dest);

		$this->assertFileExists($dest);

		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		$publisher
			->method('publish')
			->will($this->throwException(new \Exception));
		
		$instance = new ThemeAssets('foobar', $publisher);
	    $instance->publish();

	    $this->assertFileExists($dest);
	    $this->assertFileDoesNotExist($dest . '_backup');
	}

	/** @test */
	public function it_backups_previously_published_assets_file()
	{
		$dest = vfsStream::url('folio/public/_assets/foobar');

		$this->assertFileDoesNotExist($dest);

		mkdir(vfsStream::url('folio/public/_assets'));
		file_put_contents($dest, '');

		$this->assertFileExists($dest);

		$publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		$publisher
			->method('publish')
			->will($this->throwException(new \Exception));
		
		$instance = new ThemeAssets('foobar', $publisher);
	    $instance->publish();

	    $this->assertFileExists($dest);
	    $this->assertFileDoesNotExist($dest . '_backup');
	}

	/** @test */
	public function it_returns_assets_url()
	{
	    $instance = new ThemeAssets('foobar');

	    $this->assertEquals(
	    	'https://folio-engine.test/_assets/foobar/test.css', 
	    	$instance->getUrl('test.css')
	    );
	}

	/** @test */
	public function it_returns_assets_url_nested()
	{
	    $instance = new ThemeAssets('foobar');

	    $this->assertEquals(
	    	'https://folio-engine.test/_assets/foobar/fonts/neue-machina/NeueMachina-Regular.woff',
	    	$instance->getUrl('fonts/neue-machina/NeueMachina-Regular.woff')
	    );
	}

	/** @test */
	public function it_provides_a_way_to_provide_a_publisher_after_instantiation()
	{
	    $publisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$publisher
			->expects($this->once())
			->method('publish')
			->with(
				$this->isInstanceOf(Manifest::class),
				$this->equalTo('vfs://folio/theme/assets'),
				$this->equalTo('vfs://folio/public/_assets/foobar')
			);

		$instance = new ThemeAssets('foobar');

	    $instance->publish($publisher);
	}

	/** @test */
	public function it_prefers_publisher_defined_after_instantiation()
	{
	    $fooPublisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
	    $barPublisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$fooPublisher
			->expects($this->once())
			->method('publish')
			->with(
				$this->isInstanceOf(Manifest::class),
				$this->equalTo('vfs://folio/theme/assets'),
				$this->equalTo('vfs://folio/public/_assets/foobar')
			);

		$barPublisher
			->expects($this->never())
			->method('publish');

		$instance = new ThemeAssets('foobar', $barPublisher);

	    $instance->publish($fooPublisher);
	}

	/** @test */
	public function it_will_not_override_publisher_defined_during_instantiation()
	{
	    $fooPublisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
	    $barPublisher = $this->createMock(\App\Themes\Publishers\Publisher::class);
		
		$fooPublisher
			->expects($this->once())
			->method('publish')
			->with(
				$this->isInstanceOf(Manifest::class),
				$this->equalTo('vfs://folio/theme/assets'),
				$this->equalTo('vfs://folio/public/_assets/foobar')
			);

		$barPublisher
			->expects($this->once())
			->method('publish')
			->with(
				$this->isInstanceOf(Manifest::class),
				$this->equalTo('vfs://folio/theme/assets'),
				$this->equalTo('vfs://folio/public/_assets/foobar')
			);

		$instance = new ThemeAssets('foobar', $barPublisher);

	    $instance->publish($fooPublisher);
	    $instance->publish();
	}

	/** @test */
	public function it_throws_if_no_publisher_is_specified()
	{
	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage('Please provide a publisher');

		$instance = new ThemeAssets('foobar');

	    $instance->publish();
	}
}