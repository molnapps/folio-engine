<?php

namespace Tests\Unit;

use App\Portfolio;
use App\PortfolioAutoDiscovery;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PortfolioAutoDiscoveryTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [
					'bar.md' => '---
layout: page
title: Bar
published_at: 2014-06-04
---
Barbaz',
					'baz.md' => '---
layout: page
title: Baz
published_at: 2012-06-04
---
Bazfoo'
				],
				'projects' => [
					'foo.md' => '---
layout: project
title: Foo
published_at: 2016-02-01
---
Foobar',
					'yolo.md' => '---
layout: project
title: Yolo
published_at: 2019-02-01
---
You only live once'
				],
				'preferences' => [
					'defaults.json' => '[]'
				]
			],
			'layouts' => [],
			'public' => []
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull(
			new PortfolioAutoDiscovery(
				new Portfolio($this->basePath)
			)
		);
	}

	/** @test */
	public function it_throws_if_environment_driver_is_not_file()
	{
	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage('Driver must be file.');

	    $this->overrideEnvironmentVariable('REPOSITORY_DRIVER', 'test');
	    
	    new PortfolioAutoDiscovery(
	    	new Portfolio($this->basePath)
	    );
	}

	/** @test */
	public function it_auto_discovers_projects()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->projects());

	    $instance->discover();

	    $this->assertCount(2, $portfolio->projects());
	    
	    $slugs = array_map('strval', $portfolio->projects()->flatten('slug'));

	    $this->assertContains('foo', $slugs);
	    $this->assertContains('yolo', $slugs);
	}

	/** @test */
	public function it_auto_discovers_pages()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(2, $portfolio->pages());

	    $slugs = array_map('strval', $portfolio->pages()->flatten('slug'));

	    $this->assertContains('bar', $slugs);
	    $this->assertContains('baz', $slugs);
	}

	/** @test */
	public function it_auto_discovers_nested_pages()
	{
		mkdir(vfsStream::url('folio/contents/pages/nested'));
		mkdir(vfsStream::url('folio/contents/pages/nested/sub'));

		copy(
			vfsStream::url('folio/contents/pages/bar.md'), 
			vfsStream::url('folio/contents/pages/nested/bar.md')
		);

		copy(
			vfsStream::url('folio/contents/pages/bar.md'), 
			vfsStream::url('folio/contents/pages/nested/sub/subar.md')
		);
		unlink(vfsStream::url('folio/contents/pages/bar.md'));

	    $portfolio = new Portfolio($this->basePath);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(3, $portfolio->pages());

	    $slugs = array_map('strval', $portfolio->pages()->flatten('slug'));
	    
	    $this->assertContains('nested-bar', $slugs);
	    $this->assertContains('nested-sub-subar', $slugs);
	    $this->assertContains('baz', $slugs);
	}

	/** @test */
	public function it_does_not_automatically_adds_selected_works()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(2, $portfolio->pages());
	    $this->assertNotContains('portfolio', $portfolio->pages()->flatten('slug'));
	}

	/** @test */
	public function it_adds_selected_works_if_settings_is_false()
	{
		$portfolio = new Portfolio($this->basePath);

		$portfolio->settings()->load([
			'portfolio' => '0'
		]);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(2, $portfolio->pages());
	    $this->assertNotContains('portfolio', $portfolio->pages()->flatten('slug'));
	}

	/** @test */
	public function it_adds_selected_works_if_settings_is_true()
	{
		$portfolio = new Portfolio($this->basePath);

		$portfolio->settings()->load([
			'portfolio' => '1'
		]);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(3, $portfolio->pages());
	    $this->assertContains('portfolio', $portfolio->pages()->flatten('slug'));
	}

	/** @test */
	public function it_automatically_adds_contact_if_email_is_provided()
	{
		$portfolio = new Portfolio($this->basePath);

		$portfolio->settings()->load([
			'email' => 'info@example.com'
		]);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $this->assertCount(0, $portfolio->pages());

	    $instance->discover();

	    $this->assertCount(3, $portfolio->pages());
	    $this->assertContains('contact', $portfolio->pages()->flatten('slug'));
	}

	/** @test */
	public function it_sorts_discovered_projects_by_published_at_desc()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $instance->discover();

	    $this->assertEquals([
	    	'yolo',
	    	'foo',
	    ], $portfolio->projects()->flatten('slug'));
	}

	/** @test */
	public function it_sorts_discovered_pages_by_published_at()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $portfolio->settings()->load([
	    	'portfolio' => '1',
			'email' => 'info@example.com'
		]);

	    $instance = new PortfolioAutoDiscovery($portfolio);

	    $instance->discover();

	    $this->assertEquals([
	    	'portfolio',
	    	'baz',
	    	'bar',
	    	'contact'
	    ], $portfolio->pages()->flatten('slug'));
	}
}