<?php

namespace Tests\Unit\Repositories;

use App\Domain\Post;
use App\Repositories\PostCollection;
use App\Repositories\Test\PostsRepository;
use Tests\TestCase;

class PostCollectionTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PostCollection(new PostsRepository));
	}

	/** @test */
	public function it_implements_countable_interface()
	{
	    $this->assertInstanceOf(
	    	\Countable::class, 
	    	new PostCollection(new PostsRepository)
	    );
	}

	/** @test */
	public function it_returns_zero_count_if_no_project_is_defined()
	{
	    $this->assertCount(0, new PostCollection(new PostsRepository));
	}

	/** @test */
	public function it_adds_post_with_slug()
	{
		$instance = new PostCollection(new PostsRepository);

	    $this->assertCount(0, $instance);

	    $instance->addWithSlug('foo');

	    $this->assertCount(1, $instance);
	}

	/** @test */
	public function it_adds_post_with_array()
	{
		$instance = new PostCollection(new PostsRepository);

	    $this->assertCount(0, $instance);

	    $instance->addWithArray(['slug' => 'foo']);

	    $this->assertCount(1, $instance);
	}

	/** @test */
	public function it_adds_post_with_domain_object()
	{
		$instance = new PostCollection(new PostsRepository);

	    $this->assertCount(0, $instance);

	    $instance->add(new Post(['slug' => 'foo']));

	    $this->assertCount(1, $instance);
	}

	/** @test */
	public function it_finds_post_from_repository_by_slug()
	{
		$instance = new PostCollection(
			(new PostsRepository)->exclude('foo')
		);

		$this->assertInstanceOf(Post::class, $instance->find('foo'));
		$this->assertFalse($instance->find('foo')->exists());

	    $this->assertInstanceOf(Post::class, $instance->find('bar'));
	    $this->assertTrue($instance->find('bar')->exists());
	}

	/** @test */
	public function it_finds_post_added_at_runtime_by_slug()
	{
		$instance = new PostCollection(
			(new PostsRepository)->exclude('foo')
		);

		$instance->add(new Post(['slug' => 'foo', 'exists' => true]));

		$this->assertInstanceOf(Post::class, $instance->find('foo'));
		$this->assertTrue($instance->find('foo')->exists());

	    $this->assertInstanceOf(Post::class, $instance->find('bar'));
	    $this->assertTrue($instance->find('bar')->exists());
	}

	/** @test */
	public function it_returns_array()
	{
	    $instance = new PostCollection(new PostsRepository);

	    $this->assertCount(0, $instance->toArray());

	    $instance->addWithSlug('foo');

	    $this->assertCount(1, $instance->toArray());
	}

	/** @test */
	public function it_returns_immutable_instance_for_visible_projects()
	{
	    $instance = new PostCollection(new PostsRepository);

	    $instance->addWithArray(['slug' => 'foo']);
	    $instance->addWithArray(['slug' => 'bar', 'published_at' => 'yesterday']);
	    $instance->addWithArray(['slug' => 'baz', 'published_at' => 'tomorrow']);

	    $this->assertCount(3, $instance->toArray());

	    $visibleOnly = $instance->visibleOnly();
	    
	    $this->assertInstanceOf(PostCollection::class, $visibleOnly);
	    $this->assertNotSame($instance, $visibleOnly);
	    
	    $this->assertCount(1, $visibleOnly->toArray());
	    
	    $this->assertArrayHasKey(0, $visibleOnly->toArray());
	    $this->assertInstanceOf(Post::class, $visibleOnly->toArray()[0]);
	    $this->assertEquals('bar', $visibleOnly->toArray()[0]->slug);
	}

	/** @test */
	public function it_sorts_projects_by_key()
	{
	    $instance = new PostCollection(new PostsRepository);

	    $instance->addWithSlug('foo');
	    $instance->addWithSlug('bar');
	    $instance->addWithSlug('baz');

	    $all = $instance->toArray();

	    $this->assertEquals('foo', $all[0]->slug);
	    $this->assertEquals('bar', $all[1]->slug);
	    $this->assertEquals('baz', $all[2]->slug);

	    $instance->sortBy('slug');

	    $allSorted = $instance->toArray();
	    
	    $this->assertEquals('bar', $allSorted[0]->slug);
	    $this->assertEquals('baz', $allSorted[1]->slug);
	    $this->assertEquals('foo', $allSorted[2]->slug);

	    $instance->sortBy('slug', 'desc');

	    $allSortedDesc = $instance->toArray();
	    
	    $this->assertEquals('foo', $allSortedDesc[0]->slug);
	    $this->assertEquals('baz', $allSortedDesc[1]->slug);
	    $this->assertEquals('bar', $allSortedDesc[2]->slug);
	}

	/** @test */
	public function it_flattens_by_key()
	{
	    $instance = new PostCollection(new PostsRepository);

	    $instance->addWithSlug('foo');
	    $instance->addWithSlug('bar');
	    $instance->addWithSlug('baz');

	    $this->assertEquals(
	    	['foo', 'bar', 'baz'], 
	    	$instance->flatten('slug')
	    );
	}

	/** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new PostCollection(new PostsRepository);

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

	    $this->assertInstanceOf(
	    	\App\Resources\CollectionResource::class, 
	    	$instance->apiResource()
	    );
	}
}