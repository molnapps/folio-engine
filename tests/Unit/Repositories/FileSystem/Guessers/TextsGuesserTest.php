<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Image;
use App\Testing\PreviewTest;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\TextsGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class TextsGuesserTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new TextsGuesser($basePath = null, []));
	}

	/** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new TextsGuesser($basePath = null, []));
	}

    /** @test */
	public function it_returns_previous_keys()
	{
	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

	    $this->assertArrayHasKey('slug', $instance->get());
	}

    /** @test */
	public function it_returns_credits_key_if_does_not_exist()
	{
        $this->assertFileDoesNotExist(
            vfsStream::url('folio/contents/credits/foo.md')
        );

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNull($result['credits']);
	}

    /** @test */
	public function it_returns_credits_key_if_credits_exist()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = '**Credits** 
Foo bar'
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNotNull($result['credits']);
        $this->assertEquals($expected, $result['credits']);
	}

    /** @test */
	public function it_returns_credits_key_if_credits_are_defined_as_markdown_block()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@markdown('credits')
**Credits** 
Foo bar
@endmarkdown"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNotNull($result['credits']);
        $this->assertEquals('**Credits** 
Foo bar', 
            $result['credits']
        );
	}

    /** @test */
	public function it_returns_single_texts_key_if_defined()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            "@markdown('excerpt')
Lorem ipsum dolor siet amet
@endmarkdown
**Credits** 
Foo bar"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNotNull($result['credits']);
        $this->assertEquals('**Credits** 
Foo bar', $result['credits']);

        $this->assertArrayHasKey('texts', $result);
        $this->assertIsArray($result['texts']);
        $this->assertCount(2, $result['texts']);
        $this->assertArrayHasKey('credits', $result['texts']);
        $this->assertEquals('**Credits** 
Foo bar', $result['texts']['credits']);
        $this->assertArrayHasKey('excerpt', $result['texts']);
        $this->assertEquals('Lorem ipsum dolor siet amet', $result['texts']['excerpt']);
	}

    /** @test */
	public function it_returns_multiple_texts_key_if_defined()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            "@markdown('excerpt')
Lorem ipsum dolor siet amet
@endmarkdown
@markdown('leading')
Suae quisquae fortunae faber est
@endmarkdown
**Credits** 
Foo bar"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNotNull($result['credits']);
        $this->assertEquals('**Credits** 
Foo bar', $result['credits']);

        $this->assertArrayHasKey('texts', $result);
        $this->assertIsArray($result['texts']);
        $this->assertCount(3, $result['texts']);
        
        $this->assertArrayHasKey('credits', $result['texts']);
        $this->assertEquals('**Credits** 
Foo bar', $result['texts']['credits']);
        
        $this->assertArrayHasKey('excerpt', $result['texts']);
        $this->assertEquals('Lorem ipsum dolor siet amet', $result['texts']['excerpt']);

        $this->assertArrayHasKey('leading', $result['texts']);
        $this->assertEquals('Suae quisquae fortunae faber est', $result['texts']['leading']);
	}

    /** @test */
	public function it_returns_single_texts_key_if_defined_after_credits()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            "**Credits** 
Foo bar
@markdown('excerpt')
Lorem ipsum dolor siet amet
@endmarkdown"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('credits', $result);
        $this->assertNotNull($result['credits']);
        $this->assertEquals('**Credits** 
Foo bar', $result['credits']);

        $this->assertArrayHasKey('texts', $result);
        $this->assertIsArray($result['texts']);
        $this->assertCount(2, $result['texts']);
        
        $this->assertArrayHasKey('credits', $result['texts']);
        $this->assertEquals('**Credits** 
Foo bar', $result['texts']['credits']);
        
        $this->assertArrayHasKey('excerpt', $result['texts']);
        $this->assertEquals('Lorem ipsum dolor siet amet', $result['texts']['excerpt']);
	}

    /** @test */
	public function it_returns_credits_key_if_credits_are_defined_as_markdown_block_on_one_line()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@markdown('foo')Foo bar@endmarkdown"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('texts', $result);
        $this->assertArrayHasKey('foo', $result['texts']);
        $this->assertNotNull($result['texts']['foo']);
        $this->assertEquals('Foo bar', $result['texts']['foo']);
	}

    /** @test */
	public function it_allows_dashes_and_numbers_as_block_header()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@markdown('my-block_01')Foo bar@endmarkdown"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('texts', $result);
        $this->assertArrayHasKey('my-block_01', $result['texts']);
        $this->assertEquals('Foo bar', $result['texts']['my-block_01']);
	}

    /** @test */
	public function it_allows_credit_shortcut()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@credits('foobar')
Foo bar
@endcredits"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('texts', $result);
        $this->assertArrayHasKey('credits-foobar', $result['texts']);
        $this->assertEquals('{credits}
Foo bar
{/credits}', 
            $result['texts']['credits-foobar']
        );
	}

    /** @test */
	public function it_allows_title_shortcut()
	{
        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@title('foobar', 'Foobar')"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();

	    $this->assertArrayHasKey('texts', $result);
        $this->assertArrayHasKey('title-foobar', $result['texts']);
        $this->assertEquals('Foobar', $result['texts']['title-foobar']);
	}

    /** @test */
	public function it_throws_if_title_does_not_end_on_a_new_line()
	{
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('@token($slug, $body) syntax does not accept any content on the same line');

        file_put_contents(
            $path = vfsStream::url('folio/contents/credits/foo.md'),
            $expected = "@title('foobar', 'Foobar') Some credits here"
        );

        $this->assertFileExists($path);

	    $instance = new TextsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

        $result = $instance->get();
	}
}