<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Domain\Image;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\PreviewGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PreviewGuesserTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'images' => [
					'baz' => [],
					'barsvg' => [
						'preview.svg' => ''
					],
					'barjpg' => [
						'preview.jpg' => ''
					],
					'barpng' => [
						'preview.png' => ''
					]
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PreviewGuesser($this->basePath, []));
	}

	/** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new PreviewGuesser($this->basePath, []));
	}

	/** @test */
	public function it_returns_hardcoded_preview_with_relative_path()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    		'preview' => 'foobar.jpg'
	    	]
	    );

	    $this->assertEquals(
	    	'foobar.jpg', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_returns_hardcoded_preview_with_relative_url()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    		'preview' => 'https://example.com/foobar.jpg'
	    	]
	    );

	    $this->assertEquals(
	    	'https://example.com/foobar.jpg', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_returns_guessed_preview_svg()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg')]
	    );

	    $this->assertEquals(
	    	'barsvg/preview.svg', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_returns_guessed_preview_jpg()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barjpg')]
	    );

	    $this->assertEquals(
	    	'barjpg/preview.jpg', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_returns_guessed_preview_png()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barpng')]
	    );

	    $this->assertEquals(
	    	'barpng/preview.png', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_returns_null_if_preview_cannot_be_guessed()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('baz')]
	    );

	    $this->assertArrayNotHasKey('preview', $instance->get());
	}

	/** @test */
	public function it_gives_precedence_to_hardcoded_preview()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg'), 'preview' => 'foobar.png']
	    );

	    $this->assertEquals(
	    	'foobar.png', 
	    	$instance->get()['preview']
	    );
	}

	/** @test */
	public function it_guesses_preview_if_it_is_hardcoded_but_is_null()
	{
	    $instance = new PreviewGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg'), 'preview' => null]
	    );

	    $this->assertEquals(
	    	'barsvg/preview.svg', 
	    	$instance->get()['preview']
	    );
	}
}