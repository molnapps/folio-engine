<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Image;
use App\Testing\VideoTest;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\VideosGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class VideosGuesserTest extends TestCase
{
    use InteractsWithFileSystem;
    
    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new VideosGuesser($basePath = null, []));
	}

    /** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new VideosGuesser($basePath = null, []));
	}

    /** @test */
	public function it_returns_previous_keys()
	{
	    $instance = new VideosGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

	    $this->assertArrayHasKey('slug', $instance->get());
	}

    /** @test */
	public function it_returns_empty_array_by_default()
	{
	    $instance = new VideosGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $videos = $instance->get()['videos'];

        $this->assertIsArray($videos);
	    $this->assertCount(0, $videos);
	}

    /** @test */
	public function it_returns_array_from_qualified_json_file()
	{
		VideoTest::writeJson(
			[ VideoTest::vimeoVideo() ]
		);

		$instance = new VideosGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $videos = $instance->get()['videos'];

		$this->assertIsArray($videos);
	    $this->assertCount(1, $videos);
        
        $this->assertArrayHasKey('provider', $videos[0]);
        $this->assertEquals('vimeo', $videos[0]['provider']);
        
        $this->assertArrayHasKey('id', $videos[0]);
        $this->assertEquals('12345', $videos[0]['id']);

        $this->assertArrayHasKey('slug', $videos[0]);
        $this->assertEquals('foo-video', $videos[0]['slug']);
	}
}