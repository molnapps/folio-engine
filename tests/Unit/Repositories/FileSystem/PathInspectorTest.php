<?php

namespace Tests\Unit\Repositories\FileSystem;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithFileSystem;
use App\Repositories\FileSystem\PathInspector;

class PathInspectorTest extends TestCase
{
    use InteractsWithFileSystem;
    
	/** @test */
    public function it_can_be_instantiated() {
        $this->assertNotNull(new PathInspector(''));
    }

    /** @test */
    public function it_provides_static_constructor() {
        $instance = PathInspector::fromPath('foo');

        $this->assertInstanceOf(PathInspector::class, $instance);
        $this->assertEquals('foo', $instance->toString());
    }

    /** @test */
    public function it_can_be_converted_to_string() {
        $instance = PathInspector::fromPath('foo/bar.jpg');

        $this->assertEquals('foo/bar.jpg', $instance->toString());
    }

    /** @test */
    public function it_returns_base_path() {
        $instance = PathInspector::fromPath('foo/bar.jpg');

        $this->assertEquals('foo', $instance->getBasePath());
    }

    /** @test */
    public function it_returns_base_path_trimming_slashes_only_at_the_end() {
        $instance = PathInspector::fromPath('/foo/bar.jpg');

        $this->assertEquals('/foo', $instance->getBasePath());
    }

    /** @test */
    public function it_returns_relative_path() {
        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.jpg'));

        $this->assertEquals('contents/images/foo.jpg', $instance->getRelativePath());
    }

    /** @test */
    public function it_returns_filename_without_extension() {
        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.jpg'));
        $this->assertEquals('foo', $instance->getFileNameWithoutExtension());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo-bar.jpg'));
        $this->assertEquals('foo-bar', $instance->getFileNameWithoutExtension());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo_bar.jpg'));
        $this->assertEquals('foo_bar', $instance->getFileNameWithoutExtension());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.bar.jpg'));
        $this->assertEquals('foo.bar', $instance->getFileNameWithoutExtension());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo-bar_02.image.jpg'));
        $this->assertEquals('foo-bar_02.image', $instance->getFileNameWithoutExtension());
    }

    /** @test */
    public function it_returns_extension() {
        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.jpg'));
        $this->assertEquals('jpg', $instance->getExtension());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.json'));
        $this->assertEquals('json', $instance->getExtension());
    }

    /** @test */
    public function it_returns_filename() {
        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.jpg'));
        $this->assertEquals('foo.jpg', $instance->getFileName());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo-bar.jpg'));
        $this->assertEquals('foo-bar.jpg', $instance->getFileName());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo_bar.jpg'));
        $this->assertEquals('foo_bar.jpg', $instance->getFileName());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo.bar.jpg'));
        $this->assertEquals('foo.bar.jpg', $instance->getFileName());

        $instance = PathInspector::fromPath(vfsStream::url('folio/contents/images/foo-bar_02.image.jpg'));
        $this->assertEquals('foo-bar_02.image.jpg', $instance->getFileName());
    }

    /** @test */
    public function it_returns_checksum() {
        $file = vfsStream::url('folio/contents/images/foo.jpg');
        
        file_put_contents($file, 'foobar');
        
        $instance = PathInspector::fromPath($file);
        
        $this->assertEquals(
            md5('foobar'), 
            $instance->getChecksum()
        );
    }
}