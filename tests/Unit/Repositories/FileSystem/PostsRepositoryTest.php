<?php

namespace Tests\Unit\Repositories\FileSystem;

use App\Domain\Images\Image;
use App\Testing\PreviewTest;
use App\Testing\VideoTest;
use App\Repositories\PreviewCollection;
use App\Testing\InteractsWithImage;
use App\Repositories\FileSystem\PostsRepository;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PostsRepositoryTest extends TestCase
{
	use InteractsWithImage;

	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'credits' => [
					'bar.md' => 'bar'
				],
				'images' => [],
				'pages' => [
					'about.md' => '---layout:about-layout---',
					'services.md' => ''
				],
				'projects' => [
					'foo.md' => '---layout:foo-layout---',
					'bar.md' => '',
					'baz.md' => '',
				],
				'preferences'  => [
					'defaults.json' => '{"foo": "bar"}',
					'bar.json' => '{"bar": "BAR"}',
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PostsRepository($this->basePath));
	}

	/** @test */
	public function it_implements_posts_repository_interface()
	{
	    $this->assertInstanceOf(
	    	\App\Repositories\PostsRepository::class, 
	    	new PostsRepository($this->basePath)
	    );
	}

	/** @test */
	public function it_throws_if_a_project_does_not_exist()
	{
		$this->expectException(\App\Exceptions\NotFoundException::class);
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $repository->find('yolo');
	}

	/** @test */
	public function it_finds_a_project_if_it_exists()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $this->assertInstanceOf(
	    	\App\Domain\Post::class, 
	    	$repository->find('foo')
	    );
	}

	/** @test */
	public function it_does_not_return_credits_if_markdown_file_does_not_exist()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertNull($project->credits);
	}

	/** @test */
	public function it_returns_credits_if_markdown_file_exists()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('bar');
	    
	    $this->assertEquals('bar', $project->credits);
	}

	/** @test */
	public function it_returns_default_preferences_if_no_dedicated_file_exists()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertEquals(['foo' => 'bar'], $project->preferences);
	}

	/** @test */
	public function it_returns_additional_preferences_if_dedicated_file_exists()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('bar');
	    
	    $this->assertEquals(
	    	['foo' => 'bar', 'bar' => 'BAR'], 
	    	$project->preferences
	    );
	}

	/** @test */
	public function it_returns_path_based_on_type_and_slug()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertEquals(
	    	'/project/foo', 
	    	$project->path
	    );
	}

	/** @test */
	public function it_returns_layout_for_projects()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertEquals(
	    	'foo-layout', 
	    	$project->layout
	    );
	}

	/** @test */
	public function it_guesses_layout_through_type_for_projects()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('bar');
	    
	    $this->assertEquals(
	    	'project', 
	    	$project->layout
	    );
	}

	/** @test */
	public function it_returns_layout_for_pages()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('page');
	    $project = $repository->find('about');
	    
	    $this->assertEquals(
	    	'about-layout', 
	    	$project->layout
	    );
	}

	/** @test */
	public function it_guesses_layout_through_type_for_pages()
	{
	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('page');
	    $project = $repository->find('services');
	    
	    $this->assertEquals(
	    	'page', 
	    	$project->layout
	    );
	}

	/** @test */
	public function it_returns_empty_preview_if_cannot_be_guessed()
	{
		$repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertInstanceOf(Image::class, $project->preview);
		$this->assertFalse($project->preview->exists());
	}

	/** @test */
	public function it_guesses_preview()
	{
		$this->createImageFolderForSlug('foo');
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/foo/preview.jpg')
		);
		
		$repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertInstanceOf(Image::class, $project->preview);
		$this->assertTrue($project->preview->exists());
		$this->assertEquals(
			$this->baseUrlImages . '/foo/preview.jpg', 
			$project->preview->getUrl()
		);
	}

	/** @test */
	public function it_returns_empty_hero_if_cannot_be_guessed()
	{
		$repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertInstanceOf(Image::class, $project->preview);
		$this->assertFalse($project->hero->exists());
	}

	/** @test */
	public function it_guesses_hero()
	{
		$this->createImageFolderForSlug('foo');
		$this->createDummyImage(
			vfsStream::url('folio/contents/images/foo/hero.jpg')
		);
		
		$repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');
	    
	    $this->assertInstanceOf(Image::class, $project->hero);
		$this->assertTrue($project->hero->exists());
		$this->assertEquals(
			$this->baseUrlImages . '/foo/hero.jpg', 
			$project->hero->getUrl()
		);
	}

	/** @test */
	public function it_returns_empty_array_if_no_previews_are_provided()
	{
		PreviewTest::createFolder();
		
		$this->assertFileDoesNotExist(PreviewTest::jsonFilePath());

	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');

		$result = $project->previews->toArray();
	    
	    $this->assertIsArray($result);
		$this->assertCount(0, $result);
	}

	/** @test */
	public function it_guesses_multiple_previews_if_file_exists()
	{
		PreviewTest::createFolder();
		
		PreviewTest::writeJson(
			[ PreviewTest::previewImage('preview.jpg') ]
		);

		$this->assertFileExists(PreviewTest::jsonFilePath());

	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');

		$result = $project->previews->toArray();
	    
	    $this->assertIsArray($result);
		$this->assertCount(1, $result);
	}

	/** @test */
	public function it_returns_empty_array_if_no_videos_are_provided()
	{
		VideoTest::createFolder();
		
		$this->assertFileDoesNotExist(VideoTest::jsonFilePath());

	    $repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');

		$result = $project->videos->toArray();
	    
	    $this->assertIsArray($result);
		$this->assertCount(0, $result);
	}

	/** @test */
	public function it_guesses_multiple_videos_if_file_exists()
	{
		VideoTest::createFolder();
		
		VideoTest::writeJson(
			[ VideoTest::vimeoVideo() ]
		);

		$this->assertFileExists(VideoTest::jsonFilePath());

		$repository = new PostsRepository($this->basePath);
	    $repository->setType('project');
	    $project = $repository->find('foo');

		$result = $project->videos->toArray();
	    
	    $this->assertIsArray($result);
		$this->assertCount(1, $result);
	}
}