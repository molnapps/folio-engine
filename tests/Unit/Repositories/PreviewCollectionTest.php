<?php

namespace Tests\Unit\Repositories;

use App\Domain\Preview;
use App\Repositories\PreviewCollection;
use App\Testing\PreviewTest;
use Tests\TestCase;

class PreviewCollectionTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PreviewCollection);
	}

    /** @test */
	public function it_implements_countable()
	{
	    $this->assertInstanceOf(\Countable::class, new PreviewCollection);
	}

    /** @test */
	public function it_implements_iterator()
	{
	    $this->assertInstanceOf(\Iterator::class, new PreviewCollection);
	}

    /** @test */
	public function it_implements_arrayable()
	{
	    $this->assertInstanceOf(\Illuminate\Contracts\Support\Arrayable::class, new PreviewCollection);
	}

    /** @test */
	public function it_implements_provides_api_resource()
	{
	    $this->assertInstanceOf(\App\Resources\ProvidesApiResource::class, new PreviewCollection);
	}

    /** @test */
	public function it_is_empty_by_default()
	{
	    $this->assertCount(0, new PreviewCollection);
	}

    /** @test */
	public function it_assert_it_is_empty()
	{
        $instance = new PreviewCollection;
	    $this->assertTrue($instance->isEmpty());
	}

    /** @test */
	public function it_can_be_instantiated_with_array_of_previews_as_arrays()
	{
        $collection = new PreviewCollection([
            $expected = PreviewTest::previewImage('foo/foo.jpg')
        ]);

	    $this->assertCount(1, $collection);
        $this->assertSame($expected, $collection->getFirst()->toArray());
	}

    /** @test */
	public function it_can_be_instantiated_with_array_of_previews()
	{
        $collection = new PreviewCollection([
            $expected = new Preview( PreviewTest::previewImage('foo/foo.jpg') )
        ]);

	    $this->assertCount(1, $collection);
        $this->assertSame($expected, $collection->getFirst());
	}

    /** @test */
	public function it_asserts_it_is_not_empty()
	{
        $instance = new PreviewCollection([new Preview()]);
	    $this->assertFalse($instance->isEmpty());
	}

    /** @test */
	public function it_returns_empty_preview_as_first_item_if_empty()
	{
        $instance = new PreviewCollection();

        $firstPreview = $instance->getFirst();

	    $this->assertInstanceOf(Preview::class, $firstPreview);
        $this->assertEquals([], $firstPreview->toArray());
	}

    /** @test */
	public function it_returns_preview_as_first_item()
	{
        $instance = new PreviewCollection([
            $expected = PreviewTest::previewImage('foo/foo.jpg')
        ]);

        $firstPreview = $instance->getFirst();

	    $this->assertInstanceOf(Preview::class, $firstPreview);
        $this->assertEquals($expected, $firstPreview->toArray());
	}

    /** @test */
	public function it_returns_preview_at_index()
	{
        $instance = new PreviewCollection([
            PreviewTest::previewImage('foo/foo.jpg'),
            $expected = PreviewTest::previewImage('foo/bar.jpg'),
        ]);

        $preview = $instance->getRow(1);

	    $this->assertInstanceOf(Preview::class, $preview);
        $this->assertEquals($expected, $preview->toArray());
	}

    /** @test */
	public function it_filters_previews_by_type()
	{
        $previews = new PreviewCollection([
            $expected = PreviewTest::previewImage('foo/foo.jpg'),
            PreviewTest::vimeoVideo('https://www.vimeo.com/12345'),
        ]);

        $this->assertCount(2, $previews);

        $images = $previews->filterByType('image');

	    $this->assertCount(1, $images);
        $this->assertNotSame($previews, $images);
        $this->assertEquals($expected, $images->getFirst()->toArray());
	}

    /** @test */
	public function it_filters_previews_by_media()
	{
        $previews = new PreviewCollection([
            PreviewTest::previewImage('foo/foo.jpg'),
            $expected = PreviewTest::heroImage('foo/bar.jpg'),
        ]);

        $this->assertCount(2, $previews);

        $heroes = $previews->filterByMedia('hero');

	    $this->assertCount(1, $heroes);
        $this->assertNotSame($previews, $heroes);
        $this->assertEquals($expected, $heroes->getFirst()->toArray());
	}

    /** @test */
	public function it_can_be_converted_to_array()
	{
        $previews = new PreviewCollection([
            $expectedPreview = PreviewTest::previewImage('foo/foo.jpg'),
            $expectedHero = PreviewTest::heroImage('foo/bar.jpg'),
        ]);

        $this->assertCount(2, $previews);

        $array = $previews->toArray();

	    $this->assertIsArray($array);
        $this->assertCount(2, $array);
        $this->assertInstanceOf(Preview::class, $array[0]);
        $this->assertEquals($expectedPreview, $array[0]->toArray());
        $this->assertInstanceOf(Preview::class, $array[1]);
        $this->assertEquals($expectedHero, $array[1]->toArray());
	}

    /** @test */
	public function it_returns_api_resource()
	{
        $previews = new PreviewCollection([
            $expectedPreview = PreviewTest::previewImage('foo/foo.jpg'),
            $expectedHero = PreviewTest::heroImage('foo/bar.jpg'),
        ]);

        $apiResource = $previews->apiResource();

        $this->assertInstanceOf(
            \App\Resources\CollectionResource::class, 
            $apiResource
        );
	}
}