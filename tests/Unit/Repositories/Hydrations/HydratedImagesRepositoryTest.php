<?php

namespace Tests\Unit\Repositories\Hydrations;

use App\Domain\Images\Image;
use App\Testing\InteractsWithImage;
use App\Repositories\Array\ArrayRepository;
use App\Repositories\Hydrations\HydratedImagesRepository;
use App\Repositories\Registry;
use App\Repositories\Repository;
use App\Resources\ProvidesApiResource;
use Tests\TestCase;

class HydratedImagesRepositoryTest extends TestCase
{
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new HydratedImagesRepository);
	}

	/** @test */
	public function it_implements_repository_interface()
	{
	    $this->assertInstanceOf(Repository::class, new HydratedImagesRepository);
	}

	/** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $this->assertInstanceOf(ProvidesApiResource::class, new HydratedImagesRepository);
	}

	/** @test */
	public function it_returns_all_values()
	{
	    $instance = new HydratedImagesRepository(
	    	new ArrayRepository(
	    		$array = ['foo' => 'bar']
	    	)
	    );

	    $this->assertEquals($array, $instance->all());
	}

	/** @test */
	public function it_returns_key_value()
	{
	    $instance = new HydratedImagesRepository(
	    	new ArrayRepository(
	    		['foo' => 'bar']
	    	)
	    );

	    $this->assertEquals('bar', $instance->get('foo'));
	}

	/** @test */
	public function it_merges_values_and_returns_self()
	{
	    $instance = new HydratedImagesRepository(
	    	new ArrayRepository(
	    		['foo' => 'bar']
	    	)
	    );

	    $this->assertSame($instance, $instance->merge(['bar' => 'baz']));

	    $this->assertEquals('bar', $instance->get('foo'));
	    $this->assertEquals('baz', $instance->get('bar'));
	}

	/** @test */
	public function it_hydrates_image_paths_when_getting_single_value()
	{
	    $instance = new HydratedImagesRepository(
	    	new ArrayRepository(
	    		['foo' => '/path/to/image.png']
	    	)
	    );

	    $this->assertInstanceOf(Image::class, $instance->get('foo'));
	}

	/** @test */
	public function it_hydrates_image_paths_when_getting_all_values()
	{
	    $instance = new HydratedImagesRepository(
	    	new ArrayRepository(
	    		['foo' => '/path/to/image.png']
	    	)
	    );

	    $this->assertInstanceOf(Image::class, $instance->all()['foo']);
	}
}