<?php

namespace Tests\Unit;

use App\Domain\Settings;
use App\Domain\Post;
use App\Http\WebsiteController;
use App\Portfolio;
use App\PortfolioGenerator;
use App\Repositories\PostCollection;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PortfolioTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [
					'bar.md' => 'Barbaz'
				],
				'projects' => [
					'foo.md' => 'Foobar'
				],
				'preferences' => [
					'defaults.json' => '[]'
				],
				'settings.json' => json_encode(['theme' => 'test'])
			],
			'theme' => [
				'resources' => [
					'views' => [
						'home.blade.php' => 'Homepage',
						'404.blade.php' => '',
					]
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Portfolio($this->basePath));
	}

	/** @test */
	public function it_returns_base_path()
	{
		$portfolio = new Portfolio($this->basePath);

	    $this->assertEquals($this->basePath, $portfolio->getBasePath());
	}

	/** @test */
	public function it_returns_a_collection_of_projects()
	{
		$portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(PostCollection::class, $portfolio->projects());
	}

	/** @test */
	public function it_returns_a_collection_of_pages()
	{
		$portfolio = new Portfolio($this->basePath);
		
	    $this->assertInstanceOf(PostCollection::class, $portfolio->pages());
	}

	/** @test */
	public function it_returns_an_array_of_links()
	{
		$portfolio = new Portfolio($this->basePath);
		
	    $this->assertEquals([], $portfolio->links());
	}

	/** @test */
	public function it_returns_a_settings_object()
	{
		$portfolio = new Portfolio($this->basePath);
		
	    $this->assertInstanceOf(Settings::class, $portfolio->settings());
	}

	/** @test */
	public function it_returns_post_instance_for_project()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(Post::class, $portfolio->getProject('foo'));
	}

	/** @test */
	public function it_returns_post_instance_for_page()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(Post::class, $portfolio->getPage('bar'));
	}

	/** @test */
	public function it_returns_post_instance_for_homepage_default_page()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(Post::class, $portfolio->getHomePost());
	}

	/** @test */
	public function it_returns_post_collection_for_gallery()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(PostCollection::class, $portfolio->getGallery());
	}

	/** @test */
	public function it_returns_post_collection_for_menu()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(PostCollection::class, $portfolio->getMenu());
	}

	/** @test */
	public function it_returns_array_for_links()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertIsArray($portfolio->getLinks());
	}

	/** @test */
	public function it_returns_an_instance_of_portfolio_generator()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertInstanceOf(PortfolioGenerator::class, $portfolio->generate());
	}

	/** @test */
	public function it_runs_the_portfolio()
	{
	    $portfolio = new Portfolio($this->basePath);

	    ob_start();
	    $controller = $portfolio->run();
	    ob_get_clean();

	    $this->assertInstanceOf(WebsiteController::class, $controller);

	    $response = $this->getTestResponse($controller);
	    $response->assertSee('Homepage');
	}

	/** @test */
	public function it_auto_discovers_files_and_returns_self()
	{
	    $portfolio = new Portfolio($this->basePath);

	    $this->assertCount(0, $portfolio->projects());
	    $this->assertCount(0, $portfolio->pages());

	    $this->assertSame($portfolio, $portfolio->autoDiscovery());

	    $this->assertCount(1, $portfolio->projects());
	    $this->assertCount(1, $portfolio->pages());
	}
}