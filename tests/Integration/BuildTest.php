<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithConsole;
use App\Testing\InteractsWithFileSystem;

class BuildTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithConsole;

    /** @test */
	public function it_does_not_compile_optimized_images_twice()
	{
	    ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->create();

        $compiledImages = [
            'foo-compressed.jpg',
            'foo-compressed.webp',
            'foo-portrait-compressed.jpg',
            'foo-portrait-compressed.webp',
            'foo-portrait.jpg',
        ];
        
        $this->assertFileDoesNotExist(
            $imagesStorageFolder = $this->getStoragePathForSlug('foo')
        );

        $this->runCommand('portfolio:generate-aws');
        
        $this->assertFileExists($imagesStorageFolder);
        $this->assertEquals($compiledImages, $this->getAllFiles($imagesStorageFolder));

        $this->modifyFilesToVerifyRecompilation($compiledImages);
        
        $this->runCommand('portfolio:generate-aws');

        $this->assertEquals($compiledImages, $this->getAllFiles($imagesStorageFolder));
        $this->assertFilesWereNotRecompiled($compiledImages);
	}

    /** @test */
	public function it_does_not_compile_optimized_images_switching_between_production_and_development()
	{
	    ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->create();

        $compiledImages = [
            'foo-compressed.jpg',
            'foo-compressed.webp',
            'foo-portrait-compressed.jpg',
            'foo-portrait-compressed.webp',
            'foo-portrait.jpg',
        ];
        
        $this->assertFileDoesNotExist(
            $imagesStorageFolder = $this->getStoragePathForSlug('foo')
        );

        $this->runCommand('portfolio:generate-aws');
        
        $this->assertFileExists($imagesStorageFolder);
        $this->assertEquals($compiledImages, $this->getAllFiles($imagesStorageFolder));

        $this->modifyFilesToVerifyRecompilation($compiledImages);
        
        $this->runCommand('portfolio:generate-dev');

        $this->assertEquals($compiledImages, $this->getAllFiles($imagesStorageFolder));
        $this->assertFilesWereNotRecompiled($compiledImages);

        $this->runCommand('portfolio:generate-aws');

        $this->assertEquals($compiledImages, $this->getAllFiles($imagesStorageFolder));
        $this->assertFilesWereNotRecompiled($compiledImages);
	}

    private function modifyFilesToVerifyRecompilation(array $compiledImages)
    {
        foreach ($compiledImages as $fileName) {
            $filePath = $this->getStoragePathForFileName($fileName);
            $this->assertNotEquals('', file_get_contents($filePath));
            file_put_contents($filePath, '');
        }
    }

    private function assertFilesWereNotRecompiled(array $compiledImages)
    {
        foreach ($compiledImages as $fileName) {
            $filePath = $this->getStoragePathForFileName($fileName);
            $this->assertEquals('', file_get_contents($filePath));
        }
    }

    private function getStoragePathForFileName($fileName)
    {
        return vfsStream::url("folio/storage/images/foo/{$fileName}");
    }

    private function getStoragePathForSlug($slug)
    {
        return vfsStream::url("folio/storage/images/{$slug}");
    }

    private function getAllFiles(string $path)
    {
        return array_values(
            array_diff(
                scandir($path),
                ['.', '..']
            )
        );
    }
}