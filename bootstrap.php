<?php

use App\App;
use Dotenv\Dotenv;

require_once "vendor/autoload.php";

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

App::bootstrapThemes([
	'folio' => \Folio\Themes\Folio\ThemeProvider::class,
	'test' => \Folio\Themes\Test\ThemeProvider::class
]);