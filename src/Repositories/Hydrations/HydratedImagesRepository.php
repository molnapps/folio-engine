<?php

namespace App\Repositories\Hydrations;

use App\Repositories\Array\ArrayRepository;
use App\Repositories\Repository;
use App\Resources\ImagePathResource;
use App\Resources\ProvidesApiResource;

class HydratedImagesRepository implements Repository, ProvidesApiResource
{
	private $delegate;

	public function __construct(Repository $delegate = null)
	{
		$this->delegate = $delegate ?: new ArrayRepository;
	}

	public function __call($methodName, $args) {
		if (method_exists($this->delegate, $methodName)) {
			return call_user_func_array([$this->delegate, $methodName], $args);
		}

		throw new \Exception('Method is not defined.');
	}

	public function apiResource()
	{
		return $this->delegate->apiResource();
	}

	public function merge(array $override)
	{
		$this->delegate->merge($override);

		return $this;
	}

	public function all()
	{
		return ImagePathResource::hydrate(
			$this->delegate->all()
		)->get();
	}

	public function get($key)
	{
		return ImagePathResource::hydrate(
			$this->delegate->get($key)
		)->get();
	}
}