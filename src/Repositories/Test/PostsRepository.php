<?php

namespace App\Repositories\Test;

use App\Discovery\Slug;
use App\Repositories\AbstractPostsRepository;
use Carbon\Carbon;

class PostsRepository extends AbstractPostsRepository
{
	private $exclude = [];

	public function exclude($slug)
	{
		$this->exclude[] = $slug;
		
		return $this;
	}

	public function doFind(Slug $slug) : array
	{
		if (in_array($slug->getFull(), $this->exclude)) {
			return [];
		}

		return [
			'exists' => true,
			'layout' => $this->type,
			'published_at' => Carbon::now()->format('Y-m-d'),
			'preferences' => [],
			'preview' => 'https://example.com/uploads/a1dd2ed5d338a5057020ec2278e46c2a.svg',
			'type' => $this->type,
			'path' => '/' . $this->type . '/' . $slug->getFull(),
			'slug' => $slug->getFull(),
			'title' => ucfirst($slug->getFull()),
			'excerpt' => ucfirst($slug->getFull()) . ' excerpt',
			'content' => ucfirst($slug->getFull()) . ' content',
			'credits' => $this->type === 'project' 
				? ucfirst($slug->getFull()) . ' credits' 
				: '',
		];
	}
}