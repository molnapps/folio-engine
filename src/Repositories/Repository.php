<?php

namespace App\Repositories;

interface Repository
{
	public function merge(array $merge);
	public function all();
	public function get($key);
}