<?php

namespace App\Repositories\Json;

use Adbar\Dot;
use App\Repositories\Array\ArrayRepository;
use App\Repositories\Repository;
use App\Resources\ProvidesApiResource;

class JsonRepository implements Repository, ProvidesApiResource
{
	private $delegate;
	
	private $basePath;
	private $assoc = true;

	public function __construct($basePath, $assoc = true)
	{
		$this->basePath = $basePath;
		$this->assoc = $assoc;
		$this->load();
	}

	public function refresh()
	{
		$this->load();
	}

	private function load()
	{
		$data = [];

		if (file_exists($this->basePath)) {
			$data = json_decode(
				file_get_contents($this->basePath), 
				$this->assoc
			);
		}

		$this->delegate = new ArrayRepository($data);
	}

	public function apiResource()
	{
		return $this->delegate->apiResource();
	}

	public function merge(array $merge)
	{
		$this->delegate->merge($merge);
		
		return $this;
	}

	public function all()
	{
		return $this->delegate->all();
	}

	public function get($key)
	{
		return $this->delegate->get($key);
	}
}