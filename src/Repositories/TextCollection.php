<?php

namespace App\Repositories;

class TextCollection
{
	private $texts;

	public function __construct(array $texts = null)
	{
		if (is_null($texts)) {
			$texts = [];
		}

		$this->texts = $texts;
	}

	public function has($slug)
	{
		return array_key_exists($slug, $this->texts);
	}

	public function get($slug)
	{
		return $this->texts[$slug];
	}
}