<?php

namespace App\Repositories;

use App\Discovery\Slug;
use App\Discovery\SlugFactory;
use App\Domain\Post;

abstract class AbstractPostsRepository implements PostsRepository
{
	protected $type;
	
	public function setType($type)
	{
		$this->type = $type;
		
		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function find($slug) : Post
	{
		$slug = SlugFactory::normalize($slug);

		return new Post(
			$this->doFind($slug)
		);
	}

	abstract protected function doFind(Slug $slug) : array;
}