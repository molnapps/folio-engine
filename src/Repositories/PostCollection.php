<?php

namespace App\Repositories;

use App\Domain\Post;

class PostCollection extends AbstractCollection
{
	private $repository;
	
	public function __construct(PostsRepository $repository)
	{
		$this->repository = $repository;
	}

	protected function getTargetClass()
	{
		return Post::class;
	}

	public function addWithSlug($slug)
	{
		return $this->add(
			$this->find($slug)
		);
	}

	public function find($slug)
	{
		$collection = $this->filterByClosure(
			function (Post $post) use ($slug) {
				return $post->slug == $slug;
			}
		);

		if ( ! $collection->isEmpty()) {
			return $collection->getFirst();
		}

		return $this->repository->find($slug);
	}

	public function visibleOnly()
	{
		return $this->filterByClosure(
			function (Post $post) {
				return $post->isVisible();
			}
		);
	}

	public function exceptSlug($slug)
	{
		return $this->filterByClosure(
			function (Post $post) use ($slug) {
				return $post->slug != $slug;
			}
		);
	}

	protected function getNewInstance()
	{
		return new static($this->repository);
	}
}