<?php

namespace App\Repositories;

use \App\Domain\Preview;

class PreviewCollection extends AbstractCollection 
{
	protected function getTargetClass()
	{
		return Preview::class;
	}

	public function filterByType($type)
	{
		return $this->filterByClosure(
			function (Preview $preview) use ($type) {
				return $preview->isType($type);
			}
		);
	}

	public function filterByMedia($media)
	{
		return $this->filterByClosure(
			function (Preview $preview) use ($media) {
				return $preview->isMedia($media);
			}
		);
	}
}