<?php

namespace App\Repositories\FileSystem;

use App\Repositories\Repository as RepositoryInterface;
use App\Repositories\ClientsRepository as ClientsRepositoryInterface;
use App\Resources\ProvidesApiResource;
use App\Repositories\Json\JsonRepository;
use App\Domain\Client;

class ClientsRepository implements RepositoryInterface, ClientsRepositoryInterface, ProvidesApiResource
{
	private $delegate;
	
	public function __construct($basePath)
	{
		$this->delegate = new JsonRepository($basePath . '/clients.json');
	}

    public function apiResource() {
        return new \App\Resources\RepositoryResource($this);
    }

    public function merge(array $merge) {
        $this->delegate->merge($merge);

        return $this;
    }

    public function all() {
        return $this->delegate->all();
    }

    public function get($key) {
        return $this->delegate->get($key);
    }

	public function findBySlug($slug) : Client
	{
		foreach ($this->delegate->all() as $client) {
            if ($client['slug'] == $slug) {
                return new Client($client);
            }
        }

        return new Client(['slug' => $slug]);
	}
}