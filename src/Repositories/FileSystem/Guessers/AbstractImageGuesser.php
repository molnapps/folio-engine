<?php

namespace App\Repositories\FileSystem\Guessers;

abstract class AbstractImageGuesser implements Guesser
{
	private $imagesBasePath;
	private $result = [];

	public function __construct($basePath, array $result)
	{
		$this->imagesBasePath = $basePath . '/images';
		$this->result = $result;
	}

	abstract public function getKey();
	
	public function get() : array
	{
		if ($this->hasValidHardcodedKey()) {
			return $this->result;
		}

		foreach ($this->getSupportedFormats() as $format) {
			if ($this->fileExists($format)) {
				$this->result[$this->getKey()] = $this->getRelativePath($format);
			}
		}

		return $this->result;
	}

	private function fileExists($format)
	{
		return file_exists($this->getFullPath($format));
	}

	private function getFullPath($format)
	{
		return $this->imagesBasePath . '/' . $this->getRelativePath($format);
	}

	private function getRelativePath($format = 'svg')
	{
		return sprintf(
			'%s/%s.%s', 
			$this->result['slug']->getOriginal(),
			$this->getKey(),
			$format
		);
	}

	private function hasValidHardcodedKey() {
		return isset($this->result[$this->getKey()]) && 
			! empty($this->result[$this->getKey()]);
	}

	private function getSupportedFormats()
	{
		return ['svg', 'png', 'jpg'];
	}
}