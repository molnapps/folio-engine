<?php

namespace App\Repositories\FileSystem\Guessers;

interface Guesser {
	public function get() : array;
}