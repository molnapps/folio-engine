<?php

namespace App\Repositories\FileSystem;

class FilesCrawler
{
    private $root;
    private $allFolders = [];
    private $allFiles = [];

    public function __construct($root)
    {
        $this->root = $root;
        $this->allFolders = [];
        $this->allFiles = [];

        $this->crawl($this->root);
    }

    public static function fromPath($root)
    {
        return new static($root);
    }

    public function getAllFiles() : array
    {
        return $this->allFiles;
    }

    public function eachFile(callable $callback)
    {
        foreach ($this->allFiles as $filePath) {
            call_user_func_array($callback, [$filePath]);
        }

        return $this;
    }

    private function crawl($path)
    {
        $items = $this->getItemsInFolder($path);

        foreach ($items as $item) {
            $this->handleItem($path . '/' . $item);
        }
    }

    private function handleItem($itemPath)
    {
        if ($this->isFolder($itemPath)) {
            $this->addFolder($itemPath);
            return $this->crawl($itemPath);
        }

        return $this->addFile($itemPath);
    }

    private function addFolder($itemPath)
    {
        $this->allFolders[] = $itemPath;
    }

    private function addFile($itemPath)
    {
        $this->allFiles[] = $itemPath;
    }

    private function isFolder($itemPath)
    {
        return is_dir($itemPath);
    }

    private function getItemsInFolder($path)
    {
        return array_values(array_diff(scandir($path), ['.', '..']));
    }
}