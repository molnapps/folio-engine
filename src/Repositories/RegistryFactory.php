<?php

namespace App\Repositories;

use Exception;

class RegistryFactory
{
	private $registry;

	public function setRegistry(Registry $registry)
	{
		$this->registry = $registry;
	}

	public function make($driver = null)
	{
		$driver = $driver ?: env('REPOSITORY_DRIVER');

		if ($this->registry) {
			return $this->registry;
		}

		if ($driver === 'database') {
			return new Database\Registry;
		}
		
		if ($driver === 'file') {
			return new FileSystem\Registry(
				env('BASE_PATH') . '/contents'
			);
		}
		
		if ($driver === 'test') {
			return new Test\Registry;
		}
		
		throw new Exception('Unknown registry driver');
	}
}