<?php

namespace App;

use App\Domain\Post;
use Carbon\Carbon;

class Context
{
	private $isStatic = false;
	private $isHome = false;

	public static function staticWebsite()
	{
		return (new static)->isStatic();
	}

	public function isStatic()
	{
		$this->isStatic = true;

		return $this;
	}

	public function isHome()
	{
		$this->isHome = true;

		return $this;
	}

	public function isNotHome()
	{
		$this->isHome = false;

		return $this;
	}

	public function getContainerModifier()
	{
		return $this->isHome 
			? 'home' 
			: 'page';
	}

	public function getPath(Post $post)
	{
		if ( ! $post->shouldRender()) {
			return $post->path;
		}

		return $this->isStatic 
			? $post->path . '.html' 
			: $post->path;
	}

	public function reset()
	{
		$this->isStatic = false;
		$this->isHome = false;
		
		return $this;
	}
}