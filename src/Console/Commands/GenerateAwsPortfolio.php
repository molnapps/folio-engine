<?php

namespace App\Console\Commands;

use App\Portfolio;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAwsPortfolio extends Command
{
	protected static $defaultName = 'portfolio:generate-aws';

	protected function configure()
	{
		$this
			->setDescription('Generates a aws portfolio.')
			->setHelp('This command allows you to generate a static aws portfolio');

		$this->addOption(
			'destination',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Where should the html files be published?',
			null
		);

		$this->addOption(
			'images',
			$shortcut = null,
			InputOption::VALUE_NEGATABLE,
			'With this option, images will be published',
			true
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		return $this->getApplication()
			->find('portfolio:generate')
			->run(new ArrayInput([
				'--build' => true,
				'--images' => $input->getOption('images'),
				'--format' => 'html',
				'--images-publisher' => 'optimized',
				'--images-only-used' => true,
				'--theme-publisher' => 'copy',
				'--destination' => $input->getOption('destination')
			]), $output);
	}
}