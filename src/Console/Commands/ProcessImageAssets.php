<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Themes\Publishers\Images\Processors\ProcessFactory;

class ProcessImageAssets extends Command
{
    protected static $defaultName = 'images:process';
    
    protected function configure()
    {
        $this
            ->setDescription('Processes the image assets.')
            ->setHelp('This command allows you to process the raw image assets.');

        $this
            ->addOption(
                'path',
                $shortcut = null,
                InputOption::VALUE_OPTIONAL,
                'Should a relative path be used?',
                ''
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ProcessFactory::usedImages(
            $this->getSourcePath($input)
        )->process();
        
        return Command::SUCCESS;
    }

    private function getSourcePath(InputInterface $input)
    {
        return $this->getRelativePath($input)
            ? env('IMAGES_SOURCE_PATH') . '/' . $this->getRelativePath($input)
            : env('IMAGES_SOURCE_PATH');
    }

    private function getRelativePath(InputInterface $input)
    {
        return trim($input->getOption('path'), '/');
    }
}