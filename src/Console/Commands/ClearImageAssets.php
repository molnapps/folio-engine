<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Themes\Publishers\Images\ImagesStorageCleaner;

class ClearImageAssets extends Command
{
    protected static $defaultName = 'images:clear';
    
    protected function configure()
    {
        $this
            ->setDescription('Clear image assets.')
            ->setHelp('This command allows you to clear image assets.');

        $this
            ->addOption(
                'path',
                $shortcut = null,
                InputOption::VALUE_OPTIONAL,
                'Should a relative path be used?',
                ''
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ImagesStorageCleaner::fromRelativePath(
            $input->getOption('path')
        )->deleteUnusedImages();

        return Command::SUCCESS;
    }
}