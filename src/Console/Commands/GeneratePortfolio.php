<?php

namespace App\Console\Commands;

use App\Portfolio;
use App\Generators\FolioManifest;
use App\Themes\Publishers\Images\ImagesBackup;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePortfolio extends Command
{
	protected static $defaultName = 'portfolio:generate';

	protected function configure()
	{
		$this
			->setDescription('Generates a static portfolio.')
			->setHelp('This command allows you to generate a static portfolio');

		$this->addOption(
			'build',
			$shortcut = null,
			InputOption::VALUE_NONE,
			'Replace BASE_URL with BASE_URL_PRODUCTION'
		);

		$this->addOption(
			'images',
			$shortcut = null,
			InputOption::VALUE_NEGATABLE,
			'With this option, images will be published',
			true
		);

		$this->addOption(
			'destination',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Where should the files be published?',
			null
		);

		$this->addOption(
			'theme-destination',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Where should the theme assets be published?',
			null
		);

		$this->addOption(
			'format',
			$shortcut = null,
			InputOption::VALUE_REQUIRED,
			'What format should be used?',
			'html'
		);

		$this->addOption(
			'images-publisher',
			$shortcut = null,
			InputOption::VALUE_REQUIRED,
			'What publisher should be used for images?',
			'symlink'
		);

		$this->addOption(
			'images-only-used',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Should only used images be published?',
			true
		);

		$this->addOption(
			'theme-publisher',
			$shortcut = null,
			InputOption::VALUE_REQUIRED,
			'What publisher should be used for theme assets?',
			'symlink'
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$imagesBackup = new ImagesBackup($input);
		$imagesBackup->save();

		FolioManifest::fromJson()->eraseAllFilesAndFolders();
		
		$this->getApplication()
			->find('publish:theme')
			->run(new ArrayInput($this->getThemeOptions($input)), $output);
		
		if ($input->getOption('images')) {
			$this->getApplication()
				->find('publish:images')
				->run(new ArrayInput($this->getImagesOptions($input)), $output);
		} else {
			$imagesBackup->restore();
		}
		
		$this->getApplication()
			->find('publish:content')
			->run(new ArrayInput($this->getContentOptions($input)), $output);

		app(FolioManifest::class)->write();
		
		if ($input->getOption('build')) {
			app(FolioManifest::class)->replaceAllBaseUrlWithBaseUrlBuild();
		};

		return Command::SUCCESS;
	}

	private function getContentOptions(InputInterface $input)
	{
		$options = [
			'--format' => $input->getOption('format')
		];

		if ($input->getOption('destination')) {
			$options = array_merge($options, [
				'--destination' => $input->getOption('destination')
			]);
		}

		return $options;
	}

	private function getThemeOptions(InputInterface $input)
	{
		$options = [
			'--publisher' => $input->getOption('theme-publisher')
		];

		if ($input->getOption('theme-destination')) {
			$options = array_merge($options, [
				'--destination' => $input->getOption('theme-destination')
			]);
		}

		return $options;
	}

	private function getImagesOptions(InputInterface $input)
	{
		return [
			'--publisher' => $input->getOption('images-publisher'),
			'--only-used' => $input->getOption('images-only-used')
		];
	}
}