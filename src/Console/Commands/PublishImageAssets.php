<?php

namespace App\Console\Commands;

use App\Portfolio;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\PublisherFactory;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class PublishImageAssets extends Command
{
    protected static $defaultName = 'publish:images';

    protected function configure()
    {
        $this
            ->setDescription('Publishes the image assets.')
            ->setHelp('This command allows you to publish the image assets.');

        $this
            ->addOption(
                'publisher',
                $shortcut = null,
                InputOption::VALUE_REQUIRED,
                'What publisher should be used?',
                'symlink'
            );

        $this
            ->addOption(
                'only-used',
                $shortcut = null,
                InputOption::VALUE_OPTIONAL,
                'Should only used images be published?',
                true
            );

        $this
            ->addOption(
                'path',
                $shortcut = null,
                InputOption::VALUE_OPTIONAL,
                'Should a relative path be used?',
                ''
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);

        try {
            $manifest = ManifestFactory::images();
            
            PublisherFactory::makeWithBackup(
                $input->getOption('publisher'), 
                $input->getOption('only-used'),
            )
                ->publish(
                    $manifest,
                    $this->getSourcePath($input), 
                    $this->getDestinationPath($input)
                );

            $manifest->publish();
       } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</>");
       }

        $output->writeln("<info>Portfolio image assets successfully published</>");
        
        return Command::SUCCESS;
    }

    private function getSourcePath(InputInterface $input)
    {
        return $this->getRelativePath($input)
            ? env('IMAGES_SOURCE_PATH') . '/' . $this->getRelativePath($input)
            : env('IMAGES_SOURCE_PATH');
    }

    private function getDestinationPath(InputInterface $input)
    {
        return $this->getRelativePath($input)
            ? env('IMAGES_DESTINATION_PATH') . '/' . $this->getRelativePath($input)
            : env('IMAGES_DESTINATION_PATH');
    }

    private function getRelativePath(InputInterface $input)
    {
        return trim($input->getOption('path'), '/');
    }
}