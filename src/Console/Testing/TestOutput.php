<?php

namespace App\Console\Testing;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;

class TestOutput implements OutputInterface
{
	private $formatter;
	public $output = [];

    public function write($messages, bool $newline = false, int $options = 0)
    {
    	$this->output[] = $messages;
    }

    public function writeln($messages, int $options = 0)
    {
    	$this->output[] = $messages;
    }

    public function setVerbosity(int $level)
    {

    }

    public function getVerbosity()
    {

    }

    public function isQuiet()
    {
    	return false;
    }

    public function isVerbose()
    {
    	return false;
    }

    public function isVeryVerbose()
    {
    	return false;
    }

    public function isDebug()
    {
    	return false;
    }

    public function setDecorated(bool $decorated)
    {

    }

    public function isDecorated()
    {
    	return false;
    }

    public function setFormatter(OutputFormatterInterface $formatter)
    {
    	$this->formatted = $formatter;
    }

    public function getFormatter()
    {
    	return $this->formatter;
    }
}
