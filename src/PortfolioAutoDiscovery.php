<?php

namespace App;

use App\Discovery\MarkdownFilesFinder;
use App\Repositories\PostCollection;
use App\Repositories\Registry;

class PortfolioAutoDiscovery
{
	private $portfolio;
	private $basePath;

	public function __construct(Portfolio $portfolio)
	{
		$this->portfolio = $portfolio;

		$this->guardRepositoryDriver();

		$this->basePath = app(Registry::class)->getBasePath();
	}

	private function guardRepositoryDriver()
	{
		if (env('REPOSITORY_DRIVER') !== 'file') {
			throw new \Exception('Driver must be file.');
		}
	}

	public function discover()
	{
		$projects = $this->autoDiscover('projects');
		$projects->sortBy('published_at', 'desc');

		$pages = $this->autoDiscover('pages');
		$this->addSelectedWorksPage($pages);
		$this->addContactsPage($pages);
		$pages->sortBy('published_at', 'asc');
	}

	private function autoDiscover($type)
	{
		$finder = MarkdownFilesFinder::withBasePath($this->basePath);

		$collection = $this->portfolio->$type();

		foreach ($finder->find($type) as $slug) {
			$collection->addWithSlug($slug);
		}

		return $collection;
	}

	private function addSelectedWorksPage(PostCollection $pages)
	{
		if ( ! $this->portfolio->settings()->has('portfolio')) {
			return;
		}

		$pages->addWithArray([
			'title' => 'Selected Works', 
			'slug' => 'portfolio',
			'path' => '/', 
			'disableRender' => true,
			'published_at' => '1970-01-01',
		]);
	}

	private function addContactsPage(PostCollection $pages)
	{
		if ( ! $this->portfolio->settings()->has('email')) {
			return;
		}

		$pages->addWithArray([
			'title' => 'Contact', 
			'slug' => 'contact',
			'path' => sprintf('mailto:%s', $this->portfolio->settings()->email), 
			'disableRender' => true,
			'published_at' => \Carbon\Carbon::now()->format('Y-m-d'),
		]);
	}
}