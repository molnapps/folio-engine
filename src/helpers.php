<?php

function app($identifier = null)
{
	if ( ! $identifier) {
		return \App\App::instance();
	}

	return \App\App::instance()->get($identifier);
}

function registry()
{
	return app(\App\Repositories\Registry::class);
}

function view($id, $context = [])
{
	return response(
		app(\Jenssegers\Blade\Blade::class)->render($id, $context)
	);
}

function response($content)
{
	return new \App\Http\Response($content);
}

function theme()
{
	return new \App\Views\ThemeFacade;
}

function image($relativePath)
{
	return \App\Domain\ImageFactory::makeResponsive($relativePath);
}