<?php

namespace App\Generators;

use \Illuminate\Contracts\Support\Arrayable;
use \App\Generators\FolioManifest\FolioManifestFile;
use \App\Generators\FolioManifest\FolioManifestRow;
use \App\Generators\FolioManifest\FolioManifestTypes;
use \App\Generators\FolioManifest\FolioManifestValidator;

class FolioManifest implements \Countable, Arrayable
{
	private $basePath;
	private $manifest = [];
	private $folioManifestFile;

	public function __construct() 
	{
		$this->basePath = env('BASE_PATH');
		$this->folioManifestFile = new FolioManifestFile;
	}

	private function isEmpty()
	{
		return count($this) === 0;
	}

	public function count() : int
	{
		return count($this->manifest);
	}

	public function toArray()
	{
		return array_map(
			function ($manifestFolioRow) {
				return $manifestFolioRow->toArray();
			},
			$this->manifest
		);
	}

	public static function fromJson($destinationPath = null) : FolioManifest
	{
		return static::make()->createFromJson($destinationPath);
	}

	public static function fromArray(array $manifest) : FolioManifest
	{
		return static::make()->createFromArray($manifest);
	}

	public static function make() : FolioManifest
	{
		return new static;
	}

	private function createFromJson($manifestFilePath = null) : FolioManifest
	{
		return $this->folioManifestFile->read($manifestFilePath);
	}

	private function createFromArray(array $manifest) : FolioManifest
	{
		$this->manifest = (new FolioManifestValidator)->sanitize($manifest);
		
		return $this;
	}

	// Write/Delete methods

	public function write($pathToManifestFolder = null)
	{
		$this->folioManifestFile->write($this, $pathToManifestFolder);
		
		return $this;
	}

	public function createFilesForTest()
	{
		foreach ($this->manifest as $folioManifestRow) {
			$folioManifestRow->createForTest();
		}

		return $this;
	}

	public function eraseAllFilesAndFolders()
	{
		$manifest = $this->manifest;

		rsort($manifest);

		foreach ($manifest as $folioManifestRow) {
			$folioManifestRow->erase();
		}
	}

	public function replaceAllBaseUrlWithBaseUrlBuild()
	{
		$htmlFiles = $this
			->filterByType('file')
			->filterByFileExtension('html')
			->manifest;

		foreach ($htmlFiles as $folioManifestRow) {
			$folioManifestRow->replaceAllBaseUrlWithBaseUrlBuild();
		}

		$jsonFiles = $this
			->filterByType('file')
			->filterByFileExtension('json')
			->manifest;

		foreach ($jsonFiles as $folioManifestRow) {
			$folioManifestRow->replaceAllBaseUrlWithBaseUrlBuild();
		}
	}

	// Add methods

	public function addFile(string $absolutePath) 
	{
		return $this->addItem($absolutePath, FolioManifestTypes::TYPE_FILE);
	}

	public function addFolder(string $absolutePath) 
	{
		return $this->addItem($absolutePath, FolioManifestTypes::TYPE_FOLDER);
	}

	public function addSymlink(string $absolutePath) 
	{
		return $this->addItem($absolutePath, FolioManifestTypes::TYPE_SYMLINK);
	}

	// Has methods

	public function hasAny($absolutePath)
	{
		foreach ((new FolioManifestTypes)->getAll() as $type) {
			if ($this->has($absolutePath, $type)) {
				return true;
			}
		}

		return false;
	}
	
	public function hasFile($absolutePath)
	{
		return $this->has($absolutePath, FolioManifestTypes::TYPE_FILE);
	}

	public function hasFolder($absolutePath)
	{
		return $this->has($absolutePath, FolioManifestTypes::TYPE_FOLDER);
	}

	public function hasSymlink($absolutePath)
	{
		return $this->has($absolutePath, FolioManifestTypes::TYPE_SYMLINK);
	}

	// Private methods

	private function addItem(string $absolutePath, string $type)
	{
		$relativePath = $this->getRelativePath($absolutePath);

		return $this->add(
			new FolioManifestRow([
				'path' => $relativePath, 
				'type' => $type
			])
		);
	}

	private function add(FolioManifestRow $folioManifestRow)
	{
		if ($this->has($folioManifestRow->getAbsolutePath())) {
			return $this;
		}

		$this->manifest[] = $folioManifestRow;

		return $this;
	}

	private function has($absolutePath, $type = null)
	{
		$result = $this->filterByPathExact($absolutePath);

		if ($type) {
			$result = $result->filterByType($type);
		}

		return ! $result->isEmpty();
	}

	public function merge(FolioManifest $anotherManifest)
	{
		foreach ($anotherManifest->manifest as $folioManifestRow) {
			$this->add($folioManifestRow);
		}
	}

	public function filterByFileExtension($extension)
	{
		$callback = function ($folioManifestRow) use ($extension) {
			return $folioManifestRow->hasFileExtension($extension);
		};

		return $this->filterByCallback($callback);
	}

	public function filterByType($type)
	{
		$callback = function ($folioManifestRow) use ($type) {
			return $folioManifestRow->isType($type);
		};

		return $this->filterByCallback($callback);
	}

	public function filterByPath($absolutePath)
	{
		$relativePath = $this->getRelativePath($absolutePath);

		$callback = function ($folioManifestRow) use ($relativePath) {
			return $folioManifestRow->pathStartsWith($relativePath);
		};

		return $this->filterByCallback($callback);
	}

	private function filterByPathExact($absolutePath)
	{
		$relativePath = $this->getRelativePath($absolutePath);

		$callback = function ($folioManifestRow) use ($relativePath) {
			return $folioManifestRow->pathEqualsTo($relativePath);
		};

		return $this->filterByCallback($callback);
	}

	private function filterByCallback(callable $callback)
	{
		return (new static)->createFromArray(
			array_values(
				array_filter(
					$this->manifest,
					$callback
				)
			)
		);
	}

	private function getRelativePath($absolutePath)
	{
		(new FolioManifestValidator)->guardRealPath($absolutePath);
		
		return trim(
			str_replace(
				$this->basePath,
				'',
				$absolutePath
			),
			'/'
		);
	}
}