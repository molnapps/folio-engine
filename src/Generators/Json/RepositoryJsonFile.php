<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Generators\FileSystem;
use App\Resources\ImagePathResource;
use App\Resources\ProvidesApiResource;

class RepositoryJsonFile implements Generator
{
	private $destinationPath;
	private $repositoryKey;

	public function __construct($destinationPath, $repositoryKey)
	{
		$this->destinationPath = $destinationPath;
		$this->repositoryKey = $repositoryKey;
	}

	public function generate()
	{
		app(FileSystem::class)->createFile(
			$this->getDestinationPath(),
			$this->getJson()
		);
	}

	private function getDestinationPath()
	{
		return "{$this->destinationPath}/{$this->repositoryKey}.json";
	}

	private function getJson()
	{
		return json_encode(
			$this->getArray()
		);
	}

	private function getArray()
	{
		return [
			$this->repositoryKey => $this->doGetArray(),
		];
	}

	private function doGetArray()
	{
		$repository = registry()->{$this->repositoryKey}();

		if ($repository instanceof ProvidesApiResource) {
			return $repository->apiResource()->toArray();
		}

		return $repository->all();
	}

	private function getRepositoryKeysRegisteredAtRuntime()
	{
		return array_values(
			array_diff(
				registry()->getRepositoryKeys(), 
				$this->getFirstClassCitizens()
			)
		);
	}
}