<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Portfolio;

class ProjectsJsonFile extends JsonFile
{
	protected function getFilename() : string
	{
		return 'projects.json';
	}

	protected function getArray() : array
	{
		return [
			'projects' => $this->portfolio->projects()->visibleOnly()->apiResource()->toArray()
		];
	}
}