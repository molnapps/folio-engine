<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Portfolio;

class LinksJsonFile extends JsonFile
{
	protected function getFilename() : string
	{
		return 'links.json';
	}

	protected function getArray() : array
	{
		return [
			'links' => $this->portfolio->links()
		];
	}
}