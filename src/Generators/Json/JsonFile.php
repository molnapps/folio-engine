<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Generators\FileSystem;
use App\Portfolio;

abstract class JsonFile implements Generator
{
	protected $portfolio;
	private $destinationPath;

	public function __construct(Portfolio $portfolio, $destinationPath)
	{
		$this->portfolio = $portfolio;
		$this->destinationPath = $destinationPath;
	}

	public function generate()
	{
		app(FileSystem::class)->createFile(
			$this->getDestinationPath(),
			$this->getJson()
		);
	}

	private function getDestinationPath() : string
	{
		return $this->destinationPath . '/' . $this->getFilename();
	}

	abstract protected function getFilename() : string;

	private function getJson() : string
	{
		return json_encode($this->getArray());
	}

	abstract protected function getArray() : array;
}