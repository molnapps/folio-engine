<?php

namespace App\Generators\FolioManifest;

class FolioManifestRow
{
	private $row;
	private $file;

	public function __construct(array $row)
	{
		$this->row = $row;
		
		$this->file = new FolioManifestRowFile(
			$this->getRelativePath()
		);
	}

	public function toArray()
	{
		return $this->row;
	}

	public function createForTest()
	{
		$this->file->createForTest($this);
	}

	public function erase()
	{
		$this->file->erase();
	}

	public function replaceAllBaseUrlWithBaseUrlBuild()
	{
		$this->file->replaceAllBaseUrlWithBaseUrlBuild();
	}

	public function pathStartsWith($relativePath)
	{
		return stripos($this->row['path'], $relativePath) === 0;
	}

	public function pathEqualsTo($relativePath)
	{
		return $this->row['path'] == $relativePath;
	}

	public function exists()
	{
		return $this->file->exists();
	}

	public function getRelativePath()
	{
		return $this->hasPath()
			? $this->row['path']
			: null;
	}

	public function hasPath()
	{
		return array_key_exists('path', $this->row);
	}

	public function isType($type)
	{
		return $this->row['type'] == $type;
	}

	public function hasValidType()
	{
		return (new FolioManifestTypes)->isValid(
			$this->getType()
		);
	}

	private function getType()
	{
		return $this->row['type'];
	}

	private function setType($type)
	{
		$this->row['type'] = $type;

		return $this;
	}

	public function hasType()
	{
		return array_key_exists('type', $this->row);
	}

	public function guessTypeIfNotSpecified()
	{
		if ($this->hasType()) {
			return $this;
		}

		return $this->setType(
			$this->guessType()
		);
	}

	private function guessType()
	{
		if ( ! $this->file->exists()) {
			return;
		}

		return (new FolioManifestTypes)->guessFromPath(
			$this->getAbsolutePath()
		);
	}

	public function hasFileExtension($extension)
	{
		return $this->file->hasExtension($extension);
	}

	public function getAbsolutePath()
	{
		return $this->file->getAbsolutePath();
	}
}