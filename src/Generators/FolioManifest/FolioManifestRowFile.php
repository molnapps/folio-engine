<?php

namespace App\Generators\FolioManifest;

class FolioManifestRowFile
{
	private $basePath;
	private $relativePath;
	
	public function __construct($relativePath)
	{
		$this->basePath = env('BASE_PATH');
		$this->relativePath = $relativePath;
	}

	public function erase()
	{
		$absolutePath = $this->getAbsolutePath();

		if (is_file($absolutePath)) {
			$result = unlink($absolutePath);
		}
		if (is_link($absolutePath)) {
			$result = unlink($absolutePath);
		}
		if (is_dir($absolutePath)) {
			$result = rmdir($absolutePath);
		}
	}

	public function createForTest(FolioManifestRow $folioManifestRow)
	{
		if ( ! app()->env('testing')) {
			throw new \Exception('Cannot use this method outside of testing environment.');
		}

		if ( ! $this->exists()) {
			if ($folioManifestRow->isType(FolioManifestTypes::TYPE_FOLDER)) {
				$this->createFolder();
			}

			if ($folioManifestRow->isType(FolioManifestTypes::TYPE_FILE)) {
				$this->write();
			}
		}
	}

	public function replaceAllBaseUrlWithBaseUrlBuild()
	{
		$baseUrl = env('BASE_URL');
		$baseUrlBuild = env('BASE_URL_BUILD');
		
		$source = $this->read();

		$source = str_replace(
			$baseUrl,
			$baseUrlBuild,
			$source
		);

		$source = str_replace(
			$this->getJsonEncodedUrl($baseUrl) , 
			$this->getJsonEncodedUrl($baseUrlBuild), 
			$source
		);

		$this->write($source);
	}

	private function read()
	{
		return file_get_contents($this->getAbsolutePath());
	}

	private function createFolder()
	{
		return mkdir($this->getAbsolutePath());
	}

	private function write($content = null)
	{
		return file_put_contents($this->getAbsolutePath(), $content);
	}

	public function exists()
	{
		return file_exists($this->getAbsolutePath());
	}

	public function hasExtension($extension)
	{
		$extension = '.' . $extension;

		return substr(
			$this->getRelativePath(), 
			-strlen($extension), 
			strlen($extension)
		) === $extension;
	}

	public function getAbsolutePath()
	{
		return $this->basePath . DIRECTORY_SEPARATOR . $this->getRelativePath();
	}

	private function getRelativePath()
	{
		return $this->relativePath;
	}

	private function getJsonEncodedUrl($url)
	{
		return trim(
			json_encode($url), 
			'"'
		);
	}
}