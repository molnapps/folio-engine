<?php

namespace App\Generators\FolioManifest;

class FolioManifestValidator
{
	public function sanitize(array $manifest)
	{
		$manifest = $this->normalize($manifest);

		$manifest = array_filter($manifest, [$this, 'hasPath']);
		$manifest = array_filter($manifest, [$this, 'hasRealPath']);
		$manifest = array_map([$this, 'guessTypeIfNotSpecified'], $manifest);
		$manifest = array_filter($manifest, [$this, 'hasType']);
		$manifest = array_filter($manifest, [$this, 'hasValidType']);

		return $manifest;
	}

	private function normalize(array $manifest)
	{
		return array_map(
			[$this, 'normalizeRow'],
			$manifest
		);
	}

	private function normalizeRow($row) {
		if (is_array($row)) {
			$row = new FolioManifestRow($row);
		}

		if ( ! $row instanceof FolioManifestRow) {
			throw new \Exception('Please provide a FolioManifestRow instance or an array.');
		}

		return $row;
	}

	public function hasPath(FolioManifestRow $item)
	{
		return $item->hasPath();
	}

	public function hasType(FolioManifestRow $item)
	{
		return $item->hasType($item);
	}

	public function hasRealPath(FolioManifestRow $item) {
		$this->guardRealPath(
			$item->getRelativePath()
		);

		return true;
	}

	public function guardRealPath($absolutePath)
	{
		if (
			stripos($absolutePath, './') === false && 
			stripos($absolutePath, '../') === false
		) {
			return;
		}

		throw new \Exception('Only real path allowed.');
	}

	public function hasValidType(FolioManifestRow $item) {
		return $item->hasValidType();
	}

	public function guessTypeIfNotSpecified(FolioManifestRow $item)
	{
		return $item->guessTypeIfNotSpecified();
	}
}