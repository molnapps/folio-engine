<?php

namespace App\Generators\FolioManifest;

use App\Generators\FolioManifest;

class FolioManifestFile
{
	private $defaultDestinationPath;
	private $fileName = 'manifest.folio.json';

	public function __construct()
	{
		$this->defaultDestinationPath = env('BASE_PATH') . '/manifests';
	}

	public function read($pathToManifestFolder) : FolioManifest
	{
		if ( ! $this->exists($pathToManifestFolder)) {
			return new FolioManifest;
		}

		return FolioManifest::fromArray(
            json_decode(
                $this->getContents($pathToManifestFolder), 
                $assoc = true
            )
        );
	}

	private function getContents($pathToManifestFolder)
	{
		return file_get_contents(
			$this->getFilePath($pathToManifestFolder)
		);
	}

	public function write(FolioManifest $manifest, $pathToManifestFolder)
	{
		if ( ! $this->destinationPathExists($pathToManifestFolder)) {
			mkdir($this->getDestinationPath($pathToManifestFolder));
		}

		file_put_contents(
			$this->getFilePath($pathToManifestFolder), 
			json_encode($manifest->toArray())
		);
	}

    private function destinationPathExists($pathToManifestFolder)
    {
        return file_exists(
            $this->getDestinationPath($pathToManifestFolder)
        );
    }

	public function exists($manifestFilePath)
	{
		return file_exists(
			$this->getFilePath($manifestFilePath)
		);
	}

	private function getFilePath($destinationPath)
	{
		return $this->getDestinationPath($destinationPath) . 
			DIRECTORY_SEPARATOR . 
			$this->getFileName();
	}

	private function getDestinationPath($destinationPath)
	{
		return $destinationPath ?: $this->getDefaultDestinationPath();
	}

	private function getDefaultDestinationPath()
	{
		return $this->defaultDestinationPath;
	}

	private function getFileName()
	{
		return $this->fileName;
	}
}