<?php

namespace App\Generators\Html;

use App\Context;
use App\Domain\Post;
use App\Generators\Generator;
use App\Generators\FileSystem;
use App\Portfolio;
use App\Themes\ThemeResponseProvider;
use Gajus\Dindent\Indenter;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class HtmlGenerator implements Generator
{
	private $portfolio;
	private $destinationPath;
	private $responseProvider;
	private $log;

	public function __construct(
		Portfolio $portfolio, 
		$destinationPath,
		LoggerInterface $log = null
	) {
		$this->portfolio = $portfolio;
		$this->destinationPath = $destinationPath;
		$this->log = $log ?: new NullLogger;
	}

	public function swapResponseProviderForTest(ThemeResponseProvider $responseProvider)
	{
		return $this->setResponseProvider($responseProvider);
	}

	public function generate()
	{
		$this->generateHomepage();
		$this->generateProjects();
		$this->generatePages();
	}

	private function generateHomepage()
	{
		$this->writeHtmlFile(
			new Post(['path' => '/index', 'slug' => 'index']), 
			$this->getResponseProvider()->homepage()->getContent()
		);
	}

	private function generateProjects()
	{
		$this->logType('Projects');

		$this->createFolderIfDoesNotExists('project');

		foreach ($this->portfolio->getGallery()->toArray() as $post) {
			$this->writeHtmlFile(
				$post, 
				$this->getResponseProvider()->content($post)->getContent()
			);
		}
	}

	private function generatePages()
	{
		$this->logType('Pages');

		$this->createFolderIfDoesNotExists('page');
		
		foreach ($this->portfolio->getMenu()->toArray() as $post) {
			$this->writeHtmlFile(
				$post, 
				$this->getResponseProvider()->content($post)->getContent()
			);
		}
	}

	private function getResponseProvider()
	{
		if ( ! $this->responseProvider) {
			$this->setResponseProvider(
				$this->getDefaultResponseProvider()
			); 
		}

		return $this->responseProvider;
	}

	private function setResponseProvider(ThemeResponseProvider $responseProvider)
	{
		$this->responseProvider = $responseProvider;

		return $this;
	}

	private function getDefaultResponseProvider()
	{
		return new ThemeResponseProvider(
			$this->portfolio, 
			app(Context::class)->isStatic()
		);
	}

	private function createFolderIfDoesNotExists($folder)
	{
		app(FileSystem::class)->createFolderIfDoesNotExist(
			$this->getDestinationPath() . '/' . $folder
		);
	}

	private function writeHtmlFile(Post $post, $markup)
	{
		if ( ! $post->shouldRender()) {
			return $this->logFile($post);
		}
		
		$filePath = $this->getHtmlFilePath($post);
		
		app(FileSystem::class)->createFile(
			$filePath, 
			(new Indenter)->indent($markup)
		);
		
		$this->logFile($post, $filePath);
	}

	private function getHtmlFilePath(Post $post)
	{
		return $this->getDestinationPath() . $post->path . '.html';
	}

	private function getDestinationPath()
	{
		return $this->destinationPath;
	}

	private function logType($type)
	{
		$this->log->notice('');
		$this->log->notice($type);
	}

	private function logFile(Post $post, $filePath = '')
	{
		if ( ! $post->shouldRender()) {
			$operation = 'skip';
		} 

		if ($filePath) {
			$operation = file_exists($filePath) 
				? 'override' 
				: 'create';
		}

		$this->log->notice(sprintf('[%s] [%s]', $operation, $post->slug));
		$this->log->notice(sprintf('- %s', $filePath ?: $post->path));
	}
}