<?php

namespace App\Testing;

use App\Generators\FolioManifest;

trait InteractsWithFolioManifest
{
    protected function assertFolioManifestContainsFiles(array $expectedAbsolutePaths)
    {
        $folioManifest = $this->getFolioManifestFromJsonFile();

        foreach ($expectedAbsolutePaths as $key => $absolutePath) {
           $this->assertFolioManifestContainsFile($absolutePath, $folioManifest);
        }
    }

    protected function assertFolioManifestDoesNotContainFiles(array $expectedFilePaths)
    {
        $folioManifest = $this->getFolioManifestFromJsonFile();

        foreach ($expectedFilePaths as $key => $absolutePath) {
            $this->assertFolioManifestDoesNotContainFile($absolutePath, $folioManifest);
        }
    }

    protected function assertFolioManifestContainsFile($absolutePath, FolioManifest $folioManifest = null)
    {
        $folioManifest = $folioManifest ?: $this->getFolioManifestFromJsonFile();

        $this->assertTrue(
            $folioManifest->hasAny($absolutePath), 
            "Failed asserting that FolioManifest contains file [{$absolutePath}]"
        );
    }

    protected function assertFolioManifestDoesNotContainFile($absolutePath, FolioManifest $folioManifest = null)
    {
        $folioManifest = $folioManifest ?: $this->getFolioManifestFromJsonFile();

        $this->assertFalse(
            $folioManifest->hasAny($absolutePath), 
            "Failed asserting that FolioManifest does not contain file [{$absolutePath}]"
        );
    }

    private function getFolioManifestFromJsonFile()
    {
        return FolioManifest::fromJson();
    }
}