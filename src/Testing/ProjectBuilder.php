<?php

namespace App\Testing;

use org\bovigo\vfs\vfsStream;
use App\Themes\Publishers\Images\Processors\ProcessFactory;

class ProjectBuilder
{
	use InteractsWithImage;
	use InteractsWithMarkdownFiles;
	use InteractsWithConsole;

	private $slug;
	private $usedImages = [];
	private $notUsedImages = [];
	private $imageAttributesMap = [];

	private $sourcePath;
	private $publicPath;
	private $isPublished = true;
	private $meta = [];
	private $type = 'project';

	public function __construct($slug)
	{
        $this->sourcePath = vfsStream::url('folio/contents/images');
        $this->publicPath = vfsStream::url('folio/public/images');
		$this->slug = $slug;
	}

	public static function fromSlug($slug)
	{
		return new static($slug);
	}

	public function withLandscapeUsedImage($name)
	{
		return $this->withUsedImages([
			[
				'name' => $name, 
				'attributes' => $this->landscape(),
			]
		]);
	}

	public function withUsedImages($images)
	{
		$images = $this->normalizeImage($images);

		$this->usedImages = $this->getImageNames($images);

		$this->recordImageAttributes($images);

		return $this;
	}

	public function withNotUsedImages($images)
	{
		$images = $this->normalizeImage($images);

		$this->notUsedImages = $this->getImageNames($images);

		$this->recordImageAttributes($images);

		return $this;
	}

	public function isNotPublished()
	{
		$this->isPublished = false;

		return $this;
	}

	public function withMeta(array $meta)
	{
		$this->meta = $meta;

		return $this;
	}

	public function isPage()
	{
		return $this->withType('page');
	}

	private function withType($type)
	{
		$this->type = $type;

		return $this;
	}

	public function withWebsiteFeatureImage(string $imageName)
	{
		$jsonFile = vfsStream::url(
			sprintf('folio/contents/previews/%s.json', $this->slug)
		);
		
		$previews = [
            [
                'src' => sprintf('%s/%s.jpg', $this->slug, $imageName), 
                'type' => 'image', 
                'media' => ['website-feature']
            ]
        ];

        file_put_contents(
            $jsonFile, 
            json_encode($previews)
        );

		return $this->withNotUsedImages($imageName);
	}

	public function create()
	{
		$this->createImageFolderForSlug($this->slug);

		$methodName = $this->type == 'project'
			? 'createPublishedProjectFile'
			: 'createPublishedPageFile';
		
		$this->$methodName([
			'slug' => $this->slug,
			'meta' => $this->getMeta(),
			'markdown' => $this->getUsedImagesMarkdown()
		]);

		foreach ($this->usedImages as $usedImage) {
			$this->createDummyImage(
				$this->getSourceImagePath($usedImage),
				$this->getImageAttributes($usedImage)
			);
		}

		foreach ($this->notUsedImages as $notUsedImage) {
			$this->createDummyImage(
				$this->getSourceImagePath($notUsedImage),
				$this->getImageAttributes($notUsedImage)
			);
		}
		
		return $this;
	}

	public function publishWebsiteFeatureImages()
	{
		ProcessFactory::websiteFeatureImages()->process();
	}

	public function publishImages()
	{
		$this->setUpConsoleApplication();
		$this->runCommand('publish:images', ['--publisher' => 'optimized']);
	}

	public function replaceUsedImage($usedImage, $newUsedImage)
	{
		$this->deleteSourceImage($usedImage);
		
		$this->withUsedImages($newUsedImage);
		
		$this->createDummyImage(
			$this->getSourceImagePath($newUsedImage)
		);
		
		$this->createPublishedProjectFile([
			'slug' => $this->slug,
			'markdown' => $this->getUsedImagesMarkdown()
		]);
	}

	public function deleteSourceImage($name)
	{
		unlink(
			$this->getSourceImagePath($name)
		);

		return $this;
	}

	public function deleteSourceFolder()
	{
		rmdir(
			$this->getSourceFolder()
		);

		return $this;
	}

	public function assertBefore($phpunit)
	{
		foreach ($this->getNotExpectedFiles() as $notExpectedFile) {
			$phpunit->assertFileDoesNotExist($notExpectedFile);
		}
		
		foreach ($this->getExpectedFiles() as $expectedFile) {
			$phpunit->assertFileDoesNotExist($expectedFile);
		}
	}

	public function assertAfter($phpunit)
	{
		foreach ($this->getNotExpectedFiles() as $notExpectedFile) {
			$phpunit->assertFileDoesNotExist($notExpectedFile);
		}
		
		foreach ($this->getExpectedFiles() as $expectedFile) {
			$phpunit->assertFileExists($expectedFile);
		}
	}

	public function getExpectedFiles()
	{
		return $this->getPublicImagePaths($this->usedImages);
	}

	public function getNotExpectedFiles()
	{
		return $this->getPublicImagePaths($this->notUsedImages);
	}

	private function getPublicImagePaths($images)
	{
		$result = [];

		foreach ($this->getExpectedCroppedImages($images) as $name) {
			$result = array_merge(
				$result, 
				$this->getPublicImagePathForExpectedFormats($name)
			);
		}

		return $result;
	}

    private function getExpectedCroppedImages(array $images)
    {
        $crops = [];

		foreach ($images as $image) {
			$crops[] = $image;
			foreach ($this->getExpectedCrops($image) as $crop) {
				$crops[] = $image . '-' . $crop;
			}
		}

        return $crops;
    }

	private function getPublicImagePathForExpectedFormats($name)
	{
		return array_map(
			function ($format) use ($name) {
				return $this->getPublicImagePath($name, $format);
			},
			$this->getExpectedFormats()
		);
	}

	public function getSourceImagePath($name, $format = 'jpg')
	{
		return "{$this->getSourceFolder()}/{$name}.{$format}";
	}

	private function getSourceFolder()
	{
		return "{$this->sourcePath}/{$this->slug}";
	}

	private function getPublicImagePath($name, $format)
	{
		return "{$this->getPublicFolder()}/{$name}-compressed.{$format}";
	}

	private function getPublicFolder()
	{
		return "{$this->publicPath}/{$this->slug}";
	}

	private function getUsedImagesMarkdown()
	{
		$markdown = array_map(
			function ($image) {
				return "![$image]";
			},
			$this->usedImages
		);

		return implode(" ", $markdown);
	}

	private function getExpectedFormats()
	{
		return ['jpg', 'webp'];
	}

	private function getExpectedCrops($image)
	{
		if ($this->imageAttributesMap[$image] == $this->square()) {
			return ['portrait'];
		}

		return ['portrait', 'square'];
	}

	private function getImageAttributes($image)
	{
		return $this->imageAttributesMap[$image];
	}

	private function recordImageAttributes(array $images)
	{
		foreach ($images as $image) {
			$this->imageAttributesMap[$image['name']] = $image['attributes'];
		}
	}

	private function getImageNames(array $images)
	{
		return array_map(
			function ($image) {
				return $image['name'];
			},
			$images
		);
	}

	private function normalizeImage($images)
	{
		if (is_string($images)) {
			$images = [ $images ];
		}

		if ( ! is_array($images)) {
			throw new \Exception('Please provide either a string or an array');
		}

		return array_map(
			[$this, 'normalizeImageEntry'],
			$images
		);
	}

	private function normalizeImageEntry($imageEntry)
	{
		if (is_array($imageEntry)) {
			return $imageEntry;
		}
		
		return $this->getImageEntry($imageEntry);
	}

	private function getImageEntry(string $image)
	{
		return [
			'name' => $image, 
			'attributes' => $this->square(),
		];
	}

	private function landscape()
	{
		return [
			'width' => 200, 
			'height' => 100
		];
	}

	private function square()
	{
		return [
			'width' => 100,
			'height' => 100
		];
	}

	private function getMeta() : array
	{
		$meta = [
			'published_at' => $this->isPublished 
				? 'yesterday' 
				: 'tomorrow'
		];

		return array_merge($this->meta, $meta);
	}
}