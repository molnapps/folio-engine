<?php

namespace App\Testing;

use org\bovigo\vfs\vfsStream;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\ProcessedPublisher;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Crops\SourceRectCalculator;

trait InteractsWithImage
{
	private $filename;
	
	protected $baseSourcePathImages;
	protected $baseDestinationPathImages;
	protected $baseUrlImages;

	/** @before */
	public function setUpBaseUrlImages()
	{
		$this->baseUrlImages = env('IMAGES_DESTINATION_URL');

		$this->assertNotNull($this->baseUrlImages);
	}

	/** @before */
	public function setUpBaseSourcePathImages()
	{
		$this->baseSourcePathImages = env('IMAGES_SOURCE_PATH');

		$this->assertNotNull($this->baseSourcePathImages);
	}

	/** @before */
	public function setUpBaseDestinationPathImages()
	{
		$this->baseDestinationPathImages = env('IMAGES_DESTINATION_PATH');

		$this->assertNotNull($this->baseDestinationPathImages);
	}
	
	public function bootstrapInteractsWithImage()
	{
		$filename = 'foo.jpg';

		$this->setImageFileName($filename);

		$this->createDummyImage(
			$this->getImagePath($filename),
			['width' => 120, 'height' => 20]
		);
	}

	private function unlinkDefaultImage()
	{
		unlink($this->getImagePath());
	}

	protected function getImagePath($filename = null)
	{
		$filename = $filename ?: $this->getImageFileName();

		return vfsStream::url("folio/contents/images/{$filename}");
	}

	private function setImageFileName($filename)
	{
		$this->filename = $filename;
		return $this;
	}

	protected function getImageFileName()
	{
		return $this->filename;
	}

	protected function createImageFolderForSlug($slug)
	{
		$this->createSourceImageFolderForSlug($slug);
	}

	protected function createSourceImageFolderForSlug($slug)
	{
		mkdir(
			vfsStream::url(
				sprintf('folio/contents/images/%s', $slug)
			)
		);
	}

	protected function createStorageImageFolderForSlug($slug)
	{
		mkdir(
			vfsStream::url(
				sprintf('folio/storage/images/%s', $slug)
			)
		);
	}

	protected function publishImages() : Manifest
	{
		$manifest = ManifestFactory::images();

		(new ProcessedPublisher)
			->publish(
				$manifest,
				$this->baseSourcePathImages,
				$this->baseDestinationPathImages
			);

		return $manifest->publish();
	}

	protected function createDummyImageWithAlpha($filePath, array $overrideOptions = [])
	{
		$options = $this->getImageOptions($overrideOptions);

		$image = imagecreatetruecolor($options['width'], $options['height']);

		imagesavealpha($image, true);

		$transparentColor = imagecolorallocatealpha($image, 0, 0, 0, 127);
		imagefill($image, 0, 0, $transparentColor);

		imagepng($image, $filePath);

		return $image;
	}

	protected function createDummyImage($filePath, array $overrideOptions = []) 
	{
		$options = $this->getImageOptions($overrideOptions);

		if ($this->getImageExtension($filePath) == 'svg') {
			return $this->createDummySvgImage($filePath, $options);
		}

		return $this->createTrueColorImage($filePath, $options);
	}

	private function createTrueColorImage($filePath, array $options)
	{
		$image = imagecreatetruecolor($options['width'], $options['height']);
		
		switch ($this->getImageExtension($filePath))
		{
			case 'png':
				imagepng($image, $filePath);
				break;
			case 'webp':
				imagewebp($image, $filePath);
				break;
			case 'gif':
				imagegif($image, $filePath);
				break;
			default:
				imagejpeg($image, $filePath, $options['quality']);
		}

		imagedestroy($image);

		return $image;
	}

	private function createDummySvgImage($filePath, array $options)
	{
		$viewBox = sprintf(
			'viewBox="0 0 %d %d"',
			$options['width'],
			$options['height']
		);

		$sizeAttr = sprintf(
			'width="%d" height="%d"',
			$options['width'],
			$options['height']
		);
		
		if (isset($options['corrupted'])) {
			$viewBox = '';
		}

		$svg = sprintf(
			'<svg %s xmlns="http://www.w3.org/2000/svg" %s>
				<circle cx="50" cy="50" r="50" />
			</svg>',
			$viewBox,
			$sizeAttr
		);

		file_put_contents($filePath, $svg);

		return $svg;
	}

	private function createRandomNoiseImage($sourceFile)
    {
        $width = 100;
        $height = 100;
        $im = imagecreatetruecolor($width,$height);
        for($i = 0; $i < $width; $i++) {
            for($j = 0; $j < $height; $j++) {
                $color = imagecolorallocate($im, rand(0,255), rand(0,255), rand(0,255));
                imagesetpixel($im, $i, $j, $color);
            }
        }
        imagejpeg($im, $sourceFile);
        imagedestroy($im);
    }

    private function createCroppedImage($sourcePath, $destinationPath, array $destinationRect)
    {
		$sourceRect = (new SourceRectCalculator($sourcePath))
			->getSourceRect($destinationRect);

		$sourceImage = imagecreatefromjpeg($sourcePath);
        
		$destinationImage = imagecreatetruecolor(
            $destinationRect['width'], 
            $destinationRect['height']
        );

        imagecopyresampled(
            $destinationImage, 
            $sourceImage, 
            $destinationRect['x'], 
            $destinationRect['y'], 
            $sourceRect['x'], 
            $sourceRect['y'], 
            $destinationRect['width'], 
            $destinationRect['height'], 
            $sourceRect['width'], 
            $sourceRect['height']
        );

        imagejpeg($destinationImage, $destinationPath);

        imagedestroy($sourceImage);
        imagedestroy($destinationImage);

		return;
    }

	private function getImageExtension($filePath)
	{
		$parts = explode('.', $filePath);
		$extension = end($parts);
		return $extension;
	}

	private function getImageOptions(array $overrideOptions)
	{
		$defaultOptions = ['width' => 50, 'height' => 50, 'quality' => 75];
		
		return array_merge($defaultOptions, $overrideOptions);
	}
}