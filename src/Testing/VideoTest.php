<?php

namespace App\Testing;

use App\Domain\Video;
use App\Repositories\VideoCollection;
use org\bovigo\vfs\vfsStream;

class VideoTest
{
	public static function createFolder($path = null)
	{
		$path = $path ?: vfsStream::url('folio/contents/videos');
		mkdir($path);
	}

	public static function jsonFilePath()
	{
		return vfsStream::url('folio/contents/videos/foo.json');
	}

	public static function writeJson(array $videos)
	{
		return static::write(
			static::jsonFilePath(),
			$videos
		);
	}

	private static function write($filePath, array $videos)
    {
		$collection = new VideoCollection($videos);

		$array = array_map(
			function (Video $video) {
				return static::toRepositoryRepresentation($video);
			},
			$collection->toArray()
		);

		file_put_contents(
            $filePath, 
            $content = json_encode($array)
        );

		return $collection;
    }

	public static function toRepositoryRepresentation(Video $video)
	{
		return [
			'provider' => $video->provider, 
			'id' => $video->id, 
			'slug' => $video->slug,
			'size' => $video->size
		];
	}

    public static function vimeoVideo($id = '12345', array $override = [])
    {
        return array_merge([
            'provider' => 'vimeo',
            'id' => $id,
			'slug' => 'foo-video',
			'size' => [
				'width' => 1920,
				'height' => 1080
			]
        ], $override);
    }

	public static function youtubeVideo($id = 'abc123', array $override = [])
    {
        return array_merge([
            'provider' => 'youtube',
            'id' => $id,
			'slug' => 'bar-video',
			'size' => [
				'width' => 1920,
				'height' => 1080
			]
        ], $override);
    }

	public static function video(array $override = [])
    {
        return array_merge(static::vimeoVideo(), $override);
    }
}