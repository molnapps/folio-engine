<?php

namespace App\Testing;

use App\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

trait InteractsWithConsole
{
    private $console;

    /** @before */
    public function setUpConsoleApplication()
    {
        $this->console = new Application;
    }

    protected function runCommand($name, array $input = [])
    {
        $this->console->find($name)->run(
            new ArrayInput($input), 
            new NullOutput
        );
    }
}