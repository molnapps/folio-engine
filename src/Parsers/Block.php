<?php

namespace App\Parsers;

interface Block
{
    public function getType();
	public function getSlug();
	public function getBody();
}