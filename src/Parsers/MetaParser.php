<?php

namespace App\Parsers;

use App\Meta;

class MetaParser
{
	private $rows = [];

	public function __construct(string|null $fragment)
	{
		if ( ! $fragment) {
			return;
		}
		
		$this->rows = array_values(
			array_filter(
				explode("\n", $fragment)
			)
		);
	}

	public function get()
	{
		$result = [];
		
		foreach ($this->rows as $row) {
			$parts = explode(':', $row);
			$property = array_shift($parts);
			$result[$property] = trim(implode(':', $parts));
		}
		
		return $result;
	}
}