<?php

namespace App\Parsers;

class BaseBlock implements Block
{
    private $defaultSlug;
	private $defaultType;
    private $type;
	private $sourceBlock;
	private $block;

	public function __construct(string $type, string $sourceBlock, string $defaultSlug = null)
	{
        $this->type = $type;
		$this->sourceBlock = $sourceBlock;
        $this->defaultSlug = $defaultSlug ?: 'default';
		$this->defaultType = 'markdown';

		$this->parse();
	}

	public function isEmpty(){
		return ! trim($this->getBody());
	}

	public function getType()
	{
		return $this->block['type'];
	}

	public function getSlug() {
		return $this->block['slug'];
	}

	public function getBody() {
		return $this->block['body'];
	}

	private function parse() {
		$lines = $this->getLines();

		$this->block = [
			'type' => $this->parseType($lines),
			'slug' => $this->parseSlug($lines),
			'body' => $this->parseBody($lines)
		];

		return $this;
	}

	private function getLines() : array
	{
		return array_map(
			function ($line) {
				return new BlockLine($line);
			},
			explode("\n", trim($this->sourceBlock))
		);
	}

	private function parseType(array $lines)
	{
		if ($lines[0]->isCreditsHeader()) {
			return 'markdown';
		}

		if ($lines[0]->isHeader()) {
			return $lines[0]->getType();
		}

		return $this->defaultType;
	}

	private function parseSlug(array $lines)
	{
		if ($lines[0]->isCreditsHeader()) {
			return 'credits-' . $lines[0]->getSlug();
		}

		if ($lines[0]->isTitleHeader()) {
			return 'title-' . $lines[0]->getSlug();
		}

		if ($lines[0]->isHeader()) {
			return $lines[0]->getSlug();
		}

		return $this->defaultSlug;
	}

	private function parseBody(array $lines) : string
	{
		return $lines[0]->isCreditsHeader()
			? $this->getCreditsBody($lines)
			: $this->getDefaultBody($lines);
	}

	private function getCreditsBody(array $lines)
	{
		return implode("\n", [
			'{credits}',
			$this->getDefaultBody($lines),
			'{/credits}'
		]);
	}

	private function getDefaultBody(array $lines)
	{
		$linesBody = array_map(
			function (BlockLine $line) {
				return $line->getBody();
			},
			$lines
		);

		return trim(
			implode("\n", $linesBody)
		);
	}
}