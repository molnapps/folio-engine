<?php

namespace App\Discovery;

class MarkdownSlug implements Slug
{
	private $file;

	private function __construct(array $file)
	{
		$this->file = $file;
	}

	public function __toString()
	{
		return $this->getFull();
	}

	public static function fromFile(array $file)
	{
		return new static($file);
	}

	public function getFilePath()
	{
		return trim(
			$this->getBasePath() . '/' . $this->getFilename(), 
			'/'
		);
	}

	public function getPath()
	{
		return trim(
			$this->getBasePath() . '/' . $this->getOriginal(), 
			'/'
		);
	}

	public function getBasePath()
	{
		return $this->file['path'];
	}

	private function getFilename()
	{
		return $this->file['filename'];
	}

	public function getFull()
	{
		return $this->getPrefix() . $this->getBase();
	}

	public function getPrefix()
	{
		return ltrim(
			str_replace('/', '-', $this->file['path']) . '-', 
			'-'
		);
	}

	public function getOriginal()
	{
		return $this->getBase();
	}

	public function getBase()
	{
		return preg_replace('/\.md$/', '', $this->file['filename']);
	}
}