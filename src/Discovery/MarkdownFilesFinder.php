<?php

namespace App\Discovery;

class MarkdownFilesFinder
{
	private $basePath;

	public function __construct($basePath)
	{
		$this->basePath = $basePath;
	}

	public static function withBasePath($basePath)
	{
		return new static($basePath);
	}

	public function find(string $folder) : array
	{
		return $this->getNestedMarkdownFiles($folder);
	}

	private function getNestedMarkdownFiles(string $folder, array $tree = []) : array
	{
		foreach ($this->getFolders($folder) as $nestedFolder) {
			$tree = $this->getNestedMarkdownFiles(
				$folder . '/' . $nestedFolder, 
				$tree
			);
		}

		foreach ($this->getMarkdownFiles($folder) as $filename) {
			$tree[] = MarkdownSlug::fromFile([
				'path' => $this->removeRootFolder($folder), 
				'filename' => $filename,
			]);
		}

		return $tree;
	}

	private function removeRootFolder($folder) : string
	{
		$parts = explode('/', $folder);
		array_shift($parts);
		return implode('/', $parts);
	}

	private function getFolders($folder) : array
	{
		return array_values(
			array_filter(
				$this->getFiles($folder),
				function ($filename) use ($folder) {
					return $this->isFolder($filename, $folder);
				}
			)
		);
	}

	private function getMarkdownFiles($folder) : array
	{
		return array_values(
			array_filter(
				$this->getFiles($folder), 
				[$this, 'isMarkdownFile']
			)
		);
	}

	public function isFolder($filename, $folder)
	{
		return 
			! in_array($filename, ['.', '..']) && 
			is_dir($this->basePath . '/' . $folder . '/' . $filename);
	}

	public function isMarkdownFile($filename)
	{
		return preg_match('/\.md$/', $filename);
	}

	private function getFiles($folder)
	{
		return scandir($this->basePath . '/' . $folder);
	}
}