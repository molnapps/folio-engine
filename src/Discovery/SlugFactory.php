<?php

namespace App\Discovery;

class SlugFactory
{
	public static function fromRouter($slug, string $path = null)
	{
		if ($path && stripos($path, '.')) {
			throw new \Exception('Invalid path');
		}

		if ($path) {
			return static::fromFile([
				'path' => $path, 
				'filename' => $slug . '.md'
			]);
		}

		return static::fromString($slug);
	}

	public static function fromString($slug)
	{
		return static::normalize($slug);
	}

	public static function fromFile(array $slug)
	{
		return static::normalize($slug);
	}
	
	public static function normalize($slug)
	{
		if ($slug instanceof Slug) {
			return $slug;
		}

		if (is_string($slug)) {
			return BaseSlug::fromString($slug);
		}

		if (is_array($slug)) {
			return MarkdownSlug::fromFile($slug);
		}

		throw new \Exception('Could not create slug.');
	}
}