<?php

namespace App\Themes\Publishers\Manifests;

class FileManifestData
{
	private $filePath;

	public function __construct($filePath)
	{
		$this->filePath = $filePath;
	}

	public static function fromFilePath($filePath)
	{
		return new static($filePath);
	}

	public function get(array $merge = [])
	{
		return array_merge([
			'bytes' => $this->getFileSize(),
			'path' => $this->filePath,
			'format' => $this->getFormat()
		], $merge);
	}

	private function getFileSize()
	{
		return filesize($this->filePath);
	}

	private function getFormat()
	{
		if (is_dir($this->filePath)) {
			return 'folder';
		}
		
		$parts = explode('.', $this->filePath);

		return end($parts);
	}
}