<?php

namespace App\Themes\Publishers\Manifests;

class NullManifestEntry implements ManifestEntry
{
	public function toArray() : array
	{
		return [];
	}

	public function getOutputs() : array
	{
		return [];
	}

	public function isSameChecksum() : bool
	{
		return false;
	}

	public function isUncompressed() : bool
	{
		return false;
	}

	public function isSkipped() : bool
	{
		return true;
	}

	public function inputFileExists() : bool
	{
		return false;
	}

	public function allOutputsExist() : bool
	{
		return false;
	}

	public function toArrayWithConvertedPaths(string $methodName) : array
	{
		return [];
	}
}