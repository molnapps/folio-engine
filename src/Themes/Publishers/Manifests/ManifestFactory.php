<?php

namespace App\Themes\Publishers\Manifests;

class ManifestFactory
{
	public static function images()
    {
        return new Manifest(
            env('MANIFESTS_DESTINATION_PATH') . '/manifest.img.json'
        );
    }

    public static function compressedImages()
    {
        return new Manifest(
            env('MANIFESTS_DESTINATION_PATH') . '/images.compressed.json'
        );
    }

    public static function croppedImages()
    {
        return new CropsManifest(
            env('MANIFESTS_DESTINATION_PATH') . '/manifest.crops.json'
        );
    }

    public static function assets()
    {
        return new Manifest(
            env('MANIFESTS_DESTINATION_PATH') . '/manifest.assets.json'
        );
    }

    public static function websiteFeatureImages()
    {
        return new Manifest(
            env('MANIFESTS_DESTINATION_PATH') . '/manifest.website-features.json'
        );
    }
}