<?php

namespace App\Themes\Publishers\Manifests;

use App\Repositories\FileSystem\PathInspector;

class CropsManifest extends Manifest
{
	public function addFormats($absolutePath, array $formatAbsolutePaths = [])
	{
		$this->addEntry(
			$absolutePath,
			[
				'input' => $this->getImageManifestData($absolutePath),
				'output' => $this->getOutputsManifestData($formatAbsolutePaths)
			]
		);

		return $this;
	}

	private function getOutputsManifestData($allPaths)
	{
		$outputs = array_map(
			[$this, 'getImageManifestData'],
			$allPaths
		);

		return array_values($outputs);
	}

	private function getImageManifestData($absolutePath)
	{
		return ImageManifestData::fromFilePath($absolutePath)->get();
	}
}