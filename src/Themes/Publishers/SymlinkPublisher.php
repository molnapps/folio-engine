<?php

namespace App\Themes\Publishers;

use \App\Generators\FileSystem;
use App\Themes\Publishers\Manifests\Manifest;

class SymlinkPublisher implements Publisher
{
	public static function test()
	{
		return TestSymlinkPublisher::instance();
	}

	public function publish(Manifest $manifest, $source, $destination)
	{
		app(FileSystem::class)->createSymlink($source, $destination);
		
		if (app()->env('testing')) {
			return static::test()->publish($manifest, $source, $destination);
		}
	}
}