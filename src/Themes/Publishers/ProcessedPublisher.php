<?php

namespace App\Themes\Publishers;

use App\Generators\FileSystem;
use App\Themes\Publishers\Manifests\Manifest;
use App\Repositories\FileSystem\PathInspector;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Processors\ProcessFactory;

class ProcessedPublisher implements Publisher
{
	private $onlyUsed = false;
    private $test = false;
    private $manifest;

    private $storagePath;
    private $destinationPath;

    public function __construct($onlyUsed = false)
	{
		$this->onlyUsed = !! $onlyUsed;

        $this->storagePath = env('IMAGES_STORAGE_PATH');
        $this->destinationPath = env('IMAGES_DESTINATION_PATH');
	}

    public function test()
    {
        $this->test = true;

        return $this;
    }

	public function publish(Manifest $manifest, $source, $destination)
	{
        $this->manifest = $manifest->loadIfNotLoaded();

        $this->createDestinationFolderIfDoesNotExist();

        foreach ($this->getProcesses($source) as $process) {
            $this->handle($process);
        }
	}

    private function getProcesses($source)
    {
        if ($this->test) {
            return [
                $this->getTestProcess($source),
            ];
        }

        return [
            $this->getImagesProcess($source),
            $this->getWebsiteFeatureImagesProcess($source),
        ];
    }

    private function getTestProcess($source)
    {
        return ProcessFactory::test($source);
    }

    private function getImagesProcess($source)
    {
        return $this->onlyUsed
            ? ProcessFactory::usedImages($source)
            : ProcessFactory::allImages($source);
    }

    private function getWebsiteFeatureImagesProcess($source)
    {
        return ProcessFactory::websiteFeatureImages($source);
    }

    private function handle($process)
    {
        $provider = $process->process();

        foreach ($provider->getImages() as $sourceImagePath => $processedImages) {
            $outputs = $this->publishImages(
                $sourceImagePath, 
                $processedImages
            );

            $this->addPublishedImagesToManifest(
                $sourceImagePath, 
                $outputs
            );
        }
    }

    private function publishImages($sourceImagePath, $processedImages) : array
    {
        $outputs = [];

        foreach ($processedImages as $processedImagePath) {
            $outputs[] = $this->publishImage(
                $sourceImagePath, 
                $processedImagePath
            );
        }

        return $outputs;
    }

    private function addPublishedImagesToManifest($sourceImagePath, array $outputs)
    {
        $this->manifest->addEntry(
            $sourceImagePath,
            $this->getManifestData($sourceImagePath, $outputs)
        );
    }

    private function getManifestData($sourceImagePath, array $outputs) : array
    {
        return [
            'input' => ImageManifestData::fromFilePath($sourceImagePath)->get(),
            'output' => array_map(
                function ($output) {
                    return ImageManifestData::fromFilePath($output)->get();
                },
                $outputs
            )
        ];
    }

    private function publishImage($sourceImagePath, $processedImagePath) : string
    {
        $destinationImagePath = $this->getImageDestinationPath($processedImagePath);
        
        $this->createQualifiedDestinationFolderIfDoesNotExist($destinationImagePath);
        $this->cloneImageFile($processedImagePath, $destinationImagePath);

        return $destinationImagePath;
    }

    private function createDestinationFolderIfDoesNotExist()
    {
        app(FileSystem::class)->createFolderIfDoesNotExist(
            $this->destinationPath
        );
    }

    private function createQualifiedDestinationFolderIfDoesNotExist($imageDestinationPath)
    {
        app(FileSystem::class)->createFolderIfDoesNotExist(
            PathInspector::fromPath($imageDestinationPath)->getBasePath()
        );
    }

    private function cloneImageFile($processedImagePath, $destinationImagePath)
    {
        app(FileSystem::class)->copyFile(
            $processedImagePath, 
            $destinationImagePath
        );
    }

    private function getImageDestinationPath($processedImagePath)
    {
        return str_replace(
            $this->storagePath,
            $this->destinationPath,
            $processedImagePath
        );
    }
}