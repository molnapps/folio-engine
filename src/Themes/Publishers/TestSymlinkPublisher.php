<?php

namespace App\Themes\Publishers;

use App\Themes\Publishers\Manifests\Manifest;

class TestSymlinkPublisher implements Publisher
{
	private static $instance;
	
	private $published = [];
	
	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function reset()
	{
		static::$instance = null;
	}

	public function publish(Manifest $manifest, $source, $destination)
	{
		if ( ! app()->env('testing')) {
			throw new \Exception('Use this class only in a testing environment!');
		}

		$this->published[] = [$source, $destination];
	}

	public function getPublished()
	{
		return $this->published;
	}

	public function hasPublished($source, $destination)
	{
		return in_array([$source, $destination], $this->published);
	}
}