<?php

namespace App\Themes\Publishers;

use App\Generators\FileSystem;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\FileManifestData;

class CopyPublisher implements Publisher
{
	private $manifest;

	public function publish(Manifest $manifest, $source, $destination)
	{
		$this->manifest = $manifest;
		$this->recursiveCopy($source, $destination);
	}

	private function recursiveCopy($sourcePath, $destinationPath) {
		$this->createDestinationFolderIfDoesNotExist($sourcePath, $destinationPath);
		
		foreach ($this->getFiles($sourcePath) as $fileOrFolder) {
			$fileSourcePath = "{$sourcePath}/{$fileOrFolder}";
			$fileDestinationPath = "{$destinationPath}/{$fileOrFolder}";
			
			if ( $this->isFolder($fileSourcePath) ) {
				$this->recursiveCopy($fileSourcePath, $fileDestinationPath);
			}

			if ($this->isFile($fileSourcePath)) {
				$this->copyFile($fileSourcePath, $fileDestinationPath);
			}
		}
	}

	private function createDestinationFolderIfDoesNotExist($sourcePath, $destinationPath)
	{
		app(FileSystem::class)->createFolderIfDoesNotExist($destinationPath);

		$this->addFileManifestData($sourcePath, $destinationPath);
	}

	private function copyFile($sourcePath, $destinationPath)
	{
		app(FileSystem::class)->copyFile($sourcePath, $destinationPath);
		
		$this->addFileManifestData($sourcePath, $destinationPath);
	}

	private function addFileManifestData($sourcePath, $destinationPath)
	{
		$this->manifest->addEntry($sourcePath, [
			'input' => FileManifestData::fromFilePath($sourcePath)->get(),
			'output' => [
				FileManifestData::fromFilePath($destinationPath)->get()
			]
		]);
	}

	private function isFile($path)
	{
		return ! $this->isFolder($path);
	}

	private function isFolder($path)
	{
		return is_dir($path);
	}

	private function getFiles($path)
	{
		return app(FileSystem::class)->getFilesInFolder($path);
	}
}