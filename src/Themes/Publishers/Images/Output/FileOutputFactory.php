<?php

namespace App\Themes\Publishers\Images\Output;

class FileOutputFactory
{
	private $output;
	private $files = [];
	private $currentFile = null;

	public function __construct(OutputFactory $output)
	{
		$this->output = $output;
	}
	
	public function file($key)
	{
		$this->currentFile = $key;
		$this->files[$this->currentFile] = new OutputMap;

		return $this;
	}

	public function override(array $outputMap)
	{
		$this->files[$this->currentFile]->override($outputMap);

		return $this;
	}

	public function skip()
	{
		$this->files[$this->currentFile]->skip();
		
		return $this;
	}

	public function raw()
	{
		$this->files[$this->currentFile]->raw();
		
		return $this;
	}

	public function compress(array $formats)
	{
		$this->files[$this->currentFile]->compress($formats);

		return $this;
	}

	public function compressAlpha(array $formats)
	{
		$this->files[$this->currentFile]->compressAlpha($formats);

		return $this;
	}

	public function get($key) : OutputMap
	{
		if ($this->has($key)) {
			return $this->files[$key];
		}

		return $this->output->get(
			$this->getExtension($key)
		);
	}

	public function has($key)
	{
		return array_key_exists($key, $this->files);
	}

	private function getExtension($key)
	{
		$parts = explode('.', $key);
		$extension = end($parts);
		return $extension;
	}
}