<?php

namespace App\Themes\Publishers\Images\Crops;

use App\Domain\Images\ImageSizeCalculator;
use App\Repositories\FileSystem\PathInspector;
use App\Themes\Publishers\Images\Crops\CropsImage;
use App\Themes\Publishers\Manifests\CropsManifest;
use App\Themes\Publishers\Images\Sources\SvgCropper;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;
use App\Themes\Publishers\Images\Sources\SourceProviderFactory;

class CroppedImagesCreator
{
	private $sourceImagePath;
	private $destinationPath;
	private $formats = [];
	private $manifest;
	private $map;

	public function __construct($sourceImagePath, CropsManifest $manifest)
	{
		$this->sourceImagePath = PathInspector::fromPath($sourceImagePath);
		$this->manifest = $manifest;
		$this->formats = [
			['suffix' => 'square', 'aspectRatio' => [1, 1]],
			['suffix' => 'portrait', 'aspectRatio' => [4, 5]],
		];
	}

	public function setMap(CroppedImagesMap $map)
	{
		$this->map = $map;

		return $this;
	}

	public function getAllPaths()
	{
		$existingFormats = array_filter(
			$this->formats,
			function ($format) {
				return $this->destinationImageExists($format);
			}
		);

		$pathsDictionary = [];
		foreach ($existingFormats as $format) {
			$pathsDictionary[$format['suffix']] = $this->getDestinationPathForFormat($format);
		}
		
		return $pathsDictionary;
	}

	public static function fromSourceImagePath(
		$sourceImagePath, 
		CropsManifest $manifest = null
	) {
		return new static(
			$sourceImagePath, 
			$manifest ?: ManifestFactory::croppedImages()
		);
	}

	public function usesStorage() : static
	{
		return $this->setDestinationPath(
			PathInspector::fromPath(
				str_replace(
					env('IMAGES_SOURCE_PATH'),
					env('IMAGES_STORAGE_PATH'),
					$this->sourceImagePath->toString()
				)
			)->getBasePath()
		);
	}

	public function usesMap() : static
	{
		return $this->setMap(
			app(CroppedImagesMap::class)
		);
	}

	public function setDestinationPath($destinationPath) : static
	{
		$this->destinationPath = rtrim($destinationPath, '/');

		return $this;
	}

	public function delete()
	{
		if ( ! $this->isSupportedImage()) {
			return;
		}

		foreach ($this->formats as $format) {
			$this->deleteCroppedImage($format);
		}
	}

	public function create()
	{
		if ( ! $this->isSupportedImage()) {
			return;
		}

		if ( ! $this->getFormats()) {
			return;
		}

		foreach ($this->getFormats() as $format) {
			$this->createCroppedImage($format);
		}

		$this->addToManifest();
	}

	private function getFormats() : array
	{
		if ( ! $this->map) {
			return $this->formats;
		}

		return $this->map->getRequestedFormats(
			$this->sourceImagePath->toString(),
			$this->formats
		);
	}

	private function deleteCroppedImage(array $format)
	{
		if ( ! $this->destinationImageExists($format)) {
			return;
		}

		unlink(
			$this->getDestinationPathForFormat($format)
		);
	}

	private function createCroppedImage(array $format)
	{
		if (
			$this->destinationImageExists($format) && 
			! $this->sourceImageWasChanged()
		) {
			return;
		}

		if ( ! $this->sourceImageIsWiderThanFormat($format)) {
			return;
		}

		if (
			$this->customSourceImageIsProvided($format) && 
			$this->customDestinationIsProvided()
		) {
			return $this->cloneCustomSourceImage($format);
		}

		return $this->getCropper()->writeCroppedImage(
			$this->sourceImagePath->toString(), 
			$this->getDestinationPathForFormat($format),
			$this->getCropRectangle($format)
		);
	}

	private function addToManifest()
	{
		$this->manifest->addFormats(
			$this->sourceImagePath->toString(),
			$this->getAllPaths()
		);
	}

	private function cloneCustomSourceImage($format)
	{
		copy(
			$this->getSourcePathForFormat($format), 
			$this->getDestinationPathForFormat($format)
		);
	}

	private function getCropper() : CropsImage
	{
		if ($this->isSvg()) {
			return new SvgCropper(
				$this->sourceImagePath->toString()
			);
		}

		return SourceProviderFactory::fromExtension(
			$this->sourceImagePath->getExtension()
		);
	}

	private function destinationImageExists(array $format)
	{
		return file_exists(
			$this->getDestinationPathForFormat($format)
		);
	}

	private function sourceImageWasChanged()
	{
		return ! $this->manifest
			->getEntry($this->sourceImagePath->toString())
			->isSameChecksum();
	}

	private function sourceImageIsWiderThanFormat(array $format)
	{
		$sourceSize = $this->getSize();

		return $sourceSize['width'] > $this->getCropRectangle($format)['width'];
	}

	private function customSourceImageIsProvided($format) : bool
	{
		return file_exists(
			$this->getSourcePathForFormat($format)
		);
	}

	private function customDestinationIsProvided() : bool
	{
		return $this->getSourceBasePath() != $this->getDestinationBasePath();
	}

	public function getCropRectangle(array $format)
	{
		$sourceSize = $this->getSize();

		$aspectRatioFactor = $format['aspectRatio'][0] / $format['aspectRatio'][1];
		
		$destinationSize = [
			'width' => $sourceSize['height'] * $aspectRatioFactor, 
			'height' => $sourceSize['height']
		];

		$destinationCoords = [
			'x' => ($sourceSize['width'] - $destinationSize['width']) / 2, 
			'y' => ($sourceSize['height'] - $destinationSize['height']) / 2, 
		];

		return array_merge(
			$destinationSize, 
			$destinationCoords
		);
	}

	private function getSourcePathForFormat(array $format)
	{
		return $this->getPathForFormat(
			$this->getSourceBasePath(),
			$format
		);
	}
	
	private function getDestinationPathForFormat(array $format)
	{
		return $this->getPathForFormat(
			$this->getDestinationBasePath(),
			$format
		);
	}

	private function getPathForFormat($basePath, array $format) 
	{
		return sprintf(
			'%s/%s-%s.%s',
			$basePath,
			$this->sourceImagePath->getFileNameWithoutExtension(),
			$format['suffix'],
			$this->sourceImagePath->getExtension()
		);
	}

	private function getSourceBasePath()
	{
		return $this->sourceImagePath->getBasePath();
	}

	private function getDestinationBasePath()
	{
		return $this->destinationPath 
			?: $this->sourceImagePath->getBasePath();
	}

	private function getSize()
	{
		return (new ImageSizeCalculator($this->sourceImagePath->toString()))->get();
	}

	private function isSupportedImage()
	{
		if ($this->isSvg()) {
			return true;
		}

		return SourceProviderFactory::isSupported(
			$this->sourceImagePath->getExtension()
		);
	}

	private function isSvg()
	{
		return $this->sourceImagePath->getExtension() == 'svg';
	}
}