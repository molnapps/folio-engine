<?php

namespace App\Themes\Publishers\Images\Testing;

use org\bovigo\vfs\vfsStream;
use App\Themes\Publishers\ProcessedPublisher;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

trait InteractsWithManifest
{
	protected function getPublishedManifestInstance(Manifest $manifest = null)
	{
		$manifest = $manifest ?: $this->getManifestInstance();

		(new ProcessedPublisher)
			->test()
			->publish(
				$manifest,
				vfsStream::url('folio/contents/images'),
				vfsStream::url('folio/public/images')
			);

		$manifest->publish();
		
		return $manifest;
	}

	protected function getManifestInstance()
	{
		return ManifestFactory::images();
	}

	// Assertions

	protected function assertManifestKey($original, array $manifest)
	{
		$this->assertArrayHasKey($original, $manifest);
	}

	protected function assertManifestInput(
		array $expectedInput, 
		$manifestItem
	) {
		$this->assertArrayHasKey('input', $manifestItem);

		list ($expectedFilePath, $expectedFormat) = $expectedInput;

		$expectedFilePath = $this->isVfsUrl($expectedFilePath)
				? $expectedFilePath
				: vfsStream::url($expectedFilePath);

		$this->assertManifest([
	    	'bytes' => filesize($expectedFilePath), 
	    	'width' => $this->isImageFormat($expectedFormat) 
	    		? getimagesize($expectedFilePath)[0]
	    		: 0,
	    	'height' => $this->isImageFormat($expectedFormat) 
	    		? getimagesize($expectedFilePath)[1]
	    		: 0,
	    	'path' => $expectedFilePath, 
	    	'format' => $expectedFormat
	    ], $manifestItem['input']);
	}

	protected function assertManifestOutput(
		$expectedMethod, 
		array $expectedOutputs, 
		array $manifestItem
	) {
		$this->assertArrayHasKey('output', $manifestItem);

		foreach ($expectedOutputs as $key => $expectedOutput) {
			list ($expectedFilePath, $expectedFormat) = $expectedOutput;

			$expectedFilePath = $this->isVfsUrl($expectedFilePath)
				? $expectedFilePath
				: vfsStream::url($expectedFilePath);
			
			$this->assertManifest([
		    	'method' => $expectedMethod,
		    	'bytes' => filesize($expectedFilePath), 
		    	'width' => $this->isImageFormat($expectedFormat) 
		    		? getimagesize($expectedFilePath)[0]
		    		: 0,
		    	'height' => $this->isImageFormat($expectedFormat) 
		    		? getimagesize($expectedFilePath)[1]
		    		: 0,
		    	'path' => $expectedFilePath, 
		    	'format' => $expectedFormat
		    ], $manifestItem['output'][$key]);
		}
	}

	private function assertManifest(array $expected, array $manifestItem)
	{
		foreach ($expected as $key => $expectedValue) {
			$this->assertArrayHasKey($key, $manifestItem);
	    	$this->assertEquals($expectedValue, $manifestItem[$key]);
		}
	}

	private function isImageFormat($expectedFormat)
	{
		return in_array($expectedFormat, ['jpg', 'png', 'webp', 'gif']);
	}

	private function isVfsUrl($string)
	{
		return substr($string, 0, 6) === 'vfs://';
	}
}