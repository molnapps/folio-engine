<?php

namespace App\Themes\Publishers\Images\Used;

use \App\Portfolio;
use \App\Domain\Post;

class UsedImagesFinder
{
	private $used;
	private $portfolio;
	private $whitelist = [];
	
	public function __construct()
	{
		$this->used = [];
		$this->portfolio = app(Portfolio::class);
		$this->whitelist = new UsedImagesWhitelist;
		
		$this->findAllUsedImages();
	}

	public function whitelist()
	{
		return $this->whitelist;
	}

	public function add($sourcePath)
	{
		$this->used[] = $sourcePath;

		return $this;
	}

	private function findAllUsedImages() {
		$types = ['projects', 'pages'];

		foreach ($types as $type) {
			$this->findUsedImagesForPosts($type);
		}
	}

	private function findUsedImagesForPosts($type) {
		foreach ($this->portfolio->$type()->toArray() as $post) {
			$this->findUsedImagesForPost($post);
		}
	}

	private function findUsedImagesForPost(Post $post) {
		if (! $post->isVisible()) {
			return;
		}

		$post->html()->findUsedImages($this);
		
		foreach ($post->previews as $preview) {
			$preview->findUsedImages($this);
		}
	}

	public function isUsed($imageSourcePath) {
		if ($this->whitelist()->contains($imageSourcePath)) {
			return true;
		}

		return in_array(
			$imageSourcePath, 
			$this->used
		);
	}

	public function getProcessorMap($basePath)
	{
		$map = array_filter(
			$this->used,
			function ($path) use ($basePath) {
				return $this->imageSourcePathStartsWith($path, $basePath);
			}
		);
		
		return array_merge(
			$map,
			$this->whitelist()->getProcessorMap($basePath)
		);
	}

	private function imageSourcePathStartsWith($imageSourcePath, $basePath) 
    {
        return stripos($imageSourcePath, $basePath) === 0;
    }
}