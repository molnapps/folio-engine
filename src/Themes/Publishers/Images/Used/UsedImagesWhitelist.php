<?php

namespace App\Themes\Publishers\Images\Used;

use App\Repositories\FileSystem\FilesCrawler;

class UsedImagesWhitelist
{
	private $whitelist = [];
	private $baseSourcePath;

	public function __construct()
	{
		$this->baseSourcePath = env('IMAGES_SOURCE_PATH');
	}

	public function set(array $whitelist)
	{
		$this->whitelist = array_map(
			[$this, 'prependBaseSourcePath'], 
			$whitelist
		);
	}

	public function contains($imageSourcePath) : bool
	{
		foreach ($this->whitelist as $basePath) {
			if ($this->pathStartsWithBasePath($imageSourcePath, $basePath)) {
				return true;
			}
		}

		return false;
	}

	public function getProcessorMap($basePath)
	{
		$whitelist = array_filter(
			$this->whitelist,
			function ($path) use ($basePath) {
				return $this->pathStartsWithBasePath($path, $basePath);
			}
		);

		$files = array_filter($whitelist, 'is_file');
		$folders = array_filter($whitelist, 'is_dir');

		foreach ($folders as $path) {
			$files = [
				...$files,
				...FilesCrawler::fromPath($path)->getAllFiles()
			];
		}

		return $files;
	}

    private function pathStartsWithBasePath($path, $basePath) 
    {
        return stripos($path, $basePath) === 0;
    }

    private function prependBaseSourcePath($relativePath)
    {
        return $this->baseSourcePath . '/' . trim($relativePath, '/');
    }
}