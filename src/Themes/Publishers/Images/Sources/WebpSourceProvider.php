<?php

namespace App\Themes\Publishers\Images\Sources;

class WebpSourceProvider extends SourceProvider
{
	protected function createImage($sourcePath)
	{
		return imagecreatefromwebp($sourcePath);
	}

	protected function writeImage(\GDImage $destinationImage, $destinationPath)
	{
		return imagewebp($destinationImage, $destinationPath);
	}
}