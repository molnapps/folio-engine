<?php

namespace App\Themes\Publishers\Images\Sources;

use App\Themes\Publishers\Images\Crops\CropsImage;

abstract class SourceProvider implements CropsImage
{
	private $sourceImages = [];

	public function getSourceImage($sourcePath) : \GDImage
	{
		if ( ! $this->hasSourceImage($sourcePath)) {
			$this->sourceImages[$sourcePath] = $this->createImage($sourcePath);
		}
		
		return $this->sourceImages[$sourcePath];
	}

	public function writeCroppedImage($sourcePath, $destinationPath, array $cropRectangle)
	{
		$sourceImage = $this->getSourceImage($sourcePath);
		$destinationImage = $this->getCroppedImage($sourceImage, $cropRectangle);
		
		if ( ! $this->cropWasSuccessful($destinationImage)) {
			return;
		}

		$this->writeImage($destinationImage, $destinationPath);
		$this->destroyImage($sourceImage);
		$this->destroyImage($destinationImage);
	}

	public function writeCopyResampledImage(
		$sourcePath, 
		$destinationPath, 
		array $sourceRect, 
		array $destinationRect
	) {
		$sourceImage = $this->createImage($sourcePath);
        
		$destinationImage = $this->getCopyResampledImage(
			$sourceImage, 
			$sourceRect, 
			$destinationRect
		);

        $this->writeImage(
            $destinationImage, 
            $destinationPath
        );

        $this->destroyImage($sourceImage);
        $this->destroyImage($destinationImage);
	}

	abstract protected function createImage($sourcePath);
	abstract protected function writeImage(\GDImage $destinationImage, $destinationPath);

	private function getCopyResampledImage(
		\GDImage $sourceImage, 
		array $sourceRect, 
		array $destinationRect
	) : \GDImage
	{
		$destinationImage = imagecreatetruecolor(
            $destinationRect['width'], 
            $destinationRect['height']
        );

        imagecopyresampled(
            $destinationImage, 
            $sourceImage, 
            $destinationRect['x'], 
            $destinationRect['y'], 
            $sourceRect['x'], 
            $sourceRect['y'], 
            $destinationRect['width'], 
            $destinationRect['height'], 
            $sourceRect['width'], 
            $sourceRect['height']
        );

		return $destinationImage;
	}

	private function getCroppedImage(\GDImage $image, $rectangle) : \GDImage
	{
		return imagecrop($image, $rectangle);
	}

	private function destroyImage(\GDImage $image)
	{
		return imagedestroy($image);
	}

	private function hasSourceImage($sourcePath)
	{
		return array_key_exists($sourcePath, $this->sourceImages);
	}

	private function cropWasSuccessful(\GDImage $destinationImage)
	{
		return $destinationImage !== false;
	}
}