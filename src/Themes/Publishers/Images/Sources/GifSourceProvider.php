<?php

namespace App\Themes\Publishers\Images\Sources;

class GifSourceProvider extends SourceProvider
{
	protected function createImage($sourcePath)
	{
		return imagecreatefromgif($sourcePath);
	}

	protected function writeImage(\GDImage $destinationImage, $destinationPath)
	{
		return imagegif($destinationImage, $destinationPath);
	}
}