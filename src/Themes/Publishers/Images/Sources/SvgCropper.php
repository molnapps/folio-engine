<?php

namespace App\Themes\Publishers\Images\Sources;

use Exception;
use SimpleXMLElement;
use App\Themes\Publishers\Images\Crops\CropsImage;

class SvgCropper implements CropsImage
{
	private $simpleXml;

	public function writeCroppedImage(
		$sourceImagePath, 
		$destinationImagePath, 
		array $cropRectangle
	) {
		return file_put_contents(
			$destinationImagePath,
			$this->getCroppedSvg($sourceImagePath, $cropRectangle)
		);
	}

	private function getCroppedSvg($sourceImagePath, array $cropRectangle)
	{
		$this->simpleXml = new SimpleXMLElement(
			file_get_contents($sourceImagePath)
		);

		$this->guardViewBoxAttribute();

		$this->addAttributeIfNotSet('width');
		$this->addAttributeIfNotSet('height');

		$this->updateAttributes($cropRectangle);
		
		return $this->simpleXml->asXml();
	}

	private function guardViewBoxAttribute()
	{
		if ( ! $this->hasAttribute('viewBox')) {
			throw new Exception('<svg> tag requires attribute viewBox.');
		}
	}

	private function addAttributeIfNotSet($attribute)
	{
		list ($x, $y, $width, $height) = explode(' ', $this->simpleXml->attributes()['viewBox']);
		
		if ( ! $this->hasAttribute($attribute)) {
			$this->simpleXml->addAttribute($attribute, $$attribute);
		}
	}

	private function updateAttributes(array $cropRectangle)
	{
		$this->simpleXml->attributes()['width'] = $cropRectangle['width'];
		$this->simpleXml->attributes()['height'] = $cropRectangle['height'];
		$this->simpleXml->attributes()['viewBox'] = $this->getViewBoxValue($cropRectangle);
	}

	private function getViewBoxValue(array $cropRectangle)
	{
		return implode(' ', [
			$cropRectangle['x'],
			$cropRectangle['y'],
			$cropRectangle['width'],
			$cropRectangle['height']
		]);
	}

	private function hasAttribute($attribute) : bool
	{
		return isset($this->simpleXml->attributes()[$attribute]);
	}
}