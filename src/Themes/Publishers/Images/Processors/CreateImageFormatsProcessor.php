<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Crops\CroppedImagesCreator;

class CreateImageFormatsProcessor implements Processor, ProcessorProvider
{
    private $provider;
    private $manifest;
    
    private $images = [];

    public function process(ProcessorProvider $provider) : ProcessorProvider
    {
        $this->manifest = ManifestFactory::croppedImages();

        $this->manifest->loadIfNotLoaded();

        foreach ($provider->getImages() as $images) {
            foreach ($images as $image) {
                $formats = $this->createFormats($image);
                $this->addImages($image, $formats);
            }
        }

        $this->manifest->publish();

        return $this;
    }

    public function getImages() : array
    {
        return $this->images;
    }

    public function createFormats($absoluteSourcePath) : array
    {
        $croppedImagesCreator = $this->getCroppedImagesCreatorInstance($absoluteSourcePath);
        
        $croppedImagesCreator->create();

        return array_values(
            $croppedImagesCreator->getAllPaths()
        );
    }

    private function addImages($absoluteSourcePath, $formats)
    {
        $this->images[$absoluteSourcePath] = [
            $absoluteSourcePath,
            ...$formats
        ];
    }

    private function getCroppedImagesCreatorInstance($absoluteSourcePath)
    {
        return CroppedImagesCreator::fromSourceImagePath(
                $absoluteSourcePath, 
                $this->manifest
            )
            ->usesStorage()
            ->usesMap();
    }
}