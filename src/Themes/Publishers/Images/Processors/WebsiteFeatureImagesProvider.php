<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Portfolio;
use App\Domain\Images\Image;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;

class WebsiteFeatureImagesProvider implements ProcessorProvider
{
    private $sourcePath;

    public function __construct($sourcePath = null)
    {
        $sourcePath = $sourcePath ?: env('IMAGES_SOURCE_PATH');

        $this->sourcePath = $sourcePath;
    }

    public function getImages() : array
    {
        $map = [];

        foreach ($this->getAllWebsiteFeatureImages() as $path) {
            $map[$path] = [ $path ];
        }

        return $map;
    }

    private function getAllWebsiteFeatureImages() : array
    {
        $images = [];

        $images = $this->addPosts($this->getVisibleProjects(), $images);
        $images = $this->addPosts($this->getVisiblePages(), $images);

        return $images;
    }

    private function addPosts($posts, array $images)
    {
        foreach ($posts as $post) {
            if ( ! $this->shouldInclude($post->websiteFeature)) {
                continue;
            }

            $images[] = $post->websiteFeature->getSourcePath();
        }

        return $images;
    }

    private function getVisibleProjects()
    {
        return app(Portfolio::class)->projects()->visibleOnly();
    }

    private function getVisiblePages()
    {
        return app(Portfolio::class)->pages()->visibleOnly();
    }

    private function shouldInclude(Image $image)
    {
        return $image->exists() && $this->isSpecifiedPath($image);
    }

    private function isSpecifiedPath(Image $image)
    {
        return stripos($image->getSourcePath(), $this->sourcePath) === 0;
    }
}