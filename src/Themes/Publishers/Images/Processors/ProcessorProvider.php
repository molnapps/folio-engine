<?php

namespace App\Themes\Publishers\Images\Processors;

interface ProcessorProvider {
    public function getImages() : array;
}