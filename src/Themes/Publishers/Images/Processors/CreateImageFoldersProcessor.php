<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Repositories\FileSystem\PathInspector;

class CreateImageFoldersProcessor implements Processor, ProcessorProvider
{
    private $provider;

    public function process(ProcessorProvider $provider) : ProcessorProvider
    {
        $this->provider = $provider;

        foreach ($this->provider->getImages() as $images) {
            foreach ($images as $image) {
                $this->createEnclosingFolderIfDoesNotExist($image);
            }
        }
        
        return $this->provider;
    }

    public function getImages() : array
    {
        return $this->provider->getImages();
    }

    public function createEnclosingFolderIfDoesNotExist($imageSourcePath)
    {
        if ( ! $this->destinationBasePathExists($imageSourcePath)) {
            $this->createDestinationBasePath($imageSourcePath);
        }
    }

    private function destinationBasePathExists($imageSourcePath)
    {
        return file_exists(
            $this->getDestinationBasePath($imageSourcePath)
        );
    }

    private function createDestinationBasePath($imageSourcePath)
    {
        mkdir(
            $this->getDestinationBasePath($imageSourcePath)
        );
    }

    private function getDestinationBasePath($imageSourcePath)
    {
        return PathInspector::fromPath(
            $this->getFileDestinationPath($imageSourcePath)
        )->getBasePath();
    }

    private function getFileDestinationPath($imageSourcePath)
    {
        return str_replace(
            $this->getSourcePath(),
            $this->getDestinationPath(),
            $imageSourcePath
        );
    }

    private function getSourcePath()
    {
        return env('IMAGES_SOURCE_PATH');
    }

    private function getDestinationPath()
    {
        return env('IMAGES_STORAGE_PATH');
    }
}