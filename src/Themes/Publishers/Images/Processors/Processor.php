<?php

namespace App\Themes\Publishers\Images\Processors;

interface Processor {
    public function process(ProcessorProvider $provider) : ProcessorProvider;
}