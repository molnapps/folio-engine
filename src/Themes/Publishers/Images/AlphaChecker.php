<?php

namespace App\Themes\Publishers\Images;

class AlphaChecker
{
	private $source;
	private $sourceExtension;

	public function __construct(\GDImage $source, $sourceExtension)
	{
		$this->source = $source;
		$this->sourceExtension = $sourceExtension;
	}

	public function hasAlpha()
	{
		$hasAlpha = false;
		
		if ($this->sourceCouldContainAlpha()) {
			$hasAlpha = $this->searchAlphaPixelByPixel($this->source);
		}

		if ($hasAlpha) {
			$this->enableAlphaOnSource();
		}

		return $hasAlpha;
	}

	private function sourceCouldContainAlpha()
	{
		return in_array(
			$this->sourceExtension, 
			$this->getImageFormatsSupportingAlpha()
		);
	}

	private function enableAlphaOnSource()
	{
		imagesavealpha($this->source, true);
	}

	private function getImageFormatsSupportingAlpha()
	{
		return ['png'];
	}

	private function searchAlphaPixelByPixel($source)
	{
		$width = imagesx($source);
		$height = imagesy($source);

	    for($i = 0; $i < $width; $i++) {
	        for($j = 0; $j < $height; $j++) {
	            $rgba = imagecolorat($source, $i, $j);
	            if (($rgba & 0x7F000000) >> 24) {
	                return true;
	            }
	        }
	    }

	    return false;
	}
}