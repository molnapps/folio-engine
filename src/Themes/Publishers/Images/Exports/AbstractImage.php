<?php

namespace App\Themes\Publishers\Images\Exports;

use App\Themes\Publishers\Images\Options\Options;

abstract class AbstractImage
{
	private $options = null;
	
	public function __construct(Options $options)
	{
		$this->options = $options;
	}

	public static function fromOptions(Options $options)
	{
		return new static($options);
	}

	public function create(ExportData $exportData, $destinationPath)
	{
		$this->getCreateMethod()(
			$exportData->source, 
			$destinationPath, 
			$this->getQuality($exportData->sourcePath)
		);
	}

	protected function getQuality($key)
	{
		return $this->options->get($key)['quality'];
	}

	abstract protected function getCreateMethod();
	abstract public function getExtension();
}