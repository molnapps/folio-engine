<?php

namespace App\Themes\Publishers\Images\Exports;

class ExportData
{
	public $source;
	public $sourcePath;
	public $sourceExtension;
	public $destinationPath;

	public function __construct($source, $sourceExtension, $sourcePath, $destinationPath)
	{
		$this->source = $source;
		$this->sourcePath = $sourcePath;
		$this->sourceExtension = $sourceExtension;
		$this->destinationPath = $destinationPath;
	}
}