<?php

namespace App\Themes\Publishers\Images\Exports;

class ErrorHandler
{
	private $errors = [];

	public function replace()
	{
		set_error_handler([$this, 'handle'], E_ALL);
	}

	public function reset()
	{
		restore_error_handler();
	}

	public function handle($errorNumber, $errorString)
	{
		$this->errors[] = $errorString;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}