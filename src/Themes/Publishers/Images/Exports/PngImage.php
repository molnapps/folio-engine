<?php

namespace App\Themes\Publishers\Images\Exports;

class PngImage extends AbstractImage
{
	protected function getCreateMethod()
	{
		return 'imagepng';
	}

	public function getExtension()
	{
		return 'png';
	}

	protected function getQuality($key)
	{
		// PNG Quality is 0 (no compression) to 9
		$optionsQuality = parent::getQuality($key);
		return 9 - ($optionsQuality / 10);
	}
}