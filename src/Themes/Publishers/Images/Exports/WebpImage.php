<?php

namespace App\Themes\Publishers\Images\Exports;

class WebpImage extends AbstractImage
{
	protected function getCreateMethod()
	{
		return 'imagewebp';
	}

	public function getExtension()
	{
		return 'webp';
	}
}