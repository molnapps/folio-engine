<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Images\Options\Options;

interface ImagePublisher
{
	public function overwrite(bool $overwrite) : static;
	public function publish(Options $options, Manifest $manifest);
}