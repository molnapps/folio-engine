<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Images\Output\OutputFactory;
use App\Themes\Publishers\Images\Output\OutputMap;

class ImagePublisherFactory
{
	private static $test = false;

	private $sourcePath;
	private $destinationPath;

	private function __construct($sourcePath, $destinationPath)
	{
		$this->sourcePath = $sourcePath;
		$this->destinationPath = $destinationPath;
	}

	public static function fromOutput($sourcePath, $destinationPath, OutputFactory $output)
	{
		return (new static($sourcePath, $destinationPath))->createFromOutput($output);
	}

	public static function test()
	{
		static::$test = true;
	}

	public static function reset()
	{
		static::$test = false;
	}

	private function createFromOutput(OutputFactory $output)
	{
		return $this->createFromOutputMap(
			$output->files()->get($this->sourcePath)
		);
	}

	private function createFromOutputMap(OutputMap $outputMap)
	{
		if ($this->isTestingEnvironment()) {
			return new Testing\TestImagePublisher($this->sourcePath);
		}

		switch ($outputMap->getMethod())
		{
			case 'compressed':
				return new CompressedImagePublisher(
					$this->sourcePath, 
					$this->destinationPath, 
					$this->getTypeFromExtension()
				);
			case 'uncompressed':
				return new UncompressedImagePublisher(
					$this->sourcePath, 
					$this->destinationPath
				);
			case 'skip':
				return new SkippedFilePublisher($this->sourcePath);
			default:
				throw new \Exception("Unknown method [{$outputMap->getMethod()}]");
		}
	}

	private function isTestingEnvironment()
	{
		return static::$test;
	}

	private function getTypeFromExtension()
	{
		$parts = explode('.', $this->sourcePath);
		$extension = end($parts);
		return $extension;
	}
}