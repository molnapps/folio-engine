<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Images\Options\Options;
use App\Themes\Publishers\Images\Exports\ExportData;
use App\Themes\Publishers\Images\Output\OutputFactory;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Exports\AbstractImage;
use App\Themes\Publishers\Images\Exports\ExportedImage;
use App\Themes\Publishers\Images\Sources\SourceProviderFactory;

class CompressedImagePublisher implements ImagePublisher
{
	private $sourceProvider;
	private $sourcePath;
	private $destinationPath;
	private $sourceExtension;

	private $overwrite = false;

	public function __construct($sourcePath, $destinationPath, $sourceExtension)
	{
		$this->sourcePath = $sourcePath;
		$this->destinationPath = $destinationPath;
		$this->sourceExtension = $sourceExtension;
	}

	public function overwrite(bool $overwrite) : static
	{
		$this->overwrite = true;

		return $this;
	}

	public function publish(Options $options, Manifest $manifest)
	{
		$source = $this->getSourceProviderInstance()->getSourceImage($this->sourcePath);

		$this->addImageManifestData(
			$manifest, 
			$this->createExportedFiles($source, $options)
		);

		imagedestroy($source);
	}

	private function addImageManifestData(Manifest $manifest, array $outputManifestData)
	{
		$manifest->addEntry($this->sourcePath, [
			'input' => ImageManifestData::fromFilePath($this->sourcePath)->get(),
			'output' => $outputManifestData
		]);
	}

	private function createExportedFiles($source, Options $options)
	{
		$exportData = new ExportData(
			$source, 
			$this->sourceExtension, 
			$this->sourcePath,
			$this->destinationPath
		);

		$outputManifestData = [];

		foreach ($this->getExports($source, $options) as $exporter) {
			$outputManifestData[] = $exporter
				->overwrite($this->overwrite)
				->create($exportData);
		}

		return $outputManifestData;
	}

	private function getExports(\GDImage $source, Options $options)
	{
		return array_map(
			function ($format) use ($options) {
				return $this->getExportInstance($format, $options);
			}, 
			$this->getExportFormats($source, $options->output())
		);
	}

	private function getExportInstance($imageOrFormat, Options $options) : ExportedImage
	{
		return new ExportedImage(
			$this->normalizeImageOrFormat($imageOrFormat, $options)
		);
	}

	private function normalizeImageOrFormat($imageOrFormat, Options $options) : AbstractImage
	{
		if (is_string($imageOrFormat)) {
			$imageOrFormat = $this->getImageInstanceFromString($imageOrFormat, $options);
		}
		
		return $imageOrFormat;
	}

	private function getImageInstanceFromString($format, Options $options) : AbstractImage
	{
		$className = sprintf(
			'App\Themes\Publishers\Images\Exports\%sImage',
			ucfirst($format)
		);

		return $className::fromOptions($options);
	}

	private function getExportFormats(\GDImage $source, OutputFactory $output)
	{
		$alphaChecker = new AlphaChecker($source, $this->sourceExtension);
		
		return $output
			->files()
			->get($this->sourcePath)
			->getExports(
				$alphaChecker->hasAlpha()
			);
	}

	private function getSourceProviderInstance()
	{
		return SourceProviderFactory::fromExtension($this->sourceExtension);
	}
}