<?php

namespace App\Themes\Publishers\Images\Options;

class OutputLoader
{
	private $config;
	
	public function __construct(array $config)
	{
		$this->config = $config;
	}

	public static function fromArray(array $config)
	{
		return new static($config);
	}

	public function load(Options $options)
	{
		foreach ($this->config as $file => $override) {
			if ( ! $this->providesOutput($override)) {
				return;
			}

			$options
				->output()
				->files()
				->file($file)
				->override(
					$this->getOutputKeys($override)
				);
		}
	}

	private function providesOutput(array $override)
	{
		return array_key_exists('output', $override);
	}

	private function getOutputKeys(array $override)
	{
		return $override['output'];
	}
}