<?php

namespace App\Themes\Publishers;

use App\Themes\Publishers\Manifests\Manifest;

class BackupPublisher implements Publisher
{
	private $delegate;
	
	public function __construct(Publisher $delegate)
	{
		$this->delegate = $delegate;
	}

	public function publish(Manifest $manifest, $source, $destination)
	{
		$backup = new Backup($manifest, $source, $destination);
		$backup->create();

		try {
			$this->delegate->publish($manifest, $source, $destination);
			$backup->delete();
		} catch (\Exception $e) {
			$backup->restore();
		}
	}
}