<?php

namespace App\Themes\Publishers;

use App\Themes\Publishers\Manifests\Manifest;
use App\Repositories\FileSystem\PathInspector;

class BackupEraser
{
	private $sourcePath;
	private $destinationPath;
	private $backupDestinationPath;
	private $backupManifest;

	public function __construct(
		$sourcePath,
		$destinationPath,
		$backupDestinationPath,
		Manifest $manifest = null
	) {
		$this->sourcePath = $sourcePath;
		$this->destinationPath = $destinationPath;
		$this->backupDestinationPath = $backupDestinationPath;
		$this->backupManifest = $manifest;
	}

	public function erase() {
		if ( ! $this->backupManifest) {
			return;
		}

		$this->backupManifest->loadIfNotLoaded();

		$this->deleteFiles();
		$this->deleteFolders();
		$this->deleteBackupManifestFile();
		$this->deleteBackupFolder();
	}

	private function deleteFiles()
	{
		foreach ($this->getBackupFilePathsToDelete() as $file) {
			$this->deleteFile($file);
		}
	}

	private function deleteFolders()
	{
		foreach ($this->getBackupFolderPathsToDelete() as $folder) {
			$this->deleteFolder($folder);
		}
	}

	private function deleteBackupManifestFile()
	{
		$this->deleteFile(
			$this->backupManifest->getFilePath()
		);
	}

	private function deleteBackupFolder()
	{
		$this->deleteFolder(
			$this->backupDestinationPath
		);
	}

	private function deleteFolder($path)
	{
		if ( ! is_dir($path)) {
			return;
		}

		$this->deleteDSStoreFileIfExists($path);
		
		return rmdir($path);
	}

	private function deleteDSStoreFileIfExists($folderPath)
	{
		$dsStorePath = $folderPath . '/.DS_Store';

		return $this->deleteFile($dsStorePath);
	}

	private function deleteFile($path)
	{
		if ( ! is_file($path)) {
			return;
		}

		return unlink($path);
	}

	public function getBackupFolderPathsToDelete()
	{
		$folders = array_map(
			function ($file) {
				return PathInspector::fromPath($file)->getBasePath();
			},
			$this->getBackupFilePathsToDelete()
		);
		
		$folders = array_unique(
			$folders
		);

		$folders = array_filter(
			$folders,
			function ($folder) {
				return $folder != $this->backupDestinationPath;
			}
		);

		return array_values($folders);
	}

	private function getBackupFilePathsToDelete()
	{
		return array_map(
			[$this, 'getBackupDestinationPathForFile'], 
			$this->getOriginalDestinationFilePaths()
		);
	}

	private function getBackupDestinationPathForFile($filePath)
	{
		return str_replace(
			$this->destinationPath, 
			$this->backupDestinationPath, 
			$filePath
		);
	}

	private function getOriginalDestinationFilePaths()
	{
		$keysToDelete = $this->getKeysToDelete();

		$filePathsToDelete = [];

		foreach ($this->backupManifest->toArray() as $key => $data) {
			if ( ! in_array($key, $keysToDelete)) {
				continue;
			}

			foreach ($data['output'] as $output) {
				$filePathsToDelete[] = $output['path'];
			}
		}

		return $filePathsToDelete;
	}

	private function getKeysToDelete()
	{
		$result = array_filter(
			$this->getAllSourcePaths(),
			[$this, 'shouldBeDeleted']
		);

		return array_values($result);
	}

	private function getAllSourcePaths()
	{
		return array_keys($this->backupManifest->toArray());
	}

	private function shouldBeDeleted($key)
	{
		if ( ! $this->isRelativePath()) {
			return true;
		}

		return $this->startsWithRelativePath($key);
	}

	private function startsWithRelativePath($key)
	{
		return stripos(
			$this->getRelativePath($key), 
			$this->getRelativePath($this->sourcePath)
		) === 0;
	}

	private function isRelativePath()
	{
		return !! $this->getRelativePath($this->sourcePath);
	}

	private function getRelativePath($absolutePath)
	{
		return str_replace(
			$this->getBasePaths(),
			'',
			$absolutePath
		);
	}

	private function getBasePaths()
	{
		return [
			env('IMAGES_SOURCE_PATH'), 
			env('IMAGES_STORAGE_PATH')
		];
	}
}