<?php

namespace App\Themes\Publishers;

use App\Themes\Publishers\Manifests\Manifest;

interface Publisher
{
	public function publish(Manifest $manifest, $source, $destination);
}