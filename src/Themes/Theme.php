<?php

namespace App\Themes;

use App\Context;
use App\Domain\Post;
use App\Http\Response;
use App\Portfolio;
use App\Themes\Placeholders\PlaceholderFiller;

class Theme
{
	private $context;
	private $portfolio;

	public function __construct(Portfolio $portfolio, Context $context)
	{
		$this->context = $context;
		$this->portfolio = $portfolio;
	}

	public function getHomeResponse() : Response
	{
		return view('home', [
			'context' => $this->context,
			'post' => $this->portfolio->getHomePost(),
			'projects' => $this->portfolio->getGallery()->toArray(),
			'pages' => $this->portfolio->getMenu()->toArray(),
			'links' => $this->portfolio->getLinks(),
			'settings' => $this->portfolio->settings(),
			'utilities' => (object)$this->getUtilitiesDictionary(),
		]);
	}

	public function getPostResponse(Post $post) : Response
	{
		return view($post->layout, [
			'context' => $this->context,
			'post' => $post,
			'projects' => $this->portfolio->getGallery()->toArray(),
			'pages' => $this->portfolio->getMenu()->toArray(),
			'links' => $this->portfolio->getLinks(),
			'settings' => $this->portfolio->settings(),
			'utilities' => (object)$this->getUtilitiesDictionary(),
		]);
	}

	public function getNotFoundResponse() : Response
	{
		return view('404', [
			'context' => $this->context,
			'pages' => $this->portfolio->getMenu()->toArray(),
			'links' => $this->portfolio->getLinks(),
			'settings' => $this->portfolio->settings(),
			'utilities' => (object)$this->getUtilitiesDictionary(),
		]);
	}

	public function getUtilitiesDictionary()
	{
		return [
			'baseUrl' => env('BASE_URL'),
			'year' => date('Y'),
		];
	}
}