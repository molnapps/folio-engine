<?php

namespace App\Domain\Previews;

use App\Domain\Video;
use App\Domain\Preview;
use App\Domain\ImageFactory;

class PreviewFactory
{
    private $preview;

	public function __construct(Preview $preview)
    {
        $this->preview = $preview;
    }

    public function get()
    {
        if ($this->preview->isType('image')) {
			return new ImagePreview(
                ImageFactory::makeResponsive($this->preview->src)
            );
		}

		if ($this->preview->isType('video')) {
			return new VideoPreview(
                Video::fromUrl($this->preview->src)
            );
		}

		return new ImagePreview(
            ImageFactory::makeNull()
        );
    }
}