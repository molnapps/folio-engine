<?php

namespace App\Domain;

use App\Themes\Publishers\Manifests\Manifest;

class ImageFactory
{
	private $image;

	public static function makeResponsive(string|null $relativePathOrUrl)
	{
		return new Images\ResponsiveImage(
			app(Manifest::class)->loadIfNotLoaded(),
			static::make($relativePathOrUrl)
		);
	}

	public static function makeNull($relativePathOrUrl = null) : Images\Image
	{
		return new Images\NullImage($relativePathOrUrl);
	}

	public static function make(string|null $relativePathOrUrl) : Images\Image
	{
		$relativePathOrUrl = (string)$relativePathOrUrl;
		
		if (static::isUrl($relativePathOrUrl)) {
			return new Images\RemoteImage($relativePathOrUrl);
		}

		if (static::isAbsolutePath($relativePathOrUrl)) {
			return new Images\AbsoluteImage($relativePathOrUrl);
		}

		return new Images\LocalImage($relativePathOrUrl);
	}

	private static function isAbsolutePath(string $string)
	{
		return $string && $string[0] === '~';
	}

	public static function isUrl(string $string)
	{
		return stripos($string, 'https://') !== false || stripos($string, 'http://') !== false;
	}
}