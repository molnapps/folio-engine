<?php

namespace App\Domain;

use App\Resources\PreviewResource;
use App\Resources\ProvidesApiResource;
use App\Domain\Previews\PreviewFactory;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

class Preview extends DomainObject implements 
	ProvidesApiResource,
	UsedImagesProvider
{
	public function apiResource()
	{
		return new PreviewResource($this);
	}

	public function isMedia($media)
	{
		return in_array($media, $this->media);
	}

	public function isType($type)
	{
		return $this->type == $type;
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		$resource = $this->getResource();

		if ( ! $resource instanceof UsedImagesProvider) {
			return;
		}

		$resource->findUsedImages($usedImagesFinder);
	}

	public function getResource()
	{
		return (new PreviewFactory($this))->get();
	}
}