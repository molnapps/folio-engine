<?php

namespace App\Domain;

use Carbon\Carbon;
use App\Views\Content;
use App\Repositories\Registry;
use App\Resources\PostResource;
use App\Views\OpenGraph\OpenGraph;
use App\Repositories\TextCollection;
use App\Repositories\VideoCollection;
use App\Resources\ProvidesApiResource;
use App\Views\OpenGraph\PostOpenGraph;
use App\Repositories\PreviewCollection;

class Post extends DomainObject implements ProvidesApiResource
{
	public function apiResource()
	{
		return new PostResource($this);
	}
	
	public function shouldRender()
	{
		return ! $this->has('disableRender') || ! $this->disableRender;
	}

	public function isVisible()
	{
		return $this->isPublished();
	}

	public function isPublished()
	{
		return $this->has('published_at') && 
			Carbon::create($this->published_at)->isBefore(Carbon::now());
	}

	public function getDate()
	{
		return $this->isPublished()
			? Carbon::create($this->published_at) 
			: Carbon::now();
	}

	public function exists()
	{
		return !! $this->exists;
	}

	public function html()
	{
		return new Content($this);
	}

	public function getTextsAttribute($value)
	{
		return new TextCollection($value);
	}

	public function getExcerptAttribute($value)
	{
		return $this->texts->has('excerpt')
			? $this->texts->get('excerpt')
			: $value;
	}

	public function getPreviewAttribute($value) : Images\Image
	{
		return $this->getFirstPreviewImageWithMedia('preview');
	}

	public function getHeroAttribute($value) : Images\Image
	{
		return $this->getFirstPreviewImageWithMedia('hero');
	}

	public function getWebsiteFeatureAttribute($value) : Images\Image
	{
		$featureImage = $this->getFirstPreviewImageWithMedia('website-feature');

		if ( ! $featureImage->exists()) {
			return $this->hero;
		}

		return $featureImage;
	}

	public function getOpenGraph() : OpenGraph
	{
		return PostOpenGraph::fromPost($this);
	}

	private function getFirstPreviewImageWithMedia(string $media) : Images\Image
	{
		return $this->previews
			->filterByType('image')
			->filterByMedia($media)
			->getFirst()
			->getResource()
			->getImage();
	}

	public function getPreviewsAttribute($value)
	{
		return new PreviewCollection($value);
	}

	public function getVideosAttribute($value)
	{
		return new VideoCollection($value);
	}

	public function getClientAttribute($value)
	{
		if ( ! $value) {
			return $value;
		}

		return app(Registry::class)
			->clients()
			->findBySlug($value);
	}
}