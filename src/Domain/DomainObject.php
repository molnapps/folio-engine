<?php

namespace App\Domain;

use Illuminate\Contracts\Support\Arrayable;

class DomainObject implements Arrayable
{
	protected $attributes = [];

	public function __construct(array $attributes = [])
	{
		$this->load($attributes);
	}

	public function load(array $attributes)
	{
		$this->attributes = $attributes;

		return $this;
	}

	public function set($property, $value)
	{
		$this->attributes[$property] = $value;
		
		return $this;
	}

	public function has($property)
	{
		return array_key_exists($property, $this->attributes) && $this->attributes[$property];
	}

	public function toArray()
	{
		return $this->attributes;
	}

	public function __get($property)
	{
		return $this->get($property);
	}

	public function __set($property, $value)
	{
		return $this->set($property, $value);
	}

	private function get($property)
	{
		$value = $this->has($property) 
			? $this->attributes[$property]
			: null;

		$methodName = sprintf('get%sAttribute', ucfirst($property));
		
		if (method_exists($this, $methodName)) {
			return $this->$methodName($value);
		}
		
		return $value;
	}
}