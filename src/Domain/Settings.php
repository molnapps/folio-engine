<?php

namespace App\Domain;

use App\Resources\ImagePathResource;
use App\Resources\ProvidesApiResource;
use App\Resources\SettingsResource;

class Settings extends DomainObject implements ProvidesApiResource
{
	public function apiResource()
	{
		return new SettingsResource($this);
	}

	public function load(array $settings)
	{
		$settings = ImagePathResource::hydrate($settings)->get();

		parent::load($settings);
		
		if ($this->has('keywords')) {
			$this->set('keywords', implode(', ', $this->keywords));
		}

		return $this;
	}
}