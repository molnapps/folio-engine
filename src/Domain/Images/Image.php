<?php

namespace App\Domain\Images;

interface Image
{
	public function exists();
	public function getHtml(array $attributes = []);
	public function getUrl();
	public function getPath();
	public function getSourcePath();
	public function getBasePath();
	public function getBaseUrl();
	public function getRelativePath();
	public function getWidth();
	public function getHeight();
}