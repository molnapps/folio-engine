<?php

namespace App\Domain\Images;

class ImageSizeCalculator
{
	private $filePath;

	public function __construct($imageOrPath)
	{
		if ($imageOrPath instanceof Image) {
			$imageOrPath = $imageOrPath->getPath();
		}
		$this->filePath = $imageOrPath;
	}

	public function get()
	{
		try {
			list ($width, $height) = $this->isSvgImage()
				? $this->getSvgImageSize()
				: $this->getImageSize();
			
			return ['width' => $width, 'height' => $height];
		} catch ( \Exception $e) {
			return ['width' => null, 'height' => null];
		}
	}

	private function getImageSize()
	{
		return getimagesize($this->filePath);
	}

	private function getSvgImageSize()
	{
		list ($cx, $cy, $width, $height) = $this->getViewBoxAttributes();
		return [$width, $height];
	}

	private function isSvgImage()
	{
		return substr($this->filePath, -4, 4) == '.svg';
	}

	private function getViewBoxAttributes()
	{
		$svg = file_get_contents($this->filePath);
		$viewbox = explode('"', explode('viewBox="', $svg)[1])[0];

		return explode(' ', $viewbox);;
	}
}