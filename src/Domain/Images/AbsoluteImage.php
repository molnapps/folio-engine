<?php

namespace App\Domain\Images;

use \Illuminate\Contracts\Support\Arrayable;

class AbsoluteImage extends AbstractImage
{
	private $path;
	
	public function __construct($path)
	{
		$this->path = $path;
		$this->basePath = env('BASE_PATH_PUBLIC');
		$this->baseUrl = env('BASE_URL_PUBLIC');
	}

	protected function doExists()
	{
		return file_exists($this->getPath());
	}

	public function getRelativePath()
	{
		return trim($this->getAbsolutePath(), '/');
	}

	private function getAbsolutePath()
	{
		return trim($this->path, '~');
	}
}