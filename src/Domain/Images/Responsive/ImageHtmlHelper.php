<?php

namespace App\Domain\Images\Responsive;

use App\Domain\Images\ResponsiveImage;

class ImageHtmlHelper
{
	private $basePathDestination;
	private $baseUrlDestination;

	public function __construct(ResponsiveImage $responsiveImage)
	{
		$this->basePathDestination = $responsiveImage->getBasePath();
		$this->baseUrlDestination = $responsiveImage->getBaseUrl();
	}

	public function getPicture(array $images)
	{
		return '<picture>' . implode('', $images) . '</picture>';
	}

	public function getImagesMarkup(array $outputs, array $attributes) : string
	{
		$images = [];

		foreach ($outputs as $output) {
			$images[] = $this->getImageMarkup($output, $attributes);
		}

		$images = $this->displayWebpImagesFirst($images);
		
		return $this->getPicture($images);
	}

	public function getImageMarkup(array $output, array $attributes) : string
	{
		return $this->isSourceImage($output)
			? $this->getSourceImageMarkup($output, $attributes)
			: $this->getClassicImageMarkup($output, $attributes);
	}

	private function isSourceImage($output)
	{
		return in_array(
			$output['format'], 
			['webp']
		);
	}

	public function getSourceImageMarkup(array $output, array $attributes) : string
	{
		$attributes = array_merge($attributes, [
			'type' => $this->getTypeAttribute($output)
		]);

		return $this->getMarkup(
			'<source srcset="%s"%s%s></source>', 
			$output,
			$attributes
		);
	}

	public function getClassicImageMarkup(array $output, array $attributes) : string
	{
		return $this->getMarkup(
			'<img src="%s"%s%s />', 
			$output,
			$attributes
		);
	}

	private function getMarkup($templateStr, array $output, array $attributes) : string
	{
		$data = array_merge($output, $attributes);

		return sprintf(
			$templateStr, 
			$this->getSrcAttribute($output), 
			$this->getSizeHtmlAttributes($data),
			$this->getOtherHtmlAttributes($attributes)
		);
	}

	private function getSizeHtmlAttributes(array $data)
	{
		return $this->getHtmlAttributes(
            $this->getSizeAttributes($data)
        );
	}

	private function getOtherHtmlAttributes(array $attributes)
	{
		return $this->getHtmlAttributes(
            $this->getAllExceptSizeAttributes($attributes)
        );
	}

	private function getHtmlAttributes(array $attributes)
	{
		if ( ! $attributes) {
			return;
		}

		$tmp = [];

		foreach ($attributes as $key => $value) {
			$tmp[] = sprintf('%s="%s"', $key, $value);
		}
		
		return ' ' . implode(' ', $tmp);
	}

	public function getSrcAttribute(array $output)
	{
		return str_replace(
			$this->basePathDestination . '/', 
			$this->baseUrlDestination . '/', 
			$output['path']
		);
	}

	private function getSizeAttributes(array $output)
	{
        $output = array_merge(
            ['width' => '', 'height' => ''], 
            $output
        );

		return [
			'width' => $output['width'], 
			'height' => $output['height']
		];
	}

	private function getTypeAttribute(array $output)
	{
		$specialTypesMap = [
			'svg' => 'svg+xml'
		];

		$type = array_key_exists($output['format'], $specialTypesMap)
			? $specialTypesMap[ $output['format'] ]
			: $output['format'];

		return 'image/' . $type;
	}

	private function getAllExceptSizeAttributes(array $attributes)
	{
		if (isset($attributes['width'])) {
			unset($attributes['width']);
		}

		if (isset($attributes['height'])) {
			unset($attributes['height']);
		}

		return $attributes;
	}

	private function displayWebpImagesFirst(array $images)
	{
		rsort($images);

		return $images;
	}
}