<?php

namespace App\Domain\Images;

use App\Domain\Images\Image;
use Illuminate\Contracts\Support\Arrayable;

class NormalizedImage implements Image
{
	private $image;
	private $targetArea;
    private $scaleFactor;

	public function __construct(
        Image $image, 
        $targetArea = null, 
        $scaleFactor = null
    ) {
        $this->image = $image;
        $this->targetArea = $targetArea ?: 100 * 100;
        $this->scaleFactor = $scaleFactor ?: 1;
	}

    public function exists()
    {
        return $this->image->exists();
    }

    public function getUrl()
    {
        return $this->image->getUrl();
    }

    public function getPath()
    {
        return $this->image->getPath();
    }

    public function getSourcePath()
    {
        return $this->image->getSourcePath();
    }

    public function getBasePath()
    {
        return $this->image->getBasePath();
    }

    public function getBaseUrl()
    {
        return $this->image->getBaseUrl();
    }

    public function getRelativePath()
    {
        return $this->image->getRelativePath();
    }

    public function getHtml(array $attributes = [])
    {
        return $this->image->getHtml(
            array_merge(
                $attributes, 
                ['width' => $this->getWidth(), 'height' => $this->getHeight()]
            )
        );
    }

    public function getTargetArea()
    {
        return $this->targetArea;
    }

	public function getArea()
	{
		return $this->getFloatWidth() * $this->getFloatHeight();
	}

	public function getWidth()
	{
		return round($this->getFloatWidth());
	}

	public function getHeight()
	{
        return round($this->getFloatHeight());
	}

    private function getFloatWidth() : float
    {
        if ( ! $this->exists()) {
            return (float)$this->image->getWidth();
        }

        return sqrt($this->targetArea * $this->image->getWidth() / $this->image->getHeight()) * $this->scaleFactor;
    }

    private function getFloatHeight() : float
    {
        if ( ! $this->exists()) {
            return (float)$this->image->getHeight();
        }

        return sqrt($this->targetArea * $this->image->getHeight() / $this->image->getWidth()) * $this->scaleFactor;
    }
}