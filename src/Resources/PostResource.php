<?php

namespace App\Resources;

use App\Domain\Post;
use Illuminate\Contracts\Support\Arrayable;

class PostResource implements Arrayable
{
	private $post;

	public function __construct(Post $post)
	{
		$this->post = $post;
	}

	public function toArray()
	{
		return [
			'type' => $this->post->type,
			'slug' => $this->post->slug,
			'path' => $this->post->path,
			'title' => $this->post->title,
			'client' => $this->post->client 
				? $this->post->client->apiResource()->toArray()
				: null,
			'excerpt' => $this->post->excerpt,
			'lead' => $this->post->html()->getLead(),
			'body' => $this->post->html()->getBody() ?: null,
			'credits' => $this->post->html()->getCredits(),
			'preview' => $this->post->preview->exists()
				? $this->post->preview->toArray()
				: null,
			'hero' => $this->post->hero->exists()
				? $this->post->hero->toArray()
				: null,
			'published_at' => $this->post->isPublished() 
				? $this->post->getDate() 
				: null,
		];
	}
}