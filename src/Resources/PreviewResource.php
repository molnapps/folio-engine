<?php

namespace App\Resources;

use App\Domain\Preview;
use Illuminate\Contracts\Support\Arrayable;

class PreviewResource implements Arrayable
{
	private $preview;

	public function __construct(Preview $preview)
	{
		$this->preview = $preview;
	}

	public function toArray()
	{
		return [
			'type' => $this->preview->type,
            'media' => $this->preview->media,
			'resource' => $this->preview->getResource()->toArray()
		];
	}
}