<?php

namespace App\Resources;

use App\Domain\Client;
use Illuminate\Contracts\Support\Arrayable;

class ClientResource implements Arrayable
{
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function toArray()
	{
		return [
			'slug' => $this->client->slug,
			'title' => $this->client->title,
			'logo' => $this->client->logo->toArray(),
		];
	}
}