<?php

namespace App\Resources;

use App\Domain\Post;
use App\Repositories\PostCollection;
use App\Resources\ProvidesApiResource;
use Illuminate\Contracts\Support\Arrayable;

class CollectionResource
{
	private $collection;

	public function __construct(Arrayable $collection)
	{
		$this->collection = $collection;
	}

	public function toArray()
	{
		return array_map(
			function (ProvidesApiResource $domainObject) {
				return $domainObject->apiResource()->toArray();
			}, 
			$this->collection->toArray()
		);
	}
}