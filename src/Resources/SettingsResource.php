<?php

namespace App\Resources;

use App\Domain\Settings;
use Illuminate\Contracts\Support\Arrayable;

class SettingsResource implements Arrayable
{
	private $settings;

	public function __construct(Settings $settings)
	{
		$this->settings = $settings;
	}

	public function toArray()
	{
		return ImagePathResource::hydrate(
			$this->settings->toArray()
		)->toArray();
	}
}