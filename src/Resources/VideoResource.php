<?php

namespace App\Resources;

use App\Domain\Video;
use Illuminate\Contracts\Support\Arrayable;

class VideoResource implements Arrayable
{
	private $video;

	public function __construct(Video $video)
	{
		$this->video = $video;
	}

	public function toArray()
	{
		return [
            'slug' => $this->video->slug,
			'provider' => $this->video->provider,
            'id' => $this->video->id,
			'size' => $this->video->size
		];
	}
}