<?php

namespace App\Resources;

interface ProvidesApiResource
{
	public function apiResource();
}