<?php

namespace App\Views\OpenGraph;

use App\Domain\Post;
use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

class OpenGraphConfig
{
    private $callbacks = [];

    public function customize($className, \Closure $callback)
    {
        $this->callbacks[$className] = $callback;
    }

    public function invokeCallback(OpenGraph $instance, array $args) : array
    {
        $key = $this->getCallbackKey($instance);

        if ( ! $this->hasCallback($key)) {
            return [];
        }

        return $this->executeCallback($key, $args);
    }

    private function getCallbackKey(OpenGraph $instance)
    {
        return get_class($instance);
    }

    private function executeCallback($key, array $args) : array
    {
        return call_user_func_array(
            $this->getCallback($key), 
            $args
        );
    }

    private function hasCallback($key) : bool
    {
        return array_key_exists(
            $key, 
            $this->callbacks
        );
    }

    private function getCallback($key) : \Closure
    {
        return $this->callbacks[$key];
    }
}