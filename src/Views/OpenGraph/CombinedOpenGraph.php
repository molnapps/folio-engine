<?php

namespace App\Views\OpenGraph;

use App\Domain\Post;
use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

class CombinedOpenGraph implements OpenGraph
{
    private $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function getUrl()
    {
        return $this->returnFirstValid('getUrl');
    }

    public function getImageUrl()
    {
        return $this->returnFirstValid('getImageUrl');
    }

    public function getType()
    {
        return $this->returnFirstValid('getType');
    }

    public function getTitle()
    {
        return $this->returnFirstValid('getTitle');
    }

    public function getDescription()
    {
        return $this->returnFirstValid('getDescription');
    }

    private function returnFirstValid($methodName)
    {
        foreach ($this->items as $item) {
            if ( ! $item->$methodName()) {
                continue;
            }
            return $item->$methodName();
        }
    }
}