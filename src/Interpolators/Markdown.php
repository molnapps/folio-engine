<?php

namespace App\Interpolators;

use Parsedown;

class Markdown implements Interpolator
{
	public function text($content)
	{
		return (new Parsedown)->text($content);
	}
}