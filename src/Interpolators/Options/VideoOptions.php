<?php

namespace App\Interpolators\Options;

class VideoOptions 
{
	private $whitelist = ['loop'];

	public function get(array $preferences)
	{
		return $this->getCustomizedPreferences(
			$this->getWhitelistedPreferences($preferences)
		);
	}

	protected function getCustomizedPreferences(array $preferences)
	{
		return $preferences;
	}

	private function getWhitelistedPreferences(array $preferences) : array 
	{
		return array_filter(
			$preferences, 
			function ($key) {
				return in_array($key, $this->whitelist);
			},
			ARRAY_FILTER_USE_KEY
		);
	}
}