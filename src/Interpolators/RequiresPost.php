<?php

namespace App\Interpolators;

use App\Domain\Post;

interface RequiresPost
{
	public function setPost(Post $post);
}