<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Domain\Video;
use App\Preferences\VimeoPreferences;

class Videos implements Interpolator, RequiresPost
{
	use HasPost;

	public function __construct(Post $post = null)
	{
		$this->setPost($post ?: new Post);
	}
	
	public function text($content)
	{
		foreach ($this->getPost()->videos as $video) {
			if ( ! $this->isSupportedProvider($video)) {
				continue;
			}

			$content = $this->replaceSlugTokenWithProviderAndIdToken($video, $content);
		}

		return $content;
	}

	private function replaceSlugTokenWithProviderAndIdToken($video, $content)
	{
		return str_replace(
			"@video('{$video->slug}')", 
			sprintf("{%s:%s}", $video->provider, $video->id), 
			$content
		);
	}

	private function isSupportedProvider(Video $video) {
		return in_array(
			$video->provider, 
			$this->getSupportedProviders()
		);
	}

	private function getSupportedProviders()
	{
		return ['vimeo', 'youtube'];
	}
}