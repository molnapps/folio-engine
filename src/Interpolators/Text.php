<?php

namespace App\Interpolators;

use \App\Domain\Post;

class Text implements Interpolator, RequiresPost
{
    use HasPost;
    
    public function __construct(Post $post = null)
    {
        $this->setPost($post ?: new Post);
    }

    public function text($content)
	{
        $content = $this->resolveText($content, 'text');
        $content = $this->resolveTextWithScope($content, 'credits');
        $content = $this->resolveTextWithScope($content, 'title');

		return $content;
	}

    private function resolveTextWithScope($content, $token)
    {
        return $this->resolveText($content, $token, $hasScopedSlug = true);
    }

    private function resolveText($content, $token, $hasScopedSlug = false)
    {
        foreach ($this->getAllSlugs($content, $token) as $slug) {
            $content = $this->replaceSlug(
                $content, 
                $token, 
                $slug, 
                $hasScopedSlug
            );
        }

        return $content;
    }

    private function getAllSlugs($content, $token)
    {
        preg_match_all("/@{$token}\('([a-z0-9\-]+)'\)/", $content, $matches);

        return $matches[1];
    }

    private function replaceSlug($content, $token, $slug, $hasScopedSlug)
    {
        $text = $this->getPost()->texts->get(
            $this->getSlugWithScope($token, $slug, $hasScopedSlug)
        );

        return str_replace("@{$token}('{$slug}')", $text, $content);
    }

    private function getSlugWithScope($token, $slug, $hasScopedSlug)
    {
        return $hasScopedSlug
            ? $token . '-' . $slug
            : $slug;
    }
}