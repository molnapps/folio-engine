<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Preferences\VideoPreferences;
use App\Interpolators\Options\VimeoOptions;

class Vimeo extends VideoEmbed
{
	protected function getRegexPattern()
	{
		return '/{vimeo:([0-9]+)}/';
	}

	protected function getEmbedIframe()
	{
		return
			'<iframe 
				src="https://player.vimeo.com/video/{id}?{optionsQuery}&title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" 
				width="{width}" 
				height="{height}" 
				frameborder="0" 
				allow="autoplay; fullscreen"
				allowFullScreen
			>
			</iframe>';
	}

	protected function getPreferencesInstance()
	{
		return new VideoPreferences($this->getPost(), 'vimeo');
	}

	protected function getOptionsInstance()
	{
		return new VimeoOptions;
	}
}