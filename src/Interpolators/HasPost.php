<?php

namespace App\Interpolators;

use App\Domain\Post;

trait HasPost
{
	private $post;
	
	public function setPost(Post $post)
	{
		$this->post = $post;
	}

	public function getPost()
	{
		$this->guardPost();

		return $this->post;
	}

	private function guardPost()
	{
		if ($this->post) {
			return;
		}

		throw new \Exception('Please provide a post.');
	}
}