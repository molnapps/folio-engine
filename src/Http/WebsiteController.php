<?php

namespace App\Http;

use App\Http\Controllers\PortfolioController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

class WebsiteController
{
	private $response;

	public function run(Request $request = null)
	{
		$routes = (new WebsiteRouter)->register();

		$request = $request ?: Request::createFromGlobals();
		
		$matcher = new UrlMatcher($routes, new RequestContext);
		
		$dispatcher = new EventDispatcher;
		$dispatcher->addSubscriber(
			new RouterListener($matcher, new RequestStack)
		);

		$kernel = new HttpKernel(
			$dispatcher, 
			new ControllerResolver, 
			new RequestStack, 
			new ArgumentResolver
		);

		try {
			$this->response = $kernel->handle($request);
			$this->response->send();
		} catch (\Exception $e) {
			$this->response = (new PortfolioController)->handleException($e);
			$this->response->send();
		}

		$kernel->terminate($request, $this->response);

		return $this;
	}

	public function getResponse()
	{
		return $this->response;
	}
}