<?php

namespace App\Http;

use App\Http\Controllers\PortfolioController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class WebsiteRouter
{
	private $routes;

	public function register()
	{
		$this->routes = new RouteCollection;
		
		$this
			->get('/', [PortfolioController::class, 'home'])
			->get('/project/{slug}', [PortfolioController::class, 'project'])
			->get('/page/{slug}', [PortfolioController::class, 'page'])
			->get(
				'/page/{path?}/{slug}', 
				[PortfolioController::class, 'page'], 
				['path' => '.+']
			);
		
		return $this->routes;
	}

	private function get($path, array $controller, array $requirements = [])
	{
		$this->routes->add(
			$path, 
			new Route(
				$path, 
				$defaults = ['_controller' => $controller], 
				$requirements, 
				$options = [], 
				$host = null, 
				$schemes = [], 
				$methods = ['GET']
			)
		);

		return $this;
	}
}