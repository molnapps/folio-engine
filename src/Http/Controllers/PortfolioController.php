<?php

namespace App\Http\Controllers;

use App\Discovery\SlugFactory;
use App\Portfolio;
use App\PortfolioGenerator;
use App\Themes\ThemeResponseProvider;
use Symfony\Component\HttpFoundation\Response;

class PortfolioController
{
	public function home() : Response
	{
		return $this->getResponseProvider()->homepage();
	}

	public function project($slug) : Response
	{
		return $this->getResponseProvider()->project($slug);
	}

	public function page($slug, $path = null) : Response
	{
		try {
			return $this->getResponseProvider()->page(
				SlugFactory::fromRouter($slug, $path)
			);
		} catch (\Exception $e) {
			return $this->notfound();
		}
	}

	public function notFound() : Response
	{
		return $this->getResponseProvider()->notFound();
	}

	private function getResponseProvider()
	{
		return new ThemeResponseProvider(
			app(Portfolio::class)
		);
	}

	public function handleException(\Exception $e) : Response
	{
		if (app()->env('testing') || app()->env('local')) {
			throw $e;
		}

		return $this->notFound();
	}
}